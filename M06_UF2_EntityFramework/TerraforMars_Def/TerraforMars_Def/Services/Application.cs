﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerraforMars_Def.Data;
using TerraforMars_Def.Models;

namespace TerraforMars_Def.Services
{
    internal interface IApplication
    {
        void Run();
    }

    internal class Application : IApplication
    {
        public void Run()
        {
            Console.WriteLine("Hello World from application!");
        }
    }
}
