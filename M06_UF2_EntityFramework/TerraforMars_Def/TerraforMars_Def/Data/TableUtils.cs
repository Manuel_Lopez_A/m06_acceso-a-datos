﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerraforMars_Def.Models;

namespace TerraforMars_Def.Data
{

    public class TableUtils
    {
        public TerraforMarsContext Context { get; set; }

        // Constructor que recibe un TerraforMarsContext
        public TableUtils(TerraforMarsContext context)
        {
            Context = context;
        }
        public List<List<Maker>> CreateTable(int max, int min)
        {
            List<List<Maker>> table = new List<List<Maker>>();
            InitTable(table, max, min);
            PrintTable(table);
            Console.WriteLine("ADD VEINS");

            for (int y = 0; y < table.Count; y++)
            {
                for (int x = 0; x < table[y].Count; x++)
                {
                    SetVeins(table, max, y, x);
                    Context.Makers.Add(table[y][x]);
                }
            }
            PrintTable(table);
            return table;
        }

        public void InitTable(List<List<Maker>> table, int size, int min)
        {
            int id = 0;
            int row = 0;
            for (int i = min; i < size; i++)
            {
                table.Add(new List<Maker>());
                for (int j = 0; j < i; j++)
                {
                    table[row].Add(new Maker("maker_" + id));
                    id++;
                }
                row++;
            }

            for (int i = size; i >= min; i--)
            {
                table.Add(new List<Maker>());
                for (int j = 0; j < i; j++)
                {
                    table[row].Add(new Maker("maker_" + id));
                    id++;
                }
                row++;
            }
        }

        public void SetVeins(List<List<Maker>> table, int size, int y, int x)
        {
            Maker m = table[y][x];
            if (x - 1 >= 0)
            {
                m.MakersVecinos.Add(table[y][x - 1]);
            }
            if (x + 1 < table[y].Count)
            {
                m.MakersVecinos.Add(table[y][x + 1]);
            }
            Console.WriteLine(size / 2);
            if (y == size / 2)
            {
                if (y - 1 >= 0 && x - 1 >= 0)
                {
                    m.MakersVecinos.Add(table[y - 1][x - 1]);
                }
                if (y - 1 >= 0 && x < table[y - 1].Count)
                {
                    m.MakersVecinos.Add(table[y - 1][x]);
                }
                if (y + 1 < table.Count && x - 1 >= 0)
                {
                    m.MakersVecinos.Add(table[y + 1][x - 1]);
                }
                if (y + 1 < table.Count && x < table[y + 1].Count)
                {
                    m.MakersVecinos.Add(table[y + 1][x]);
                }
            }
            else if (y < size / 2)
            {
                if (y - 1 >= 0 && x - 1 >= 0)
                {
                    m.MakersVecinos.Add(table[y - 1][x - 1]);
                }
                if (y - 1 >= 0 && x < table[y - 1].Count)
                {
                    m.MakersVecinos.Add(table[y - 1][x]);
                }
                if (y + 1 < table.Count)
                {
                    m.MakersVecinos.Add(table[y + 1][x]);
                }
                if (y + 1 < table.Count && x + 1 < table[y + 1].Count)
                {
                    m.MakersVecinos.Add(table[y + 1][x + 1]);
                }
            }
            else if (y > size / 2)
            {
                if (y - 1 >= 0)
                {
                    m.MakersVecinos.Add(table[y - 1][x]);
                }
                if (y - 1 >= 0 && x + 1 < table[y - 1].Count)
                {
                    m.MakersVecinos.Add(table[y - 1][x + 1]);
                }
                if (y + 1 < table.Count && x - 1 >= 0)
                {
                    m.MakersVecinos.Add(table[y + 1][x - 1]);
                }
                if (y + 1 < table.Count && x < table[y + 1].Count)
                {
                    m.MakersVecinos.Add(table[y + 1][x]);
                }
            }
            m.MaxNeighbours = m.MakersVecinos.Count;
        }

        public void PrintTable(List<List<Maker>> table)
        {
            Console.WriteLine("-------");
            foreach (List<Maker> tiles in table)
            {
                foreach (Maker m in tiles)
                {
                    Console.WriteLine(m);
                }
                Console.WriteLine();
            }
            Console.WriteLine("-------");
        }
    }
}