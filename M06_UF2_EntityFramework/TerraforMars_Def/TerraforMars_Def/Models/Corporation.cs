﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerraforMars_Def.Data;

namespace TerraforMars_Def.Models
{
    public class Corporation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdCorporation { get; set; }

        public string Name { get; set; } = String.Empty;
        public string Description { get; set; } = String.Empty;
        public int VictoryPoints { get; set; } = 0;

        public int PlayerId { get; set; }  // Foreign key OJO
        public Player? Player { get; set; }

        public List<Maker> MakersList { get; set; } = new List<Maker>();

        //List<Corporation> CorporationsListAll(TerraforMarsContext context)
        //{
        //    List<Corporation> corpos = [];
        //    foreach(Corporation c in context.Corporations){
        //        corpos.Add(c);
        //    }
        //    return corpos;
        //}
    }
}
