﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TerraforMars_Def.Models
{
    public class Player
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdPlayer { get; set; }
        public string Name { get; set; } = String.Empty;
        public int Wins { get; set; } = 0;
        public Corporation? Corporation { get; set; }
        public List<Game> Games { get; set; } = new List<Game>();
        //public List<PlayerGames> PlayerGames { get; set; } = new List<PlayerGames>();
        public List<Game> WonGames { get; set; } = new List<Game>();

        public void SetCorporation(Corporation corporation)
        {
            Corporation = corporation;
            Console.WriteLine();
        }
    }
}