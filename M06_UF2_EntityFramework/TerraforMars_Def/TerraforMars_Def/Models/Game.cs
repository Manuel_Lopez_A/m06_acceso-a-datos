﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TerraforMars_Def.Data;

namespace TerraforMars_Def.Models
{
    public class Game
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdGame { get; set; }
        public int Oxygen { get; set; } = 0;
        public int Temperature { get; set; } = -30;
        public int Oceans { get; set; } = 0;
        public DateTime DateStart { get; set; } = DateTime.Now;
        public DateTime? DateEnd { get; set; } = null;
        public List<Player> Players { get; set; } = new List<Player>();
        //public List<PlayerGames> PlayerGames { get; set; } = new List<PlayerGames>();
        //public Player Winner { get; set; } = null;
        public int? WinnerId { get; set; }
        public Player? Winner { get; set; }

        public bool IsEndedGame(TerraforMarsContext context)
        {
            int assolits = 0;
            if (this.Temperature == 0) assolits++;            
            if (this.Oxygen == 14) assolits++;            

            bool flag = false;
            Maker mAux = new();
            List<Maker> oceanos = mAux.GetMakersByType(Typemaker.OCEA, context);
            foreach(Maker m in oceanos)
            {
                if (m.CorpoAsociada != null) flag = true;
            }
            if (flag) assolits++;

            if (assolits >= 2) return true; 
            else return false;
        }
    }
}
