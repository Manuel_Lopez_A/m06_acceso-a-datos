package dao;

import java.io.Serializable;
import java.util.Set;

import modelsPackage.Habitant;
import modelsPackage.Recurs;
import modelsPackage.Tasca;


public class TascaDAO extends DAOGeneric<Tasca, Serializable>{

	public TascaDAO() {
		super(Tasca.class);
	}

	public Tasca GenerateTasca() {
		Tasca tasca = new Tasca();
		this.save(tasca);
		return tasca;
	}
	public Tasca GenerateTasca(String titol, double dedicacio, int recompensa, Recurs recurs) {
		Tasca tasca = new Tasca(titol, dedicacio, recompensa, recurs);
		this.save(tasca);
		return tasca;
	}
	public Tasca GenerateTasca(String titol, double dedicacio, int recompensa, Recurs recurs, Set<Habitant> habitants) {
		Tasca tasca = new Tasca(titol, dedicacio, recompensa, recurs, habitants);
		this.save(tasca);
		return tasca;
	}
	
	public void setInitialReward(int id) {
		Tasca tasca = find(id);
		if(tasca.getDedicacio()>85){
			tasca.setRecompensa(tasca.getRecompensa() + 50);
		} else {

			tasca.setRecompensa(tasca.getRecompensa() + 30);
		}
		update(tasca);
		System.out.println("La tasca-->" + tasca.toString());
	}
	
	
}
