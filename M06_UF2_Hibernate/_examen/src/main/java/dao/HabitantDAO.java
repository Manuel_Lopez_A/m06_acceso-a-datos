package dao;

import java.io.Serializable;
import java.util.Set;

import modelsPackage.Especie;
import modelsPackage.Habitant;
import modelsPackage.Tasca;

public class HabitantDAO extends DAOGeneric<Habitant, Serializable>{

	public HabitantDAO() {
		super(Habitant.class);
	}

	public Habitant GenerateHabitant() {
		Habitant habitant = new Habitant();
		this.save(habitant);
		return habitant;
	}
	
	public Habitant GenerateHabitant(String nom, Especie especie) {
		Habitant habitant = new Habitant(nom, especie);
		this.save(habitant);
		return habitant;
	}
	
	public Habitant GenerateHabitant(String nom, Especie especie, Set<Tasca> tasques) {
		Habitant habitant = new Habitant(nom, especie,tasques);
		this.save(habitant);
		return habitant;
	}
	
	public void daleTasca(Tasca t, Habitant h) {
		Habitant habitant = find(h.getIdHabitant());		
		habitant.getTasques().add(t);
		update(habitant);
		
	}
	
}
