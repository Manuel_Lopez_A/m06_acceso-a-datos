package mainPackage;

import dao.HabitantDAO;
import dao.TascaDAO;
import modelsPackage.Especie;
import modelsPackage.Habitant;
import modelsPackage.Recurs;
import modelsPackage.Tasca;
public class MainExamen {
	
	public static void main(String[] args) {
		
		HabitantDAO habitantDao = new HabitantDAO();
		TascaDAO tascaDao = new TascaDAO();
		
		Habitant h1 = habitantDao.GenerateHabitant("Manu", Especie.HUMAN);
		Habitant h2 = habitantDao.GenerateHabitant("Laura", Especie.HUMAN);
		Habitant h3 = habitantDao.GenerateHabitant("Tina", Especie.FURRY);
		Habitant h4 = habitantDao.GenerateHabitant("Seth", Especie.REPTILIAN);
		Habitant h5 = habitantDao.GenerateHabitant("Marla", Especie.FURRY);
		
		Tasca t1 = tascaDao.GenerateTasca("Lavar la ropa", 15.50, 50, Recurs.FOOD);
		Tasca t2 = tascaDao.GenerateTasca("Fregar los platos", 90.50, 100, Recurs.MATERIALS);
		Tasca t3 = tascaDao.GenerateTasca("Cazar", 0.50, 50, Recurs.FOOD);
		Tasca t4 = tascaDao.GenerateTasca("Hacer guardia", 85.50, 10, Recurs.TOOLS);
		Tasca t5 = tascaDao.GenerateTasca("Tender lavadora", 15.50, 50, Recurs.MATERIALS);
		
		habitantDao.daleTasca(t5, h1);
		habitantDao.daleTasca(t4, h1);
		habitantDao.daleTasca(t3, h2);
		habitantDao.daleTasca(t3, h3);
		habitantDao.daleTasca(t2, h4);
		habitantDao.daleTasca(t1, h5);
		
		
	}
}
