package modelsPackage;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="tasca")
public class Tasca {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tasca")	
	private int idTasca;
	@Column(name = "titol", nullable = false, length = 50)
	private String titol;
	@Column(name="dedicacio", columnDefinition = "double(6,2)")
	private double dedicacio = 0.0;
	@Column(name="recompensa", columnDefinition = "int")
	private int recompensa;
	@Enumerated(EnumType.STRING)
	@Column(name = "recurs")
	private Recurs recurs;
	
	//M-N (CLÁSICO) - es mapeado por....
	@ManyToMany(mappedBy = "tasques")
	private Set<Habitant> habitants = new HashSet<>();


	//---------------------------------------------------//
	
	
	public Tasca() {
		super();
	}

	public Tasca(String titol, double dedicacio, int recompensa, Recurs recurs) {
		super();
		this.titol = titol;
		this.dedicacio = dedicacio;
		this.recompensa = recompensa;
		this.recurs = recurs;
	}

	public Tasca(String titol, double dedicacio, int recompensa, Recurs recurs, Set<Habitant> habitants) {
		super();
		this.titol = titol;
		this.dedicacio = dedicacio;
		this.recompensa = recompensa;
		this.recurs = recurs;
		this.habitants = habitants;
	}

	public String getTitol() {
		return titol;
	}

	public void setTitol(String titol) {
		this.titol = titol;
	}

	public double getDedicacio() {
		return dedicacio;
	}

	public void setDedicacio(double dedicacio) {
		this.dedicacio = dedicacio;
	}

	public int getRecompensa() {
		return recompensa;
	}

	public void setRecompensa(int recompensa) {
		this.recompensa = recompensa;
	}

	public Recurs getRecurs() {
		return recurs;
	}

	public void setRecurs(Recurs recurs) {
		this.recurs = recurs;
	}

	public Set<Habitant> getHabitants() {
		return habitants;
	}

	public void setHabitants(Set<Habitant> habitants) {
		this.habitants = habitants;
	}

	public int getIdTasca() {
		return idTasca;
	}

	@Override
	public String toString() {
		return "Tasca [idTasca=" + idTasca + ", titol=" + titol + ", dedicacio=" + dedicacio + ", recompensa="
				+ recompensa + ", recurs=" + recurs + ", habitants=" + habitants + "]";
	}
	
	
	
}
