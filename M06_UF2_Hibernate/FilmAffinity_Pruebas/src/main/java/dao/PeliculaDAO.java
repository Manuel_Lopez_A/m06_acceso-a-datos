package dao;

import java.io.Serializable;

import javax.persistence.Entity;

import modelsPackage.Genero;
import modelsPackage.Pelicula;

@Entity
public class PeliculaDAO extends DAOGeneric<Pelicula, Serializable>{

	public PeliculaDAO() {
		super(Pelicula.class);
	}
	public Pelicula generatePelicula() {
		Pelicula p = new Pelicula();
		this.save(p);
		return p;
	}
	
    public Pelicula generatePelicula(String titulo, double puntuacion, Genero genero) {
        Pelicula p = new Pelicula(titulo, puntuacion, genero);
        this.save(p);
        return p;
    }

}
