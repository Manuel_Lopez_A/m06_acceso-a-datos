package dao;

import java.io.Serializable;
import java.util.List;

public interface IDAOGeneric <T, ID extends Serializable> 
{
	void persist(T entity);
	T searchById(ID id); 
	List<T> listAll();
	void deleteById(ID id);
	void deleteEqual(Object object);	
}
