package dao;

import java.io.Serializable;

import modelsPackage.Socio;

public class SocioDAO extends DAOGeneric<Socio, Serializable>{

	public SocioDAO() {
		super(Socio.class);
	}
	public Socio generateSocio() {
		Socio s = new Socio();
		this.save(s);
		//this.persist(s);
		return s;
	}
	/* otros constructores, tantos como necesitemos */
	/* métodos de meterle cosas */

}
