package dao;

import java.io.Serializable;

import modelsPackage.Videoclub;

public class VideoclubDAO extends DAOGeneric<Videoclub, Serializable>{

	public VideoclubDAO() {
		super(Videoclub.class);
	}
	public Videoclub generateVideoclub() {
		Videoclub v = new Videoclub();
		this.save(v);
		//this.persist(v);
		return v;
	}
	/* otros constructores, tantos como necesitemos */
	/* métodos de meterle cosas */

}
