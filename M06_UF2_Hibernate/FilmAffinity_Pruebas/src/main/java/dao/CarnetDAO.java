package dao;

import java.io.Serializable;

import modelsPackage.Carnet;

public class CarnetDAO extends DAOGeneric<Carnet, Serializable> {

	public CarnetDAO() {
		super(Carnet.class);
	}

	public Carnet generateCarnet() {
		Carnet c = new Carnet();
		this.save(c);
		//this.persist(c);
		return c;
	}
	/* otros constructores, tantos como necesitemos */
	/* métodos de meterle cosas */

}
