package mainPackage;

import dao.CarnetDAO;
import dao.PeliculaDAO;
import dao.SocioDAO;
import dao.VideoclubDAO;
import modelsPackage.Genero;

public class mainExamen {

	static CarnetDAO carnetD = new CarnetDAO();
	static SocioDAO socioD = new SocioDAO();
	static VideoclubDAO videoD = new VideoclubDAO();
	static PeliculaDAO peliD = new PeliculaDAO();
		
	public static void main(String[] args) {
		
		peliD.generatePelicula("Casablanca", 8.5, Genero.DRAMA);
		peliD.generatePelicula("Gone with the Wind", 7.8, Genero.ROMANCE);
		peliD.generatePelicula("The Godfather", 9.2, Genero.CRIMEN);
		peliD.generatePelicula("Citizen Kane", 8.7, Genero.MISTERIO);
		peliD.generatePelicula("Schindler's List", 9.3, Genero.DRAMA);
		
		System.out.println(peliD.listAll());
		System.out.println(peliD.searchById(3));
		
	}
	
}
