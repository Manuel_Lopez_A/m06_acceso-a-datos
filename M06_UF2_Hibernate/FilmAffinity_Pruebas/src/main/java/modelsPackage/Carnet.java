package modelsPackage;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Carnet {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_carnet")
	private int id;
	@Column(name = "numero", nullable = false, length = 50)
	private String numero;
	
	//----------------- RELACIONES ----------------------//
	
	//1-1 (mapeado)
	@OneToOne(mappedBy = "carnet")
	private Socio socio;
	
	//M-1 (CARNET es many to one)
	@ManyToOne
    @JoinColumn(name = "videoclub_id")
	private Videoclub videoclub;

	//---------------------------------------------------//
	

	public Carnet() {
		super();
	}

	public Carnet(String numero, Socio socio, Videoclub videoclub) {
		super();
		this.numero = numero;
		this.socio = socio;
		this.videoclub = videoclub;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

	public Videoclub getVideoclub() {
		return videoclub;
	}

	public void setVideoclub(Videoclub videoclub) {
		this.videoclub = videoclub;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Carnet [id=" + id + ", numero=" + numero + ", socio=" + socio + ", videoclub=" + videoclub + "]";
	}
	
	
	
	
	
	
	
}
