package modelsPackage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity
public class Socio {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_socio")
	private int id;
	@Column(name = "nombre", nullable = false, length = 80)
	private String nombre;

	//-----------------RELACIONES ----------------------//
	
	//1-1 (PESO DE LA RELACIÓN)
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "carnet_id", unique = true)
	private Carnet carnet;
	
	//N-M (REFLEXIVA)
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "socios_afines",
            joinColumns = @JoinColumn(name = "socio1_id"),
            inverseJoinColumns = @JoinColumn(name = "socio2_id")
    )
    private Set<Socio> sociosAfines = new HashSet<>();
    
    //N-M (CLÁSICA) - lleva el peso de la relación
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "alquileres",
            joinColumns = @JoinColumn(name = "socio_id"),
            inverseJoinColumns = @JoinColumn(name = "pelicula_id")
    )
    private Set<Pelicula> peliculasAlquiladas = new HashSet<>();


    
	//---------------------------------------------------//
    
    
	public Socio() {
		super();
	}

	public Socio(String nombre, Carnet carnet) {
		super();
		this.nombre = nombre;
		this.carnet = carnet;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Carnet getCarnet() {
		return carnet;
	}

	public void setCarnet(Carnet carnet) {
		this.carnet = carnet;
	}

	public Set<Socio> getSociosAfines() {
		return sociosAfines;
	}

	public void setSociosAfines(Set<Socio> sociosAfines) {
		this.sociosAfines = sociosAfines;
	}

	public Set<Pelicula> getPeliculasAlquiladas() {
		return peliculasAlquiladas;
	}

	public void setPeliculasAlquiladas(Set<Pelicula> peliculasAlquiladas) {
		this.peliculasAlquiladas = peliculasAlquiladas;
	}

	public int getId() {
		return id;
	}
	
	
	
    
}
