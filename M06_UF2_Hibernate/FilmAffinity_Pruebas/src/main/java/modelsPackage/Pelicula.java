package modelsPackage;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Pelicula {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pelicula")
	private int id;
	@Column(name = "titulo", nullable = false, length = 50)
	private String titulo;
	@Column(name="puntuacion_filmaffinity", columnDefinition = "double(4,2)") // la nota se verá en formato 4 dígitos, de los cuales 2 decimales
	double puntuacion = 0.00; // por defecto 0.00
	@Enumerated(EnumType.STRING)
	@Column(name = "genero")
	Genero genero;

	//M-N (CLÁSICO) - es mapeado por....
	@ManyToMany(mappedBy = "peliculasAlquiladas", fetch = FetchType.EAGER)
	private Set<Socio> sociosQueAlquilaron = new HashSet<>();


	//---------------------------------------------------//
	
	
	public Pelicula() {
		super();
	}

	public Pelicula(String titulo, double puntuacion, Genero genero) {
		super();
		this.titulo = titulo;
		this.puntuacion = puntuacion;
		this.genero = genero;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getPuntuacion() {
		return puntuacion;
	}

	public void setPuntuacion(double puntuacion) {
		this.puntuacion = puntuacion;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public Set<Socio> getSociosQueAlquilaron() {
		return sociosQueAlquilaron;
	}

	public void setSociosQueAlquilaron(Set<Socio> sociosQueAlquilaron) {
		this.sociosQueAlquilaron = sociosQueAlquilaron;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Pelicula [id=" + id + ", titulo=" + titulo + ", puntuacion=" + puntuacion + ", genero=" + genero
				+ ", sociosQueAlquilaron=" + sociosQueAlquilaron + "]";
	}
	
	
	
	
	
	

}
