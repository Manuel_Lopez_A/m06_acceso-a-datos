package modelsPackage;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Videoclub {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_videoclub")
	private int id;
	@Column(name = "nombre", nullable = false, length = 50)
	private String nombre;
	
	//-----------------RELACIONES ----------------------//
	
	//1-N (VIDEOCLUB es One to Many) - PESO DE LA RELACIÓN
	@OneToMany(mappedBy = "videoclub", cascade = CascadeType.ALL)
	private List<Carnet> carnets = new ArrayList<>();

	
	//---------------------------------------------------//
	

	public Videoclub() {
		super();
	}

	public Videoclub(String nombre, List<Carnet> carnets) {
		super();
		this.nombre = nombre;
		this.carnets = carnets;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Carnet> getCarnets() {
		return carnets;
	}

	public void setCarnets(List<Carnet> carnets) {
		this.carnets = carnets;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Videoclub [id=" + id + ", nombre=" + nombre + ", carnets=" + carnets + "]";
	}
	
	

}
