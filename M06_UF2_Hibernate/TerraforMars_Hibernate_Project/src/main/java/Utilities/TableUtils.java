package Utilities;

import modelsPackage.Maker;
import modelsPackage.MakerDAO;

import java.util.ArrayList;
import java.util.List;

public class TableUtils {

    MakerDAO md = new MakerDAO();

    public List<List<Maker>> createTable(int max, int min) {
        List<List<Maker>> table = new ArrayList<>();
        initTable(table, max, min);
        printTable(table);
        System.out.println("ADD VEINS");

        for (int y = 0; y < table.size(); y++) {
            for (int x = 0; x < table.get(y).size(); x++) {
                setVeins(table, max, y, x);
            }
        }
        printTable(table);
        return table;
    }

    public void initTable(List<List<Maker>> table, int size, int min) {
        int id = 0;
        int row = 0;
        for (int i = min; i < size; i++) {
            table.add(new ArrayList<>());
            for (int j = 0; j < i; j++) {
                table.get(row).add(md.createMaker("maker_" + id));
                id++;
            }
            row++;
        }

        for (int i = size; i >= min; i--) {
            table.add(new ArrayList<>());
            for (int j = 0; j < i; j++) {
                table.get(row).add(md.createMaker("maker_" + id));
                id++;
            }
            row++;
        }
    }

    public void setVeins(List<List<Maker>> table, int size, int y, int x) {
        Maker m = table.get(y).get(x);
        if (x - 1 >= 0) {
            md.addVecino(m, table.get(y).get(x - 1));
        }
        if (x + 1 < table.get(y).size()) {
            md.addVecino(m, table.get(y).get(x + 1));
        }
        System.out.println(size / 2);
        if (y == size / 2) {
            if (y - 1 >= 0 && x - 1 >= 0) {
                md.addVecino(m, table.get(y - 1).get(x - 1));
            }
            if (y - 1 >= 0 && x < table.get(y - 1).size()) {
                md.addVecino(m, table.get(y - 1).get(x));
            }
            if (y + 1 < table.size() && x - 1 >= 0) {
                md.addVecino(m, table.get(y + 1).get(x - 1));
            }
            if (y + 1 < table.size() && x < table.get(y + 1).size()) {
                md.addVecino(m, table.get(y + 1).get(x));
            }
        } else if (y < size / 2) {
            if (y - 1 >= 0 && x - 1 >= 0) {
                md.addVecino(m, table.get(y - 1).get(x - 1));
            }
            if (y - 1 >= 0 && x < table.get(y - 1).size()) {
                md.addVecino(m, table.get(y - 1).get(x));
            }
            if (y + 1 < table.size()) {
                md.addVecino(m, table.get(y + 1).get(x));
            }
            if (y + 1 < table.size() && x + 1 < table.get(y + 1).size()) {
                md.addVecino(m, table.get(y + 1).get(x + 1));
            }
        } else if (y > size / 2) {
            if (y - 1 >= 0) {
                md.addVecino(m, table.get(y - 1).get(x));
            }
            if (y - 1 >= 0 && x + 1 < table.get(y - 1).size()) {
                md.addVecino(m, table.get(y - 1).get(x + 1));
            }
            if (y + 1 < table.size() && x - 1 >= 0) {
                md.addVecino(m, table.get(y + 1).get(x - 1));
            }
            if (y + 1 < table.size() && x < table.get(y + 1).size()) {
                md.addVecino(m, table.get(y + 1).get(x));
            }
        }
        md.setMaxVecinas(m);
        md.updateEntity(m);
    }

    public void printTable(List<List<Maker>> table) {
        System.out.println("-------");
        for (List<Maker> tiles : table) {
            for (Maker m : tiles) {
                System.out.println(m);
            }
            System.out.println();
        }
        System.out.println("-------");
    }
}
