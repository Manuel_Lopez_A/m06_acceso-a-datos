package Test;

import modelsPackage.Corporation;
import modelsPackage.CorporationDAO;
import modelsPackage.Maker;
import modelsPackage.MakerDAO;
import modelsPackage.Player;
import modelsPackage.PlayerDAO;
import modelsPackage.Typemaker;
import modelsPackage.Utils;

public class TestMain {
	
	public static void main(String[] args) {

		PlayerDAO playerDao = new PlayerDAO();
		CorporationDAO corpoDao = new CorporationDAO();
		MakerDAO makerDao = new MakerDAO();

		Player p1 = playerDao.createPlayer("Martí");
		Player p2 = playerDao.createPlayer("David");
		Player p3 = playerDao.createPlayer("Manu");
		Corporation c1 = corpoDao.createCorporation("Credicor", "descripcion credicorp");
		Corporation c2 = corpoDao.createCorporation("Ecoline", "descripcion Ecoline");
		Corporation c3 = corpoDao.createCorporation("Helion", "descripcion Helion");

		System.out.println(p3);
		System.out.println(c3);
		playerDao.setCorporation(p3, c3);
		System.out.println(p3);
		/*            */System.out.println();/*            */
//		c3 = corpoDao.searchById(c3.getIdCorporation());
//		System.out.println(c3);
		
		Maker m1 = makerDao.createMaker("casilla1",3, Typemaker.CIUTAT);
		Maker m2 = makerDao.createMaker("casilla2",4, Typemaker.CIUTAT);
		Maker m3 = makerDao.createMaker("casilla3",4, Typemaker.JUNGLA);
		Maker m4 = makerDao.createMaker("casilla4",4, Typemaker.CIUTAT);
		
		//sólo así se hacen vecinas entre ellas, si no, m2 sale vacía....
		makerDao.vecinasForever(m1, m2);
		System.out.println(m1.getVecinas());
		System.out.println(m2.getVecinas());
	}
}
