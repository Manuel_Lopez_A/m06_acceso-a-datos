package Test;

import Utilities.TableUtils;
import modelsPackage.Maker;

import java.util.List;

public class CreateTableMain {
    public static void main(String[] args) 
    {
        TableUtils tutils = new TableUtils();
        List<List<Maker>> table = tutils.createTable(9, 5);
        tutils.printTable(table);
    }
}
