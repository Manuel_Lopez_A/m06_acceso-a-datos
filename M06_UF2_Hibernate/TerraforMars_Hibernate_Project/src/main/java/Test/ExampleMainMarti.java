package Test;

import modelsPackage.*;

import java.util.List;

public class ExampleMainMarti {
    static PlayerDAO playerDao = new PlayerDAO();
    static CorporationDAO corpoDao = new CorporationDAO();
    static MakerDAO makerDao = new MakerDAO();
    static GameDAO gameDao = new GameDAO();
    public static void main(String[] args) {
        Corporation c1 = corpoDao.createCorporation("Credicor", "descripcion credicorp");

        List<Maker> oceans = makerDao.getMakersByType(Typemaker.OCEA);
        List<Maker> jungles = makerDao.getMakersByType(Typemaker.JUNGLA);
        List<Maker> ciutats = makerDao.getMakersByType(Typemaker.CIUTAT);
        List<Maker> ciutatsSenseCorp = makerDao.getMakersByTypeNoCorporation(Typemaker.CIUTAT);
        List<Maker> ciutatsAmbAquestaCorp = makerDao.getMakersByTypeCorporation(c1, Typemaker.CIUTAT);


    }
}
