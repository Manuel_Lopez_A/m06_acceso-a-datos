package Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import Utilities.TableUtils;
import modelsPackage.Corporation;
import modelsPackage.CorporationDAO;
import modelsPackage.Game;
import modelsPackage.GameDAO;
import modelsPackage.Maker;
import modelsPackage.MakerDAO;
import modelsPackage.Player;
import modelsPackage.PlayerDAO;
import modelsPackage.Typemaker;

public class JugarPartida {

	static PlayerDAO playerDao = new PlayerDAO();
	static CorporationDAO corpoDao = new CorporationDAO();
	static MakerDAO makerDao = new MakerDAO();
	static GameDAO gameDao = new GameDAO();

	public static void main(String[] args) {

		Game g = gameDao.createGame();
		createTable();
		createPlayers(g);
		jugar(g);
		System.out.println("EL JOC HA ACABAT!");
		printAll();

	}

    private static void createTable() {
        TableUtils tutils = new TableUtils();
        List<List<Maker>> table = tutils.createTable(9, 5);
        tutils.printTable(table);
    }

    private static void createPlayers(Game g) {
        Player p1 = playerDao.createPlayer("Martí");
        Player p2 = playerDao.createPlayer("David");
        Player p3 = playerDao.createPlayer("Manu");
        Corporation c1 = corpoDao.createCorporation("Credicor", "descripcion credicorp");
        Corporation c2 = corpoDao.createCorporation("Ecoline", "descripcion Ecoline");
        Corporation c3 = corpoDao.createCorporation("Helion", "descripcion Helion");
		playerDao.setGame(g, p1);
		playerDao.setGame(g, p2);
		playerDao.setGame(g, p3);
		playerDao.setCorporation(p1, c1);
        playerDao.setCorporation(p2, c2);
        playerDao.setCorporation(p3, c3);
    }

    private static void jugar(Game game) {
		Queue<Player> players = gameDao.getPlayersQueue(game);
		boolean playing = true;
        while (playing) {
            Player p = players.poll();
            resolveDices(p, rollingDices(), game);
			players.offer(p);
            if (gameDao.isEndedGame(game)) {
                playing = false;
                setWinnerGame(game);
            }
        }
    }

	/**
	 * Es tiren els 6 daus de manera aleatòria. El resultat de les diverses tirades
	 * es retorna amb una llista.
	 */
	private static ArrayList<Integer> rollingDices() {
		Random r = new Random();
		ArrayList<Integer> tirades = new ArrayList<>();
		for (int i = 0; i < 6; i++) {
			tirades.add(r.nextInt(1, 7));
		}
		return tirades;
	}

	/**
	 * Resolt la tirada de daus del mètode rollingDices().
	 * @param tirades
	 */
	private static void resolveDices(Player p, ArrayList<Integer> tirades, Game game) {
        System.out.println(tirades);
		int temperatura = 0;
		int oxigen = 0;
		int ocea = 0;
		int jungla = 0;
		int ciutat = 0;
		int victoria = 0;

		for (Integer tirada : tirades) {
			switch (tirada) {
			case 1:
				temperatura++;
				break;
			case 2:
				oxigen++;
				break;
			case 3:
				ocea++;
				break;
			case 4:
				jungla++;
				break;
			case 5:
				ciutat++;
				break;
			default:
				victoria++;
				break;
			}
		}

		if (temperatura >= 3) gameDao.incrementTemperature(2, game);
		if (oxigen >= 3) gameDao.incrementOxygen(1, game);
		if (victoria >= 3) corpoDao.incrementVictoryPoints(p.getCorporation(), 2);
		if (ocea >= 3) {
			List<Maker> oceansSenseCorp = makerDao.getMakersByTypeNoCorporation(Typemaker.OCEA);
			if (oceansSenseCorp.isEmpty()) {
				System.out.println("NO hi ha cap casella d'oceà lliure");
			} else {
				System.out.println("OCEA");
				makerDao.setCorporation(oceansSenseCorp.get(0), p.getCorporation());
			}
		}
		if (ciutat >= 3) {
			List<Maker> ciutatSenseCorp = makerDao.getMakersByTypeNoCorporation(Typemaker.CIUTAT);
			if (ciutatSenseCorp.isEmpty()) {
				System.out.println("NO hi ha cap casella de ciutat lliure");
			} else {
				System.out.println("CIUTAT");
				makerDao.setCorporation(ciutatSenseCorp.get(0), p.getCorporation());
			}
		}
		if (jungla >= 3) {
			List<Maker> junglaSenseCorp = makerDao.getMakersByTypeNoCorporation(Typemaker.JUNGLA);
			if (junglaSenseCorp.isEmpty()) {
				System.out.println("NO hi ha cap casella de jungla lliure");
			} else {
				System.out.println("JUNGLA");
				makerDao.setCorporation(junglaSenseCorp.get(0), p.getCorporation());
			}
		}
	}

    private static void setWinnerGame(Game game) {
        List<Corporation> corpos = corpoDao.listAll();
        int max = Integer.MIN_VALUE;
        Player winner = null;
        for (Corporation corpo : corpos) {
            if (corpo.getVictoryPoints() > max) {
                max = corpo.getVictoryPoints();
                winner = corpo.getPlayer();
            }
            gameDao.setWinner(winner, game);
        }

    }

	private static void printAll() {
		System.out.println();
		System.out.println("FINAL PLAYERS LIST");
		List<Player> players = playerDao.listAll();
		for (Player player : players) {
			System.out.println(player);
		}
		System.out.println("FINAL CORPORATIONS LIST");
		List<Corporation> corporations = corpoDao.listAll();
		for (Corporation corp : corporations) {
			System.out.println(corp);
		}
		System.out.println("FINAL MAKERS LIST");
		List<Maker> makers = makerDao.listAll();
		for (Maker maker : makers) {
			System.out.println(maker);
		}
	}

}
