package modelsPackage;

import org.hibernate.Session;

import java.util.List;
import java.util.Random;

public class MakerDAO extends DAOGeneric<Maker, Integer> {

	private final Random r = new Random();

	public MakerDAO() {
		super(Maker.class);
	}

	public Maker createMaker(String name) {
		Maker m = new Maker(name);
		switch (r.nextInt(3)) {
			case 0:
				m.setTypemaker(Typemaker.CIUTAT);
				break;
			case 1:
				m.setTypemaker(Typemaker.JUNGLA);
				break;
			default:
				m.setTypemaker(Typemaker.OCEA);
				break;
		}
		this.save(m);
		return m;
	}

	public Maker createMaker(String name, int maxneig, Typemaker typemaker) {
		Maker m = new Maker(name, maxneig, typemaker);
		this.save(m);
		return m;
	}

	// no he encontrado otra manera de hacerlo, con los tamagotchis hice lo mismo
	public void vecinasForever(Maker m1, Maker m2) {
		m1.getVecinas().add(m2);
		m2.getVecinas().add(m1);
		updateEntity(m1);
		updateEntity(m2);
	}

	// NO TOCAR ESTE, DEJADLO ASI
	public void addVecino(Maker m1, Maker m2) {
		m1.getVecinas().add(m2);
	}

	public void setMaxVecinas(Maker m) {
		m.setMaxNeighbours(m.getVecinas().size());
	}

	public void setCorporation(Maker m, Corporation c) {
		m.setCorporation(c);
		updateEntity(m);
	}
	
	
	/**
	 * Obté tots les caselles del mateix tipus.
	 *
	 * @param typemaker
	 */
	public List<Maker> getMakersByType(Typemaker t) {
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		List<Maker> lista = session.createQuery("SELECT m FROM Maker m WHERE m.typemaker = :typemaker").setParameter("typemaker", t).getResultList();
		session.getTransaction().commit();
		return lista;
	}

	public List<Maker> getMakersByTypeNoCorporation(Typemaker t) {
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		List<Maker> lista = session.createQuery("SELECT m FROM Maker m WHERE m.typemaker = :typemaker AND m.corporation IS NULL").setParameter("typemaker", t).getResultList();
		session.getTransaction().commit();
		return lista;
	}

	public List<Maker> getMakersByTypeCorporation(Corporation c, Typemaker t) {
		Session session = Utils.getSessionFactory().getCurrentSession();
		session.getTransaction().begin();
		List<Maker> lista = session.createQuery("SELECT m FROM Maker m WHERE m.typemaker = :typemaker AND m.corporation = :corporation").setParameter("typemaker", t).setParameter("corporation", c).getResultList();
		session.getTransaction().commit();
		return lista;
	}
}
