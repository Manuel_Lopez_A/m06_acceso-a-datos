package modelsPackage;

import java.util.LinkedList;
import java.util.Queue;

public class PlayerDAO extends DAOGeneric<Player, Integer>
{
	
	public PlayerDAO() {
		super(Player.class);
	}
	
	public Player createPlayer(String name) {
		Player p = new Player(name);
		save(p);
		return p;
	}
	
	public Player createPlayer(String name, int wins) {
		Player p = new Player(name, wins);
		save(p);
		return p;
	}
	
	//método que asocia un jugador con una corporación
	public void setCorporation(Player p, Corporation c) {
		p.setCorporation(c);		
		System.out.println("--> Se ha asociado a " + p.getName() + " con la corporation " + c.getName());
		updateEntity(p);
	}
	
	public void setGame(Game g, Player p) {
		Player player = searchById(p.getIdPlayer());
		player.getPartidaJugada().add(g);
		updateEntity(player);
	}

}
