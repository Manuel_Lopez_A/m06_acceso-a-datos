package modelsPackage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table (name= "players")
public class Player {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_player")
	private int idPlayer;
	@Column(name = "name", nullable = false, length = 50)
	private String name;
	@Column(name = "wins", nullable = false, columnDefinition = "int")
	private int wins = 0;
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE})
	@JoinColumn(name="id_corporation", unique = true)
	private Corporation corporation;
	
	// hay partidas que me tienen,... llevan el peso de la relación		
	@OneToMany(mappedBy = "winnerPlayer", fetch = FetchType.EAGER)
	private Set<Game> partidasGanadas = new HashSet<Game>();	
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE}) 
	@JoinTable(
			name="game_players", 
			joinColumns = @JoinColumn(name="id_player"), //-> PK propia de la classe que mapeja, 
			inverseJoinColumns = @JoinColumn(name="id_game")// -> PK de l'altra entitat de la relació
	)
	private Set<Game> partidaJugada = new HashSet<Game>();	
	
	protected Player() {
		super();
	}
	protected Player(String name) {
		super();
		this.name = name;
		this.wins = 0;
	}

	protected Player(String name, int wins) {
		super();
		this.name = name;
		this.wins = wins;
	}

	public int getIdPlayer() {
		return idPlayer;
	}
	public String getName() {
		return name;
	}
	public int getWins() {
		return wins;
	}
	public Corporation getCorporation() {
		return corporation;
	}
	
	protected Set<Game> getPartidasGanadas() {
		return partidasGanadas;
	}
	protected void setPartidasGanadas(Set<Game> partidasGanadas) {
		this.partidasGanadas = partidasGanadas;
	}
	protected void setIdPlayer(int idPlayer) {
		this.idPlayer = idPlayer;
	}
	protected void setName(String name) {
		this.name = name;
	}
	protected void setWins(int wins) {
		this.wins = wins;
	}
	protected void setCorporation(Corporation corporation) {
		this.corporation = corporation;
	}
	
	protected String corporationName() {
		return corporation != null ? corporation.getName() : "aquest jugador no te corporation";
	}
	
	public Set<Game> getPartidaJugada() {
		return partidaJugada;
	}
	
	protected void setPartidaJugada(Set<Game> partidaJugada) {
		this.partidaJugada = partidaJugada;
	}

	private List<Integer> ids() {
		List<Integer> i = new ArrayList<>();
		for (Game win : partidasGanadas) {
			i.add(win.getIdGame());
		}
		return i;
	}
	@Override
	public String toString() {
		return "Player [idPlayer=" + idPlayer + ", name=" + name + ", wins=" + wins + ", corporation=" + corporation
				+ ", partidasGanadas=" + ids() + "]";
	}


	
	
}
