package modelsPackage;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name= "games")
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_game")
	private int idGame;
	@Column(name = "oxygen", nullable = false, columnDefinition = "int")
	private int oxygen;
	@Column(name = "temperature", nullable = false, columnDefinition = "int")
	private int temperature;
	@Column(name = "oceans")
	private int oceans;
	@Column(name = "date_start")
	private LocalDateTime dateStart;
	@Column(name = "date_end")
	private LocalDateTime dateEnd;
	
	
	/* M - N :   PARTIDA Y SUS PLAYERS  */
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "partidaJugada")
	private Set<Player> playersInGame = new HashSet<Player>();
	
	
	/* M - 1 :   PARTIDA Y SU WINNER  */
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="id_player")
	private Player winnerPlayer;
	
	
	//El constructor vacío ya va con valores por defecto que pide la práctica.
	protected Game() {
		super();
		this.dateStart = LocalDateTime.now();
		this.oxygen = 0;
		this.temperature = -30;
		this.oceans = 0;
	}	

	public int getIdGame() {
		return idGame;
	}

	public int getOxygen() {
		return oxygen;
	}

	public int getTemperature() {
		return temperature;
	}

	public int getOceans() {
		return oceans;
	}

	public LocalDateTime getDateStart() {
		return dateStart;
	}

	public LocalDateTime getDateEnd() {
		return dateEnd;
	}


	protected void setIdGame(int idGame) {
		this.idGame = idGame;
	}

	protected void setOxygen(int oxygen) {
		this.oxygen = oxygen;
	}

	protected void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	protected void setOceans(int oceans) {
		this.oceans = oceans;
	}

	protected void setDateStart(LocalDateTime dateStart) {
		this.dateStart = dateStart;
	}

	protected void setDateEnd(LocalDateTime dateEnd) {
		this.dateEnd = dateEnd;
	}

	public Player getWinnerPlayer() {
		return winnerPlayer;
	}

	protected void setWinnerPlayer(Player winnerPlayer) {
		this.winnerPlayer = winnerPlayer;
	}

		
	public Set<Player> getPlayersInGame() {
		return playersInGame;
	}

	protected void setPlayersInGame(Set<Player> playersInGame) {
		this.playersInGame = playersInGame;
	}

	@Override
	public String toString() {
		return "Game [idGame=" + idGame + ", oxygen=" + oxygen + ", temperature=" + temperature + ", oceans=" + oceans
				+ ", dateStart=" + dateStart + ", dateEnd=" + dateEnd + ", winnerPlayer=" + winnerPlayer + "]";
	}

}
