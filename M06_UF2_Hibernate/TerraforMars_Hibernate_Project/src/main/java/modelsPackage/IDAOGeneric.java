package modelsPackage;

import java.io.Serializable;
import java.util.List;

public interface IDAOGeneric<T, ID extends Serializable> {
	void persist(T entity);

	T searchById(/* T entity, */ID id); // quizás sólo id?

	List<T> listAll();

	void deleteById(ID id);

	void deleteEqual(Object object);

}
