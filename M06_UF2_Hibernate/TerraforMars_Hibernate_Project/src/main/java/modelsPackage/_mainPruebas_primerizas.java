package modelsPackage;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class _mainPruebas_primerizas {
	
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;
	
	public static synchronized SessionFactory getSessionFactory() 
	{
		try 
		{
			if(sessionFactory == null) 
			{
				StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
				Metadata metaData  = new MetadataSources(standardRegistry).getMetadataBuilder().build();
				sessionFactory = metaData.getSessionFactoryBuilder().build();
			}
			return sessionFactory;
			
		} 
		catch (Throwable ex) 
		{
			throw new ExceptionInInitializerError(ex);
		}
	}
	public static void main(String[] args) {

		//Hem d'obrir la sessio fent servir el getSessionFactory();
		session = getSessionFactory().openSession();
		//Totes les operacions a la BBDD s'han de fer amb una transacció (obrim i fem commit)
		session.beginTransaction();
		
		
		Game partida = new Game();
		session.persist(partida);
		
//		session.persist(t1);
		
		Player p1 = new Player("Martí");
		Player p2 = new Player("David");
		Player p3 = new Player("Manu");	
		
		session.persist(p1);
		session.persist(p2);
		session.persist(p3);

		session.getTransaction().commit();
		
		
		//
		session.beginTransaction();
		Corporation c1 = new Corporation("Oscorp", "Oscorp Corporation");
		Corporation c2 = new Corporation("ToysRUS", "Juguetes para todos y todas");
		Corporation c3 = new Corporation("Industrias", "Industrias de cosas");
		session.persist(c1);
		session.persist(c2);
		session.persist(c3);	
		session.getTransaction().commit();
		
//		session.persist(t1);
		session.beginTransaction();
		Maker m1 = new Maker("casilla1", 5, Typemaker.CIUTAT);
		Maker m2 = new Maker("casilla2", 3, Typemaker.JUNGLA);
		Maker m3 = new Maker("casilla3", 4, Typemaker.OCEA);		
		session.getTransaction().commit();
		
		
		session.beginTransaction();
		p1.setCorporation(c2);
		session.merge(p1);
		session.getTransaction().commit();
		
		
		session.beginTransaction();		
		m2.setCorporation(c3);
		session.merge(m2);		
		session.getTransaction().commit();
		
				
	}

}
