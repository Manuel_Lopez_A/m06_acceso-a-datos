package mainPackage;

import java.util.List;

import dao.DAOAliment;
import dao.DAOJoguina;
import dao.DAOTamagotchi;
import modelsPackage.Aliment;
import modelsPackage.Etapa;
import modelsPackage.Joguina;
import modelsPackage.Tamagotchi;

public class MainActivitat3 {
		
	static DAOTamagotchi tamagotchi = new DAOTamagotchi();
	static DAOAliment aliment = new DAOAliment();
	static DAOJoguina joguina = new DAOJoguina();
	
	public static void main(String[] args) {
		
		// los generamos y los insertamos directamente
		tamagotchi.generateTamagotchi("Marla", "Perrita vieja", 1, false, Etapa.Adult, 50);
		tamagotchi.generateTamagotchi("Tina", "Perrita juguetona", 210, true, Etapa.Adult, 40);
		tamagotchi.generateTamagotchi("Pou", "Extraño ser", 100, true, Etapa.Egg, 60);
		tamagotchi.generateTamagotchi("ThunderBird", "Correo o pájaro", 250, true, Etapa.Kid, 30);		
		
		//pongo 2 de cada para darle variedad
		aliment.generateAliment("poma", "las manzanas como hacen para tener más manzanas?", 50.55);
		aliment.generateAliment("xocolata", "del de comer", 200.90);
		Aliment a3 = aliment.returnAliment("pera", "perita en dulce", 45.35);
		Aliment a4 = aliment.returnAliment("pollastre", "pio pio", 160.5);
		// cambiamos el valor del Chocolate
		aliment.updateValorNutricional(299.99, 2);
		
		// dales sus alimentos...
		tamagotchi.equipaAliment(1, a4);
		tamagotchi.equipaAliment(2, a4);
		
		List<Tamagotchi> lista = tamagotchi.listAll();			
		System.out.println("LISTA");
		for (Tamagotchi tamagotchi : lista) 
			System.out.println(tamagotchi); 

		// muestra todos los tamagotchis que tienen el alimento a4 (pollo)
		aliment.selectByAliment(a4);
		// I'm alive?
		tamagotchi.esticViu(1);
		
		
		// para crear una JOGUINA tenemos dos funciones:
		Joguina j1 = joguina.returnJoguina("Cuerda", "no hagas maldades", 25);
		joguina.generateJoguina("pelota", "redonda", 5); 
		// cambia la diversion
		joguina.updateDiversio(j1.getId(), 50);
		
		Tamagotchi t1 = tamagotchi.searchById(1);
		Tamagotchi t2 = tamagotchi.searchById(2);
		Tamagotchi t3 = tamagotchi.searchById(3);
		
		// equipa una cosa u otra (hagamos una de cada)
//		tamagotchi.equipaAlimentOJoguina(t1, j1);
		tamagotchi.equipaJoguina(t1, j1);
		tamagotchi.equipaAlimentOJoguina(t3, a4);	
		
		
		/*                         */System.out.println(); System.out.println();
		Joguina juguete = joguina.searchById(1);
		System.out.println(juguete);
		System.out.println(juguete.getTama());
		/*                         */System.out.println(); System.out.println();
		
		tamagotchi.utilitzaJoguina(tamagotchi.searchById(t1.getId()));
		// por id de tamagotchi
		tamagotchi.menjar(1);
		tamagotchi.menjar(5);

		/* */
		tamagotchi.socializar(t1, t2);
		
		
		tamagotchi.llistarAmics(t1);
		
//		Me parece que tiene más sentido que para simular la espontaneidad en la función sea de todos los tamagotchis
//		pero está de las dos maneras--------->
//		tamagotchi.menjarInExtremis(t1);
		tamagotchi.menjarInExtremisAll();
		
		
	}
}
