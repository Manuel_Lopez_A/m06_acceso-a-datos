package mainPackage;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import modelsPackage.Aliment;
import modelsPackage.Etapa;
import modelsPackage.Joguina;
import modelsPackage.Tamagotchi;

public class MainActivitat2 {
	
	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;
	
	public static synchronized SessionFactory getSessionFactory() 
	{
		try 
		{
			if(sessionFactory == null) 
			{
				StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
				Metadata metaData  = new MetadataSources(standardRegistry).getMetadataBuilder().build();
				sessionFactory = metaData.getSessionFactoryBuilder().build();
			}
			return sessionFactory;
			
		} 
		catch (Throwable ex) 
		{
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public static void main(String[] args) 
	{
		//Hem d'obrir la sessio fent servir el getSessionFactory();
		session = getSessionFactory().openSession();
		//Totes les operacions a la BBDD s'han de fer amb una transacció (obrim i fem commit)
		session.beginTransaction();

//		Inicia una transacció on es fa la creació de quatre objectes Tamagotchi amb dades de prova i 
//		guarda’ls a la bbdd. Fes el mateix amb quatre objectes Aliment (els noms han de ser: 
//		{“poma”, ”xocolata”, ”pera” i ”pollastre”}).		
		Tamagotchi t1 = new Tamagotchi("Marla", "Perrita vieja", 10.56, false, Etapa.Adult, 50);
		Tamagotchi t2 = new Tamagotchi("Tina", "Perrita juguetona", 15.56, true, Etapa.Adult, 40);
		Tamagotchi t3 = new Tamagotchi("Pou", "Extraño ser", 11, true, Etapa.Egg, 60);
		Tamagotchi t4 = new Tamagotchi("ThunderBird", "Correo o pájaro", 9, true, Etapa.Kid, 50);
		session.persist(t1);
		session.persist(t2);
		session.persist(t3);
		session.persist(t4);
		Aliment a1 = new Aliment("poma", "las manzanas como hacen para tener más manzanas?", 50.55);
		Aliment a2 = new Aliment("xocolata", "del de comer", 200.90);
		Aliment a3 = new Aliment("pera", "perita en dulce", 45.35);
		Aliment a4 = new Aliment("pollastre", "pio pio", 160.5);
		session.persist(a1);
		session.persist(a2);
		session.persist(a3);
		session.persist(a4);
		session.getTransaction().commit();
		
		
		
//		Fes que els Tamagotchis es facin amics entre ells (com a mínim tots han de tenir un amic). 
//		Guarda les dades actualitzades i 
		session.beginTransaction();
		t1.getAmics().add(t4);
		t2.getAmics().add(t4);
		t3.getAmics().add(t2);
		t4.getAmics().add(t1);
//		t4.getAmics().add(t4); // se puede poner de amigo de él mismo...
		session.getTransaction().commit();
		
		
		
//		fes un print de tots els registres de Tamagotchi
		session.beginTransaction();
		List<Tamagotchi> misTamagochis = session.createQuery("from Tamagotchi").getResultList();		
		System.out.println(); System.out.println("* HAGO UN PRINT DE TODOS LOS TAMAGOTCHIS *");
		for (Tamagotchi tamagotchi : misTamagochis) {
			System.out.println("---> "+ tamagotchi.toString());
		} 
		session.getTransaction().commit();
		
		
		
//		El Tamagotchi amb ID 1 haurà de rebre l’aliment “poma”. El Tamagotchi amb ID 2 haurà de 
//		rebre l’Aliment “xocolata”. Comprova que al Tamagotchi amb ID 3 també pot rebre 
//		l’Aliment “xocolata”. Guarda les dades actualitzades.
		session.getTransaction().begin();		
		Tamagotchi tamaDale1 = session.find(Tamagotchi.class, 1);
		tamaDale1.setAliment(a1);
		Tamagotchi tamaDale2 = session.find(Tamagotchi.class, 2);
		tamaDale2.setAliment(a2);
		Tamagotchi tamaDale3 = session.find(Tamagotchi.class, 3);
		tamaDale3.setAliment(a2);		
		session.merge(tamaDale1);
		session.merge(tamaDale2);
		session.merge(tamaDale3);
		session.getTransaction().commit();
		
		
		
		
//		Obté tots els registres de Tamagotchis i fes un print del nom del seu aliment associat.
		/* En realidad no cal ni que empiece una transaction, puedo coger la lista anterior a asignarles alimentos
		 * y hacer un foreach syso de ella... aparecen los alimentos. Y además no hay que hacer BEGIN ni COMMIT
		 */
		session.beginTransaction();
		List<Tamagotchi>tamagochis = session.createQuery("from Tamagotchi").getResultList();
		System.out.println(); System.out.println("* TAMAGOTCHIS + SU ALIMENTO (sólo sale quien tiene alimento)");
		for (Tamagotchi tamagotchi : misTamagochis) {
			if(tamagotchi.getAliment()!=null) {
				System.out.println("--> Soy el Tamagotchi " + tamagotchi.getNom() + 
						" y tengo este alimento: "+tamagotchi.getAliment().getNom());
			}
		}
		session.getTransaction().commit();
		
		
		
		
//		Ara genera dos objectes Joguina (els noms han de ser: {“pilota”, “hotwheels”}) i assigna’ls als Tamagotchi amb ID 1 i 2 respectivament.
		session.beginTransaction();
		Joguina j1 = new Joguina("pilota", "La de toda la vida, redonda...", 6);
		Joguina j2 = new Joguina("hotwheels", "Coche en miniatura", 7);
		session.persist(j1);
		session.persist(j2);
		tamaDale1.setJoguina(j1);
		tamaDale2.setJoguina(j2);
		session.getTransaction().commit();
		//System.out.println(tamaDale1.toString());
		
		
//		Fes la persistencia i desprès modifica la joguina del tamagotchi amb ID 1 per la joguina del tamagotchi amb ID 2. 
		//NO DEJA!
//		session.getTransaction();
//		tamaDale1.setJoguina(j2);
//		tamaDale2.setJoguina(j1);
//		session.getTransaction().commit();
		
		
		
//		Consultar  tots els tamagotchis de la base de dades i mostrar les dades per consola. 		
//		Si en tenen, fes un print del nom de la joguina i l’aliment assignat.
		/*
		 *COJO LA PRIMERA LISTA QUE USÉ...sin beginTransaction ni commits ni nada 
		 */
		System.out.println(); System.out.println("* TAMAGOTCHI Y SUS ALIMENTOS O JUGUETES: ");
		for (Tamagotchi tamagotchi : misTamagochis) {
			System.out.println("--->" + tamagotchi.toString());
			if(tamagotchi.getAliment()!=null)System.out.println("[+] alimento: "+ tamagotchi.getAliment().getNom());
			if(tamagotchi.getJoguina()!=null)System.out.println("[+] juguete: "+ tamagotchi.getJoguina().getNom());
		}
		
		
		session.close();
	}

}
