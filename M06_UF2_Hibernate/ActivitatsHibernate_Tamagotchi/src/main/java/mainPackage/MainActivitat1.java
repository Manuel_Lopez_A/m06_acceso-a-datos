package mainPackage;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import modelsPackage.Etapa;
import modelsPackage.Tamagotchi;

public class MainActivitat1 {

	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;
	
	public static synchronized SessionFactory getSessionFactory() 
	{
		try {
			if(sessionFactory == null) {
				StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
				Metadata metaData  = new MetadataSources(standardRegistry).getMetadataBuilder().build();
				sessionFactory = metaData.getSessionFactoryBuilder().build();
			}
			return sessionFactory;
			
		} 
		catch (Throwable ex) 
		{
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public static void main(String[] args) 
	{
		//Hem d'obrir la sessio fent servir el getSessionFactory();
		session = getSessionFactory().openSession();
		//Totes les operacions a la BBDD s'han de fer amb una transacció (obrim i fem commit)
		session.beginTransaction();
		
//		01 - CREAR DOS TAMAGOTCHI
		System.out.println("01 - CREAR DOS TAMAGOTCHI");
		Tamagotchi tama01 = new Tamagotchi();
		tama01.setNom("Marla");
		tama01.setEtapa(Etapa.Baby);
		tama01.setFelicitat(100);
		Tamagotchi tama02 = new Tamagotchi("Tina", "Es una perrocha", Etapa.Baby);
		
		session.persist(tama01);
		session.persist(tama02);
			
		session.getTransaction().commit();

//		02 - RECUPERA EL ID1 I MOSTRAR PER PANTALLA
		System.out.println(); System.out.println("02 - RECUPERA EL ID1 I MOSTRAR PER PANTALLA");
		session.beginTransaction();		
		Tamagotchi tamaFind = session.find(Tamagotchi.class, 1);
		System.out.println(tamaFind.toString());				
		session.getTransaction().commit();
		
//		03 - MODIFICA EL ID1 I CANVIA EL NOM A UPDATEGOTCHI
		System.out.println(); System.out.println("03 - MODIFICA EL ID1 I CANVIA EL NOM A UPDATEGOTCHI");
		
		session.beginTransaction();
		tamaFind.setNom("UpdateGotchi");
		session.merge(tamaFind);
		session.getTransaction().commit();	
		
//		04 - CONSULTA TODOS LOS TAMAGOTCHIS
		System.out.println(); System.out.println("04 - CONSULTA TODOS LOS TAMAGOTCHIS");
		
		session.getTransaction().begin();     				/* CLASE, NO TABLA */
		List<Tamagotchi> misTamagotchis = session.createQuery("from Tamagotchi").getResultList();
		System.out.println(misTamagotchis);
		session.getTransaction().commit();	
		
//		05 - BORRA L'OBJECTE 1
		System.out.println(); System.out.println("05 - BORRA L'OBJECTE 1");
		
		session.getTransaction().begin();
		session.remove(session.find(Tamagotchi.class, 1));
		session.getTransaction().commit();
		
//		06 - CONSULTA TODOS LOS TAMAGOTCHIS y 07 - CIERRA
		System.out.println(); System.out.println("06 - CONSULTA TODOS LOS TAMAGOTCHIS y 07 - CIERRA");	
		
		session.getTransaction().begin();     				 /* CLASE, NO TABLA */
		List<Tamagotchi> misTamagotchis2 = session.createQuery("from Tamagotchi").getResultList();
		System.out.println(misTamagotchis2);
		session.getTransaction().commit();	
		
		session.close();
	}

}
