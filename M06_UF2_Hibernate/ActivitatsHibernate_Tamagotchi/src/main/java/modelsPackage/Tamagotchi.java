package modelsPackage;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tamagotchi")
public class Tamagotchi {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idTamagotchi")
	int id;
	@Column(name = "nom", length = 50)
	String nom;
	@Column(name = "descripcio", length = 50)
	String descripcio;
	@Column(name = "gana", nullable = false, columnDefinition = "double(6,2)")
	double gana = 50.00;
	@Column
	boolean viu = true;
	@Enumerated(EnumType.STRING)
	@Column(name = "etapa")
	Etapa etapa;
	@Column(name = "felicitat", nullable = false, columnDefinition = "int")
	int felicitat = 50;
	@Column
	LocalDateTime dataNaixement;
	
	
/*	                             RELACIONES.....                                */
	// 1 - 1 Esta clase lleva el peso de la relación
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinColumn(name="id_joguina", unique = true)
	private Joguina joguina;
	
	
	//N - 1   tamagotchi - aliment  ---> Esta clase lleva el peso de la relación
	@ManyToOne /* (fetch = FetchType.EAGER) */
	@JoinColumn(name="id_aliment")
	private Aliment alimentDelTamagotchi;	
	
	// N - M
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "amics")	
	private Set<Tamagotchi> amicsDe = new HashSet<Tamagotchi>();	
	@ManyToMany(fetch = FetchType.EAGER) 
	//Amb el @JoinTable indiquem les propietats de la taula intermitja. 
	// joinColumn -> PK propia de la classe que mapeja, 
	// inverseJoinColumn -> PK de l'altra entitat de la relació
	@JoinTable(
			name="amistades_peligrosas", 
			joinColumns = @JoinColumn(name="id_tamagotchi1"), 
			inverseJoinColumns = @JoinColumn(name="id_tamagotchi2")
	)
	private Set<Tamagotchi> amics = new HashSet<Tamagotchi>();	


	

	
	
	
	
	public Tamagotchi() {
		super();
		dataNaixement = LocalDateTime.now();
	}

	public Tamagotchi(String nom, String descripcio, Etapa etapa) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.etapa = etapa;
	}

	// El id es autonumerico
	public Tamagotchi(String nom, String descripcio, double gana, boolean viu, Etapa etapa, int felicitat) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.gana = gana;
		this.viu = viu;
		this.etapa = etapa;
		this.felicitat = felicitat;
	}

	public Aliment getAlimentDelTamagotchi() {
		return alimentDelTamagotchi;
	}

	public void setAlimentDelTamagotchi(Aliment alimentDelTamagotchi) {
		this.alimentDelTamagotchi = alimentDelTamagotchi;
	}

	public Set<Tamagotchi> getAmics() {
		return amics;
	}

	public void setAmics(Set<Tamagotchi> amics) {
		this.amics = amics;
	}

	public Joguina getJoguina() {
		return joguina;
	}

	public void setJoguina(Joguina joguina) {
		this.joguina = joguina;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public double getGana() {
		return gana;
	}

	public void setGana(double gana) {
		this.gana = gana;
	}

	public boolean isViu() {
		return viu;
	}

	public void setViu(boolean viu) {
		this.viu = viu;
	}

	public Etapa getEtapa() {
		return etapa;
	}

	public void setEtapa(Etapa etapa) {
		this.etapa = etapa;
	}

	public int getFelicitat() {
		return felicitat;
	}

	public void setFelicitat(int felicitat) {
		this.felicitat = felicitat;
	}

	public LocalDateTime getDataNaixement() {
		return dataNaixement;
	}

	public void setDataNaixement(LocalDateTime dataNaixement) {
		this.dataNaixement = dataNaixement;
	}

	public Aliment getAliment() {
		return alimentDelTamagotchi;
	}

	public void setAliment(Aliment aliment) {
		this.alimentDelTamagotchi = aliment;
	}

	protected Set<Tamagotchi> getAmicsDe() {
		return amicsDe;
	}

	protected void setAmicsDe(Set<Tamagotchi> amicsDe) {
		this.amicsDe = amicsDe;
	}
	
	@Override
	public String toString() {
		return "Tamagotchi [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", gana=" + gana + ", viu="
				+ viu + ", etapa=" + etapa + ", felicitat=" + felicitat + ", dataNaixement=" + dataNaixement + "]";
	}

}
