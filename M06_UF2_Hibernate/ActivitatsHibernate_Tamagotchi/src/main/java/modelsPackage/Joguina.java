package modelsPackage;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "joguines")
public class Joguina {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idJoguina")
	int id;
	@Column(name = "nomJoguina", length = 50, nullable = false)
	String nom;
	@Column(name = "descJoquina", length = 50, nullable = false)
	String descripcio;
	@Column(name="nivellDiversio", columnDefinition = "int")
	int nivellDiversio = 5;

	// le tenemos que hacer la referencia al tamagotchi que lo tendrá (TAMAGOTCHI lleva el peso de la rel)
	@OneToOne(mappedBy = "joguina", cascade = CascadeType.PERSIST)
	private Tamagotchi tama;
	
	
	
	public Tamagotchi getTama() {
		return tama;
	}

	public void setTama(Tamagotchi tama) {
		this.tama = tama;
	}

	public Joguina() {
	}

	public Joguina(String nom, String descripcio, int nivellDiversio) {
		super();
		this.nom = nom;
		this.descripcio = descripcio;
		this.nivellDiversio = nivellDiversio;
	}

	@Override
	public String toString() {
		return "Joguina [id=" + id + ", nom=" + nom + ", descripcio=" + descripcio + ", nivellDiversio="
				+ nivellDiversio + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public int getNivellDiversio() {
		return nivellDiversio;
	}

	public void setNivellDiversio(int nivellDiversio) {
		this.nivellDiversio = nivellDiversio;
	}

}
