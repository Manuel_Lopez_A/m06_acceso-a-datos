package dao;

import modelsPackage.Joguina;

public class DAOJoguina extends DAOGeneric<Joguina, Integer> {

	public DAOJoguina() {
		super(Joguina.class);
	}

	public void generateJoguina(String nom, String descripcio, int nivellDiversio) {
		Joguina j = new Joguina(nom, descripcio, nivellDiversio);
		persist(j);
	}
	
	public Joguina returnJoguina(String nom, String descripcio, int nivellDiversio) {
		Joguina j = new Joguina(nom, descripcio, nivellDiversio);
		persist(j);
		return j;
	}

	public void updateDiversio(Integer id, int diversio) {
		
		Joguina j = searchById(id);
		int oldDiversio = j.getNivellDiversio();
		j.setNivellDiversio(diversio);
		updateEntity(j);
		System.out.println("[UPDATE]"+ j.getNom() + " canvia el seu nivell de diversió de "+ oldDiversio+ " a " + diversio );
	}

}
