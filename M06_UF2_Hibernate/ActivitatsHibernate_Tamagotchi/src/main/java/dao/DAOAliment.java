package dao;

import java.util.Set;

import modelsPackage.Aliment;
import modelsPackage.Tamagotchi;

public class DAOAliment extends DAOGeneric<Aliment, Integer> {
	
	public DAOAliment() {		super(Aliment.class);	}
//	public DAOAliment(Class<Aliment> entityClass) 
//	{  super(entityClass);  } 
	
	
	public void generateAliment(String nom, String descripcio, double valorNutricional) {
		Aliment a = new Aliment(nom, descripcio, valorNutricional);
		persist(a);
		System.out.println("INSERT OK! : " + a.toString());
	}
	
	public Aliment returnAliment(String nom, String descripcio, double valorNutricional) {
		Aliment a = new Aliment(nom, descripcio, valorNutricional);
		persist(a);
		System.out.println("INSERT OK! : " + a.toString());
		return a;
	}
//	Han de tenir un mètode propi que els permeti llistar tots els Tamagotchi que existeixen amb 
//	un aliment del tipus demanat.
	public void selectByAliment(Aliment aliment) {
		
		Aliment a = searchById(aliment.getId());
		System.out.println("** Tamagotchis que tienen "+ aliment.getNom());
		
		Set<Tamagotchi> lista = a.getTamagotchis();	
		for (Tamagotchi tamagotchi : lista) {
			System.out.println(tamagotchi.getNom());
		}
	}
	
//	Han de tenir un mètode propi que els permeti modificar el seu valorNutricional.
	public void updateValorNutricional(double valorNutr, int id ) {
		Aliment a = searchById(id);	
		double antic = a.getValorNutricional();
		a.setValorNutricional(valorNutr);
		updateEntity(a);
		System.out.println("[UPDATE]"+ a.getNom() + " canvia el seu valor nutricional de "+ antic + " a " + valorNutr );
	}


}
