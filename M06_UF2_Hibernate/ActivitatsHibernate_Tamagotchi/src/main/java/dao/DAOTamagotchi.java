package dao;

import java.util.List;
import java.util.Set;

import modelsPackage.Aliment;
import modelsPackage.Etapa;
import modelsPackage.Joguina;
import modelsPackage.Tamagotchi;

public class DAOTamagotchi extends DAOGeneric<Tamagotchi, Integer> {

	public DAOTamagotchi() {
		super(Tamagotchi.class);
	}

	public void generateTamagotchi(String nom, String descripcio, Etapa etapa) {
		Tamagotchi t = new Tamagotchi(nom, descripcio, etapa);
		persist(t);
		System.out.println("INSERT OK! : " + t.toString());
	}

	public Tamagotchi returnTamagotchi(String nom, String descripcio, Etapa etapa) {
		Tamagotchi t = new Tamagotchi(nom, descripcio, etapa);
		persist(t);
		System.out.println("INSERT OK! : " + t.toString());
		return t;
	}

	public void generateTamagotchi(String nom, String descripcio, double gana, boolean viu, Etapa etapa,
			int felicitat) {
		Tamagotchi t = new Tamagotchi(nom, descripcio, gana, viu, etapa, felicitat);
		persist(t);
		System.out.println("INSERT OK! : " + t.toString());
	}

	public Tamagotchi returnTamagotchi(String nom, String descripcio, double gana, boolean viu, Etapa etapa,
			int felicitat) {
		Tamagotchi t = new Tamagotchi(nom, descripcio, gana, viu, etapa, felicitat);
		persist(t);
		System.out.println("INSERT OK! : " + t.toString());
		return t;
	}

//	Han de tenir un mètode per equipar una joguina a un Tamagotchi per PK.
	public void equipaJoguina(int id, Joguina joguina) {
		Tamagotchi t = searchById(id);
		t.setJoguina(joguina);
		System.out.println("--> Se ha equipado a " + t.getNom() + " con la joguina " + joguina.getNom());
		updateEntity(t);
	}
	public void equipaJoguina(Tamagotchi t, Joguina joguina) {
		t.setJoguina(joguina);
		System.out.println("--> Se ha equipado a " + t.getNom() + " con la joguina " + joguina.getNom());
		updateEntity(t);
	}

//	Han de tenir un mètode per equipar un aliment a un Tamagotchi per PK
	public void equipaAliment(int id, Aliment aliment) {
		Tamagotchi t = searchById(id);
		t.setAliment(aliment);
		System.out.println("--> Se ha equipado a " + t.getNom() + " con el alimento " + aliment.getNom());
		updateEntity(t);
	}

//	Han de tenir un mètode per comprovar si estan vius o no.
	public void esticViu(int id) {
		Tamagotchi tam = searchById(id);
		System.out.println("** " + tam.getNom() + " està viu?");
		System.out.println(tam.isViu() ? "- Estic viu" : "- " + tam.getNom() + " està mort");
	}

//	Han de tenir un mètode propi per assignar-se un Aliment i/o una Joguina.
	public void equipaAlimentOJoguina(Tamagotchi tama, Object o) {
		int id = tama.getId();
		if (o instanceof Joguina) {
			equipaJoguina(id, (Joguina) o);
		} else if (o instanceof Aliment) {
			equipaAliment(id, (Aliment) o);
		} else {
			System.out.println("Not supported Object");
		}
	}

//	Han de tenir un mètode per utilitzar la Joguina. Quan utilitzen la seva joguina, sumen a la seva
//	felicitat el valor de diversió de la joguina. Després, han de desvincular-se la joguina.
	public void utilitzaJoguina(Tamagotchi t) {
		System.out.println("** LA FELICIDAD");
		Tamagotchi tama = searchById(t.getId());
		System.out.println("- " + tama.getNom() + " tenía esta felicidad: " + tama.getFelicitat());
		tama.setFelicitat(tama.getFelicitat() + tama.getJoguina().getNivellDiversio());
		System.out.println("- se puso a jugar con su juguete " + tama.getJoguina().getNom() + " y ahora tiene esta felicidad: " + tama.getFelicitat());
		tama.setJoguina(null);
		updateEntity(tama);
	}

//	Han de tenir un mètode per alimentar-se. Quan utilitzen el seu aliment, resten a la seva gana 
//	el valor nutricional del menjar i es desvinculen. Pensa que la gana no pot ser negativa.
	public void menjar(int id) {
		
		Tamagotchi t = searchById(id);
		if (t == null)
			return;
		if (t.getAliment() == null) {
			System.out.println(t.getNom() + ": qué quieres que coma??? una piedra????");
			return;
		} else {
			System.out.println("- " + t.getNom() + " té una gana de " + t.getGana() + "...");
			t.setGana(t.getGana() - t.getAliment().getValorNutricional());
			System.out.println("- però menja el seu aliment " + t.getAliment().getNom() + " que té valor nutricional de "
					+ t.getAliment().getValorNutricional());
			if (t.getGana() < 0)
				t.setGana(0);
			System.out.println("- i ara té la següent gana: " + t.getGana());
			t.setAliment(null);
		}
		updateEntity(t);
	}

	
	
//	Han de tenir un mètode per poder fer-se amics entre ells.
	public void socializar(Tamagotchi t1, Tamagotchi t2) {
		System.out.println("** El poder de la amistad: ");
		Tamagotchi tama1 = searchById(t1.getId());
		Tamagotchi tama2 = searchById(t2.getId());
		//tama2.getAmics().add(tama1);
		tama1.getAmics().add(tama2);
		//updateEntity(tama2);
		updateEntity(tama1);
		System.out.println(tama1.getNom() + " y "+ tama2.getNom() + " ahora son amiguis.");
	}
	
	
	

//	Han de tenir un mètode per llistar tots els seus amics.
	public void llistarAmics(Tamagotchi t) {
		Tamagotchi tama = searchById(t.getId());
		Set<Tamagotchi> amics = tama.getAmics();
		System.out.println("** Llista d'amics de " + tama.getNom());
		for (Tamagotchi tamagotchi : amics) {
			System.out.println("- " + tamagotchi.getNom());
		}
	}

//	Han de tenir un mètode que, si la seva gana passa de 200, automàticament utilitzen el seu aliment. 
//	Si no tenen aliment equipat, moren.
	public void menjarInExtremis(Tamagotchi t) {
		System.out.println("** VEAMOS QUÉ TAL ANDAN DE HAMBRE NUESTRO TAMAGOTCHI");
		Tamagotchi tama = searchById(t.getId());
		if (tama.getGana() > 200) {
			if (tama.getAliment() == null) {
				tama.setViu(false);
				System.out.println("pobrecito..." + tama.getNom() + " tenía mucha hambre, no tenía comida y murió");
				updateEntity(tama);
			} else {
				System.out.println("menos mal que " + tama.getNom() + " tiene comida... se salvará de morir de hambre");
				menjar(tama.getId());
			}
		}
	}

	public void menjarInExtremisAll() {

		List<Tamagotchi> tamaList = listAll();
		System.out.println("** VEAMOS QUÉ TAL ANDAN DE HAMBRE NUESTROS TAMAGOTCHIS");
		for (Tamagotchi tama : tamaList) {
			if (tama.getGana() > 200) {
				if (tama.getAliment() == null) {
					tama.setViu(false);
					System.out.println("pobrecito..." + tama.getNom() + " tenía mucha hambre, no tenía comida y murió");
					updateEntity(tama);
				} else {
					System.out.println(
							"menos mal que " + tama.getNom() + " tiene comida... se salvará de morir de hambre");
					menjar(tama.getId());
				}
			} else {
				System.out.println(tama.getNom() + " no tiene tanta hambre...");
			}
		}
	}

}
