package com.example.demoDAMvi.Controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.example.demoDAMvi.Models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {
    
    
    @GetMapping(path = "/hello")
    public String hello() {
        return "hola ";
    }

    @GetMapping(path = "/test")
    public String test(@RequestParam String a, @RequestParam String b) {
        return "hola " + a + " y ";
    }
}
