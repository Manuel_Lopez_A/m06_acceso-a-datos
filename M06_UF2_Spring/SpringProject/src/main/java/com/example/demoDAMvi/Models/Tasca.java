package com.example.demoDAMvi.Models;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name="tasca")
public class Tasca {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tasca")	
	private int idTasca;
	@Column(name = "titol", nullable = false, length = 50)
	private String titol;
	@Column(name="dedicacio", columnDefinition = "double(6,2)")
	private double dedicacio = 0.0;
	@Column(name="recompensa", columnDefinition = "int")
	private int recompensa;
	@Enumerated(EnumType.STRING)
	@Column(name = "recompensa")
	private Recurs recurs;
	
	//M-N (CLÁSICO) - es mapeado por....
	@ManyToMany(mappedBy = "tasques", fetch = FetchType.EAGER)
	private Set<Habitant> habitants = new HashSet<>();


	//---------------------------------------------------//
	
	
	public Tasca() {
		super();
	}

	public Tasca(String titol, double dedicacio, int recompensa, Recurs recurs) {
		super();
		this.titol = titol;
		this.dedicacio = dedicacio;
		this.recompensa = recompensa;
		this.recurs = recurs;
	}

	public Tasca(String titol, double dedicacio, int recompensa, Recurs recurs, Set<Habitant> habitants) {
		super();
		this.titol = titol;
		this.dedicacio = dedicacio;
		this.recompensa = recompensa;
		this.recurs = recurs;
		this.habitants = habitants;
	}

	public String getTitol() {
		return titol;
	}

	public void setTitol(String titol) {
		this.titol = titol;
	}

	public double getDedicacio() {
		return dedicacio;
	}

	public void setDedicacio(double dedicacio) {
		this.dedicacio = dedicacio;
	}

	public int getRecompensa() {
		return recompensa;
	}

	public void setRecompensa(int recompensa) {
		this.recompensa = recompensa;
	}

	public Recurs getRecurs() {
		return recurs;
	}

	public void setRecurs(Recurs recurs) {
		this.recurs = recurs;
	}

	public Set<Habitant> getHabitants() {
		return habitants;
	}

	public void setHabitants(Set<Habitant> habitants) {
		this.habitants = habitants;
	}

	public int getIdTasca() {
		return idTasca;
	}

	@Override
	public String toString() {
		return "Tasca [idTasca=" + idTasca + ", titol=" + titol + ", dedicacio=" + dedicacio + ", recompensa="
				+ recompensa + ", recurs=" + recurs + ", habitants=" + habitants + "]";
	}
	
	
	
}
