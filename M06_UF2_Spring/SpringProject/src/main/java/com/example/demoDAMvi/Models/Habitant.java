package com.example.demoDAMvi.Models;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="habitant")
public class Habitant {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_habitant")
	private int idHabitant;
	@Column(name = "nom", nullable = false, length = 35)
	private String nom;
	@Enumerated(EnumType.STRING)
	@Column(name = "especie")
	private Especie especie;
	@Column(name = "energia", columnDefinition = "int")
	private int energia = 100;

	// N-M (CLÁSICA) - lleva el peso de la relación
	@ManyToMany(fetch = FetchType.EAGER) // ???
	@JoinTable(name = "habitant_tasca", joinColumns = @JoinColumn(name = "id_habitant"), inverseJoinColumns = @JoinColumn(name = "id_tasca"))
	private Set<Tasca> tasques = new HashSet<>();

	// ---------------------------------------------------//

	public Habitant() {
		super();
	}
	
	public Habitant(String nom, Especie especie) {
		super();
		this.nom = nom;
		this.especie = especie;
	}

	public Habitant(String nom, Especie especie, Set<Tasca> tasques) {
		super();
		this.nom = nom;
		this.especie = especie;
		this.tasques = tasques;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Especie getEspecie() {
		return especie;
	}

	public void setEspecie(Especie especie) {
		this.especie = especie;
	}

	public int getEnergia() {
		return energia;
	}

	public void setEnergia(int energia) {
		this.energia = energia;
	}

	public Set<Tasca> getTasques() {
		return tasques;
	}

	public void setTasques(Set<Tasca> tasques) {
		this.tasques = tasques;
	}

	public int getIdHabitant() {
		return idHabitant;
	}

	@Override
	public String toString() {
		return "Habitant [idHabitant=" + idHabitant + ", nom=" + nom + ", especie=" + especie + ", energia=" + energia
				+ ", tasques=" + tasques + "]";
	}
	
	

}
