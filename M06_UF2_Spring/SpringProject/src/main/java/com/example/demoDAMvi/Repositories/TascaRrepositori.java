package com.example.demoDAMvi.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demoDAMvi.Models.Tasca;

public interface TascaRrepositori extends JpaRepository<Tasca, Integer> {
    List<Tasca> findByName(String nom);
    Tasca findTascaById(int id);
}
