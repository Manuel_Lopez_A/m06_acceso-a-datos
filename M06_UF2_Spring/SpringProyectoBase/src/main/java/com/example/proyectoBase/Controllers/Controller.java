package com.example.proyectoBase.Controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {
    
    
    @GetMapping(path = "/hello")
    public String hello() {
        return "TUPUTAMADRE";
    }

    @GetMapping(path = "/test")
    public String test(@RequestParam String a, @RequestParam String b) {
        return "hola " + a + " y ";
    }
}
