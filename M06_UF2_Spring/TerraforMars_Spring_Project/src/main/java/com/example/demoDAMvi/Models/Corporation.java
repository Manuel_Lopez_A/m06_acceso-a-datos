package com.example.demoDAMvi.Models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "corporations")
public class Corporation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_corporation")
	private int idCorporation;
	@Column(name = "name", nullable = false, length = 50)
	private String name;
	@Column(name = "description", length = 100)
	private String description;
	@Column(name = "victoryPoints", nullable = false, length = 50)
	private int victoryPoints;

	// le tenemos que hacer la referencia a la variable que está en la entidad que
	// lleva el peso de l mapeo
	@OneToOne(mappedBy = "corporation", cascade = CascadeType.PERSIST)
	private Player player;

//	Una corporació aconsegueix moltes caselles i només són d’aquesta corporació.
	@OneToMany(mappedBy = "corporation", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, fetch = FetchType.EAGER)
	private Set<Maker> makers = new HashSet<Maker>();

	public Corporation() {
		super();
	}

	public Corporation(String name) {
		super();
		this.name = name;
	}

	public Corporation(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	public Corporation(String name, String description, int victoryPoints) {
		super();
		this.name = name;
		this.description = description;
		this.victoryPoints = victoryPoints;
	}

	public int getIdCorporation() {
		return idCorporation;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public int getVictoryPoints() {
		return victoryPoints;
	}

	public Player getPlayer() {
		return this.player;
	}

	public Set<Maker> getMakers() {
		return makers;
	}

	public void setIdCorporation(int idCorporation) {
		this.idCorporation = idCorporation;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setVictoryPoints(int victoryPoints) {
		this.victoryPoints = victoryPoints;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void setMakers(Set<Maker> makers) {
		this.makers = makers;
	}

	public String playerName() {
		return player != null ? player.getName() : "aquesta corporation no te player";
	}

	public List<Integer> ids() {
		List<Integer> i = new ArrayList<>();
		for (Maker m : makers) {
			i.add(m.getIdMaker());
		}
		return i;
	}

	@Override
	public String toString() {
		return "Corporation [idCorporation=" + idCorporation + ", name=" + name + ", description=" + description
				+ ", victoryPoints=" + victoryPoints + ", player=" + playerName() + ", makers=" + ids() + "]";
	}

}
