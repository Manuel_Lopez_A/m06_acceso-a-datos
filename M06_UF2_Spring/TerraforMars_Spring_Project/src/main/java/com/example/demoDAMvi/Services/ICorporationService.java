package com.example.demoDAMvi.Services;

import java.util.List;

import com.example.demoDAMvi.Models.Corporation;
import com.example.demoDAMvi.Models.Player;
import com.example.demoDAMvi.Models.Resultat;

public interface ICorporationService {
	List<Corporation> findByName(String nom);
	Corporation findByIdCorporation(int id);
	List<String> getCorporationsWithPlayer();
	List<String> setVictoryPointsMakers();
}
