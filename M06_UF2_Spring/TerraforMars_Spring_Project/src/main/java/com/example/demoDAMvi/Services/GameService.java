package com.example.demoDAMvi.Services;

import com.example.demoDAMvi.Models.*;
import com.example.demoDAMvi.Repositories.CorporationRepository;
import com.example.demoDAMvi.Repositories.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("GameService")
public class GameService implements IGameService {

    @Autowired
    GameRepository gr;
    @Autowired
    MakerService ms;
    @Autowired
    CorporationService cs;

    @Override
    public Resultat isEndedGame() {
        Game game = gr.findAll().get(0);
        int assolits = 0;
        if (game.getTemperature() == 0) {
            assolits++;
        }
        if (game.getOxygen() == 14) {
            assolits++;
        }
        boolean flag = false;
        for (Maker m : ms.findAll()) {
            if (m.getTypemaker() == Typemaker.OCEA && m.getCorporation() != null) {
                flag = true;
                break;
            }
        }
        if (flag) assolits++;

        if (assolits >= 2) return new Resultat("Partida acabada");
        else return new Resultat("Partida en progres");
    }

    @Override
    public Resultat setWinnerGame(){
        Game game = gr.findAll().get(0);
        List<Corporation> corpos = cs.findAll();
        int max = Integer.MIN_VALUE;
        Corporation corpoGuanyadora = null;
        for (int i = 0; i < corpos.size(); i++) {
            if (corpos.get(i).getVictoryPoints() > max) {
                max = corpos.get(i).getVictoryPoints();
                corpoGuanyadora = corpos.get(i);
            }
        }
        game.setWinnerPlayer(corpoGuanyadora.getPlayer());
        return new Resultat("El guanyador és " + corpoGuanyadora.getPlayer().getName());
    }
}
