package com.example.demoDAMvi.Services;

import java.util.ArrayList;
import java.util.List;

import com.example.demoDAMvi.Models.Resultat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demoDAMvi.Models.Corporation;
import com.example.demoDAMvi.Repositories.CorporationRepository;

@Service("CorporationService")
public class CorporationService implements ICorporationService {

	@Autowired
	CorporationRepository cr;

	public List<Corporation> findAll() {
		return cr.findAll();
	}

	@Override
	public List<Corporation> findByName(String nom) {
		return cr.findByName(nom);
	}

	@Override
	public Corporation findByIdCorporation(int id) {
		return cr.findByIdCorporation(id);
	}

	@Override
	public List<String> setVictoryPointsMakers() {
		List<Corporation> corps = cr.findAll();
		List<String> corpVictoryPoint = new ArrayList<>();
		for(Corporation c : corps) {
			c.setVictoryPoints(c.getMakers().size() * 2);
			corpVictoryPoint.add("Corporacio: " + c.getName() +", VictoryPoints: " + c.getVictoryPoints());
		}
		cr.saveAll(corps);
		return corpVictoryPoint;
	}

	@Override
	public List<String> getCorporationsWithPlayer() {
		List<Corporation> corporations = cr.findAll();
		System.out.println(corporations);
		List<String> corpPlayers = new ArrayList<>();
		for (int i = 0; i < corporations.size(); i++) {
			if (corporations.get(i).getPlayer() != null) {
				corpPlayers.add(corporations.get(i).toString());
			}
		}
		return corpPlayers;
	}
}
