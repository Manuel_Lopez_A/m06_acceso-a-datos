package com.example.demoDAMvi.Services;

import com.example.demoDAMvi.Models.Dices;
import com.example.demoDAMvi.Models.Game;
import com.example.demoDAMvi.Models.Resultat;
import com.example.demoDAMvi.Repositories.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("DicesService")
public class DicesService implements IDicesService{

    @Autowired
    GameRepository gr;

    @Override
    public Resultat resolveDices(Dices dices) {

        Game game = gr.findAll().get(0);

        int temperatura = game.getTemperature();
        int oxigen = game.getOxygen();
        int ocea = game.getOceans();

        for (Integer tirada : dices.getTirades()) {
            switch (tirada) {
                case 1:
                    temperatura++;
                    break;
                case 2:
                    oxigen++;
                    break;
                case 3:
                    ocea++;
                    break;
                default:
                    break;
            }
        }

        gr.saveAndFlush(game);

        String res = "";

        if (temperatura >= 3) res += "S'ha incrementat la temperatura 2 graus, ";
        if (oxigen >= 3) res += "S'ha incrementat l'oxigen, ";
        //if (victoria >= 3) res += "S'han incrementat els punts de victoria, ";
        if (ocea >= 3) res += "S'ha intentat assignar un ocea a la corporacio, ";
        //if (ciutat >= 3) res += "S'ha intentat assignar una ciutat a la corporacio, ";
        //if (jungla >= 3) res += "S'ha intentat assignar una jungla a la corporacio, ";

        return new Resultat(res);
    }
}
