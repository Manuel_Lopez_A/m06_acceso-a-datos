package com.example.demoDAMvi.Models;

import java.util.List;

public class Dices {
    public List<Integer> tirades;
    public int corporation_id;

    public Dices() {

    }

    public Dices(List<Integer> tirades, int corporation_id) {
        this.tirades = tirades;
        this.corporation_id = corporation_id;
    }

    public List<Integer> getTirades() {
        return tirades;
    }

    public void setTirades(List<Integer> tirades) {
        this.tirades = tirades;
    }

    public int getCorporation_id() {
        return corporation_id;
    }

    public void setCorporation_id(int corporation_id) {
        this.corporation_id = corporation_id;
    }

}
