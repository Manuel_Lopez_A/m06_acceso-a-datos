package com.example.demoDAMvi.Models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "makers")
public class Maker {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_maker")
	private int idMaker;
	@Column(name = "name_maker")
	private String name;
	@Column(name = "max_neighbours", nullable = false, columnDefinition = "int")
	private int maxNeighbours = 3;
	@Enumerated(EnumType.STRING)
	@Column(name = "typemaker")
	private Typemaker typemaker;

	// N - 1 : lado [N] lleva el peso de la relación
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_corporation")
	private Corporation corporation;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "casillas_vecinas", joinColumns = @JoinColumn(name = "id_maker1"), inverseJoinColumns = @JoinColumn(name = "id_maker2"))
	private Set<Maker> vecinas = new HashSet<Maker>();

	public Maker() {
		super();
	}

	public Maker(String name) {
		super();
		this.name = name;
	}

	public Maker(String name, int maxneighbours, Typemaker typemaker) {
		super();
		this.name = name;
		this.maxNeighbours = maxneighbours;
		this.typemaker = typemaker;
	}

	public int getIdMaker() {
		return idMaker;
	}

	public String getName() {
		return name;
	}

	public int getMaxNeighbours() {
		return maxNeighbours;
	}

	public Typemaker getTypemaker() {
		return typemaker;
	}

	public Corporation getCorporation() {
		return corporation;
	}

	public void setIdMaker(int idMaker) {
		this.idMaker = idMaker;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMaxNeighbours(int maxNeighbours) {
		this.maxNeighbours = maxNeighbours;
	}

	public void setTypemaker(Typemaker typemaker) {
		this.typemaker = typemaker;
	}

	public void setCorporation(Corporation corporation) {
		this.corporation = corporation;
	}

	public Set<Maker> getVecinas() {
		return vecinas;
	}

	public void setVecinas(Set<Maker> vecinas) {
		this.vecinas = vecinas;
	}

	public List<Integer> ids() {
		List<Integer> i = new ArrayList<>();
		for (Maker vei : vecinas) {
			i.add(vei.idMaker);
		}
		return i;
	}

	public String idCorp() {
		if (this.corporation != null)
			return corporation.getIdCorporation() + "";
		return "no te corp";
	}

	@Override
	public String toString() {
		return "Maker{" + "idMaker=" + idMaker + ", name='" + name + '\'' + ", maxNeighbours=" + maxNeighbours
				+ ", typemaker=" + typemaker + ", corporation=" + idCorp() + ", vecinas=" + ids() + '}';
	}
}
