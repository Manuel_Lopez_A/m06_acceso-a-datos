package com.example.demoDAMvi.Services;

import com.example.demoDAMvi.Models.Dices;
import com.example.demoDAMvi.Models.Resultat;

public interface IDicesService {
    Resultat resolveDices(Dices dices);

}
