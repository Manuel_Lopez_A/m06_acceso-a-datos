package com.example.demoDAMvi.Models;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table (name= "games")
public class Game {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_game")
	private int idGame;
	@Column(name = "oxygen", nullable = false, columnDefinition = "int")
	private int oxygen;
	@Column(name = "temperature", nullable = false, columnDefinition = "int")
	private int temperature;
	@Column(name = "oceans")
	private int oceans;
	@Column(name = "date_start")
	private LocalDateTime dateStart;
	@Column(name = "date_end")
	private LocalDateTime dateEnd;
	
	
	/* M - N :   PARTIDA Y SUS PLAYERS  */
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "partidaJugada")
	private Set<Player> playersInGame = new HashSet<Player>();
	
	
	/* M - 1 :   PARTIDA Y SU WINNER  */
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="id_player")
	private Player winnerPlayer;
	
	
	//El constructor vacío ya va con valores por defecto que pide la práctica.
	protected Game() {
		super();
		this.dateStart = LocalDateTime.now();
		this.oxygen = 0;
		this.temperature = -30;
		this.oceans = 0;
	}	

	public int getIdGame() {
		return idGame;
	}

	public int getOxygen() {
		return oxygen;
	}

	public int getTemperature() {
		return temperature;
	}

	public int getOceans() {
		return oceans;
	}

	public LocalDateTime getDateStart() {
		return dateStart;
	}

	public LocalDateTime getDateEnd() {
		return dateEnd;
	}


	public void setIdGame(int idGame) {
		this.idGame = idGame;
	}

	public void setOxygen(int oxygen) {
		this.oxygen = oxygen;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public void setOceans(int oceans) {
		this.oceans = oceans;
	}

	public void setDateStart(LocalDateTime dateStart) {
		this.dateStart = dateStart;
	}

	public void setDateEnd(LocalDateTime dateEnd) {
		this.dateEnd = dateEnd;
	}

	public Player getWinnerPlayer() {
		return winnerPlayer;
	}

	public void setWinnerPlayer(Player winnerPlayer) {
		this.winnerPlayer = winnerPlayer;
	}

		
	public Set<Player> getPlayersInGame() {
		return playersInGame;
	}

	public void setPlayersInGame(Set<Player> playersInGame) {
		this.playersInGame = playersInGame;
	}

	@Override
	public String toString() {
		return "Game [idGame=" + idGame + ", oxygen=" + oxygen + ", temperature=" + temperature + ", oceans=" + oceans
				+ ", dateStart=" + dateStart + ", dateEnd=" + dateEnd + ", winnerPlayer=" + winnerPlayer + "]";
	}

}
