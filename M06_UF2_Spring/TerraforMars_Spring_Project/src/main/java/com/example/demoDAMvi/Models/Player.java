package com.example.demoDAMvi.Models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "players")
public class Player {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_player")
	private int idPlayer;
	@Column(name = "name", nullable = false, length = 50)
	private String name;
	@Column(name = "wins", nullable = false, columnDefinition = "int")
	private int wins = 0;
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE})
	@JoinColumn(name="id_corporation", unique = true)
	private Corporation corporation;
	
	// hay partidas que me tienen,... llevan el peso de la relación		
	@OneToMany(mappedBy = "winnerPlayer", fetch = FetchType.EAGER)
	private Set<Game> partidasGanadas = new HashSet<Game>();	
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE}) 
	@JoinTable(
			name="game_players", 
			joinColumns = @JoinColumn(name="id_player"), //-> PK propia de la classe que mapeja, 
			inverseJoinColumns = @JoinColumn(name="id_game")// -> PK de l'altra entitat de la relació
	)
	private Set<Game> partidaJugada = new HashSet<Game>();	
	
	public Player() {
		super();
	}

	public Player(String name) {
		super();
		this.name = name;
		this.wins = 0;
	}

	public Player(String name, int wins) {
		super();
		this.name = name;
		this.wins = wins;
	}

	public int getIdPlayer() {
		return idPlayer;
	}

	public String getName() {
		return name;
	}

	public int getWins() {
		return wins;
	}

	public Corporation getCorporation() {
		return corporation;
	}

	public Set<Game> getPartidasGanadas() {
		return partidasGanadas;
	}

	public void setPartidasGanadas(Set<Game> partidasGanadas) {
		this.partidasGanadas = partidasGanadas;
	}

	public void setIdPlayer(int idPlayer) {
		this.idPlayer = idPlayer;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setWins(int wins) {
		this.wins = wins;
	}

	public void setCorporation(Corporation corporation) {
		this.corporation = corporation;
	}

	public String corporationName() {
		return corporation != null ? corporation.getName() : "aquest jugador no te corporation";
	}

	public Set<Game> getPartidaJugada() {
		return partidaJugada;
	}

	public void setPartidaJugada(Set<Game> partidaJugada) {
		this.partidaJugada = partidaJugada;
	}

	public List<Integer> ids() {
		List<Integer> i = new ArrayList<>();
		for (Game win : partidasGanadas) {
			i.add(win.getIdGame());
		}
		return i;
	}

	@Override
	public String toString() {
		return "Player [idPlayer=" + idPlayer + ", name=" + name + ", wins=" + wins + ", corporation=" + corporation
				+ ", partidasGanadas=" + ids() + "]";
	}

}
