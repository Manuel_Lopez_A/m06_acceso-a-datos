package com.example.demoDAMvi.Repositories;

import com.example.demoDAMvi.Models.Maker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MakerRepository extends JpaRepository<Maker, Integer> {
    List<Maker> findByName(String nom);

    Maker findByIdMaker(int id);
}
