package com.example.demoDAMvi.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demoDAMvi.Models.Corporation;
import org.springframework.stereotype.Repository;

@Repository
public interface CorporationRepository extends JpaRepository<Corporation, Integer> 
{
	List<Corporation> findByName(String nom);
	Corporation findByIdCorporation(int id);
}
