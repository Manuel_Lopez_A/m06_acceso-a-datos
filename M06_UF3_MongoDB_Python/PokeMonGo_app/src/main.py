import os
from mongoengine import *
from dotenv import load_dotenv
from Help import help_menu
from Search import search
from Delete import delete
from Catch import catch
from Update import update

# hace que se carguen las variables de entorno del fichero .env
load_dotenv()

mongodb_uri = os.getenv('MONGODB_URI')
client = connect(host=mongodb_uri)
db = client.project

pokemon = db.pokemon
team = db.team

while True:
    
        command = input("Introduce tu consulta: ").split()
        action = command[0].lower()
        if action == "help":
            help_menu()
        elif action == "search":
            search(db.pokemon, command[1], command[2])
        elif action == "release":
            delete(db.team, command[1])
        elif action == "catch":
            catch(command[1])
        elif action == "candy":
            update(command[1])
        else:
            print("""ERROR: sintaxis incorrecta... Introduce help para ver las opciones disponibles""")
    
