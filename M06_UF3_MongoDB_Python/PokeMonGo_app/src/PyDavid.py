import os
from pprint import pprint

from dotenv import load_dotenv

from Pokemon import *

load_dotenv()

mongodb_uri = os.getenv('MONGODB_URI')
client = connect(host=mongodb_uri)

mokepon = Pokemon(
    name="PokemonTest",
    type=[
        Type.BUG,
        Type.POISON
    ],
    spawn_chance=0.5,
    egg="Si",
    prev_evolution=[
        Evolution(
            num="7565",
            name="TestPREV_Evolution"
        )
    ],
    spawn_time="g"
)

pprint(mokepon)

mokepon.save()
