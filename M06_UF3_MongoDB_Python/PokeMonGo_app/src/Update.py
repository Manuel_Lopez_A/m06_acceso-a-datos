from Pokemon import *

def update(nameP):

    pokemonAlimentar = Team.objects(name = nameP).first()    
    if not pokemonAlimentar:
        print(f"No se encontró ningún resultado con la siguiente búsqueda {nameP}")
        return
    print(pokemonAlimentar.current_candy)
    pokemonAlimentar.current_candy += 1
    print(f"Caramel donat a {pokemonAlimentar.name}. Caramels {pokemonAlimentar.current_candy}")
    print(pokemonAlimentar.candy_count)
    if pokemonAlimentar.current_candy == pokemonAlimentar.candy_count:
        
        newEv = Pokemon.objects(name = nameP).first().next_evolution[0].name 
        pokemonEv = Pokemon.objects(name = newEv).first()
        print(f"{pokemonAlimentar.name} evoluciona a {pokemonEv.name}") 

        pokemonAlimentar.name = pokemonEv.name
        pokemonAlimentar.num = pokemonEv.num
        pokemonAlimentar.CP +=100
        pokemonAlimentar.candy = pokemonEv.candy
        pokemonAlimentar.next_evolution = pokemonEv.next_evolution
        pokemonAlimentar.candy_count = pokemonEv.candy_count
        pokemonAlimentar.current_candy = 0

    pokemonAlimentar.save()
        
    
