from pymongo import *
from pprint import pprint

import os
from dotenv import load_dotenv

load_dotenv()

# La vostra aplicació python establirà una forma de fer consultes a la base de dades. Les consultes s’introduiran per
# input i seguiràn sempre aquest estil. Search Nom/num consulta1 Les consultes seran el mateix nom que te a la base
# de dades i es retornaran per tuples. Si un camp té més d’un valor es farà una llista de Python. Exemples de
# consultes seran Search Bulbasaur height (“Bulbasaur”,”0.71 m”) Search 1 weaknesses (“Bulbasaur”,["Fire", "Ice",
# "Flying", "Psychic"]) Hi haura una consulta especial: “evol”: “evol” no pot estar amb altres consultes. Evol pot
# retornar una llista en comptes d’una tupla. Search Bulbasaur evol [“Bulbasaur”,”Ivysaur”,”Venusaur”] Search
# Venusaur evol [“Bulbasaur”,”Ivysaur”,”Venusaur”]

mongodb_uri = os.getenv('MONGODB_URI')
client = MongoClient(mongodb_uri)
db = client.project
collection = db.pokemon

# pokemons = collection.find()
# pokemon = collection.find_one()

# print("UN UNIC POKEMON:")
# pprint(pokemon)

consulta = input("Introduce tu consulta:")
while True:
    consultaSplit = consulta.split()
    if len(consultaSplit) == 3 and consultaSplit[0].lower() == "search":
        try:
            toSearch = consultaSplit[1]
            attribute = consultaSplit[2].lower()
            # una py-thernaria =>
            search_pokemon = (
                {"id": int(toSearch)}
                if toSearch.isdigit()
                else {"name": toSearch.lower().capitalize()}
            )
            objectPokemon = collection.find_one(search_pokemon)
            if objectPokemon:
                try:
                    if attribute == "evol":
                        arrayEv = []
                        ########## TE LO RESUMO NO MÁS ###########
                        if "prev_evolution" in objectPokemon:
                            for prev in objectPokemon["prev_evolution"]:
                                arrayEv.append(prev["name"])
                        arrayEv.append(objectPokemon["name"])
                        if "next_evolution" in objectPokemon:
                            for next in objectPokemon["next_evolution"]:
                                arrayEv.append(next["name"])
                        ##########################################
                        print(arrayEv)
                    else:
                        tutupla = (objectPokemon["name"], objectPokemon[attribute])
                        print(tutupla)
                except:
                    print(f"ERROR: value {attribute} no existe...")
            else:
                print(
                    f"No se encontró ningún resultado con la siguiente búsqueda {search_pokemon}"
                )
        except Exception as e:
            print("ERROR: " + str(e))
    else:
        print("ERROR: las consultas empiezan por \'search\' + [nombre] + [atributo] ")
    consulta = input("Introduce tu consulta:")
