from datetime import datetime
from random import randint

from Pokemon import *

def catch(nameP):
    try:     
        if nameP.isdigit():
            pokemon = Pokemon.objects(num=nameP).get()
        else:
            pokemon = Pokemon.objects(name=nameP).get()
        # NUNCA ENTRA AQUÍ, SI NO ENCUENTRA EL POKEMON SALTA A LA EXCEPTION --->
        # ERROR: Pokemon matching query does not exist.
        # if not pokemon:
        #     print(f"No se encontró ningún resultado con la siguiente búsqueda {nameP}")
        #     return
        db_moves = Move.objects.all()
        move1 = db_moves[randint(0, len(db_moves) - 1)]
        move2 = db_moves[randint(0, len(db_moves) - 1)]

        hp = randint(200, 1000)
        atk = randint(10, 50)
        defn = randint(10, 50)

        team = Team(
            num=pokemon.num,
            name=pokemon.name,
            type= pokemon.type ,
            catch_date=datetime.now(),
            CP=0,
            HPmax=hp,
            HP=0,
            atk=atk,
            def_field=defn,
            energy=0,
            moves=[move1, move2],
            candy=pokemon.candy,
            candy_count=pokemon.candy_count,
            current_candy=0,
            weaknesses= pokemon.weaknesses
        )
        team.save()        
        print(f"Capturado: {pokemon.name}. Número de Pokemon: {len(Team.objects().all())}")

    except Exception as e:
        print("ERROR: " + str(e))
