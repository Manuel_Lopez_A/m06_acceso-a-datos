
def delete(collection, name):
    try:
        search_pokemon = (
            {"num": name}
            if name.isdigit()
            else {"name": name.lower().capitalize()}
        )
        pokemon = collection.find_one(search_pokemon)        
        if not pokemon:
            print(f"No tienes este Pokemon: {name}")
            return

        collection.delete_one(search_pokemon)
        print(f"{pokemon['name']} liberado. Número de Pokemon: {len(list(collection.find()))}")

    except Exception as e:
        print("ERROR: " + str(e))
