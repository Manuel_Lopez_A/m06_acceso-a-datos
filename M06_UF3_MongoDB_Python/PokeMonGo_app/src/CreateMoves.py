import os
from mongoengine import *
from dotenv import load_dotenv
from Pokemon import ChargedMove, FastMove, Type

load_dotenv()

mongodb_uri = os.getenv('MONGODB_URI')
client = connect(host=mongodb_uri)

move1 = ChargedMove(
    name="Hydro Cannon",
    pwr=90,
    type=Type.WATER,
    energyCost=40,
)

move2 = ChargedMove(
    name="Ice Beam",
    pwr=90,
    type=Type.ICE,
    energyCost=55,
)

move3 = ChargedMove(
    name="Thunderbolt",
    pwr=90,
    type=Type.ELECTRIC,
    energyCost=55,
)

move4 = ChargedMove(
    name="Earthquake",
    pwr=120,
    type=Type.GROUND,
    energyCost=65,
)

move5 = FastMove(
    name="Water Gun",
    pwr=3,
    type=Type.WATER,
    energyGain=6,
)

move6 = FastMove(
    name="Ice Shard",
    pwr=3,
    type=Type.ICE,
    energyGain=4,
)

move7 = FastMove(
    name="Thunder Shock",
    pwr=1,
    type=Type.ELECTRIC,
    energyGain=9,
)

move8 = FastMove(
    name="Mud Shot",
    pwr=3,
    type=Type.GROUND,
    energyGain=14,
)

move1.save()
move2.save()
move3.save()
move4.save()
move5.save()
move6.save()
move7.save()
move8.save()
