**PART A: MITJANÇANT PYMONGO**

**PART 1: CONSULTES**

La vostra aplicació python establirà una forma de fer consultes a la base de dades. Les consultes s’introduiran per input i seguiràn sempre aquest estil.

Search Nom/num consulta1 

Les consultes seran el mateix nom que te a la base de dades i es retornaran per tuples. Si un camp té més d’un valor es farà una llista de Python. Exemples de consultes seran

Search Bulbasaur height

(“Bulbasaur”,”0.71 m”)

Search 1 weaknesses

(“Bulbasaur”,["Fire", "Ice", "Flying", "Psychic"])

Hi haura una consulta especial: “evol”: “evol” no pot estar amb altres consultes. Evol pot retornar una llista en comptes d’una tupla.

Search Bulbasaur evol

[“Bulbasaur”,”Ivysaur”,”Venusaur”]

Search Venusaur evol

[“Bulbasaur”,”Ivysaur”,”Venusaur”] \


**PART 2: DELETES**

La vostra aplicació permetrà allibrerar Pokémon. Per això necessitareu una segona collection anomenada “team”. Els documents que s’inseriran a team tindran alguns camps menys que els de la collection pokemon, i alguns camps extres.

La comanda release permetrà alliberar un Pokémon, sempre que el tinguis. Després de cada catch/release, et tornarà el nombre de Pokémon que tens a l’equip. El nombre de Pokémon ha de sortir d’una query a la collection, no d’un comptadorintern. 

Release 001

“Bulbasaur alliberat. Nombre de Pokémon: 0”

Release Bulbasaur

“No tens aquest Pokémon” \


**PART B: MITJANÇANT MONGOENGINE**

**PART 1: MAPEIG I RESTRICCIONS**

Estableix un mapeig de les tres classes amb MongoEngine. Comprova que puguis insertar i modificar documents i inserta’n alguns d’exemple. 

Tingues en compte de que Moves es una classe de la qual heredaran dues classes, FastMove i ChargedMove, la primera tindrà dos camps extres, movetype, que sempre serà “Fast”, i “energyGain”, que serà un nombre. la segona tindrà movetype, que sera sempre “Charged”, i “energyCost”, que serà un nombre. Pots consultar una llista de moviments amb els seus atributs aquí

[https://thesilphroad.com/pokemon-go-moves](https://thesilphroad.com/pokemon-go-moves)

Hauràs d’establir restriccions. Apart de les ja esmentades de HPmax, HP, atk, def i CP, energy ha d’estar entre 0 i 100.

En moviments, el pwr es troba entre 0 i 180.

energyGain entre 0 i 20

i energyCost entre 33 i 100

el tipus, que es troba en Pokémon tant a type com a weaknesses (com a array), i a moves es troba a type (sempre com a variable), només podrà ser un dels tipus de Pokémon, que són els següents

[”Bug”,”Dark”,”Dragon”,”Electric”,”Fairy”,”Fight”,”Fire”,”Flying”,”Ghost”,”Grass”,”Ground”,”Ice”,”Normal”,”Poison”,”Psychic”,”Rock”,”Steel”,”Water”]

**PART 2: CATCH**

L’aplicació ha de permetre inserir documents. Fes una comanda “Catch nomPokemon”. Aquesta comanda crearà un nou Pokèmon a Team, fent servir els camps de la collection Pokemon del nom proporcionat. Els camps extra es determinen de la següent manera.:

HPmax es un nombre entre 200 i 1000, atk i def entre 10 i 50, i CP es la suma dels tres valors. HP comença igual que HPmax i energy i current_candy comencen a 0. catch_date serà la data actual en el moment d’atrapar el Pokémon.

La comanda també agafarà dues referències a dos moviments de moves i els afegirà al Pokèmon.

**PART 3: UPDATES**

La vostra aplicació permetrà donar caramels a Pokémon. Cada cop que es faci la comanda, s’augmentarà en un el camp “current_candy” i es mostrarà el valor d’aquest camp. Si el valor del camp arriva a “candy_count”, el Pokémon evolucionarà. A l’evolucionar el document canviaria de la següent forma (només es mostren els camps que canvien a la collection, la resta es queden iguals)
```json
{
  "_id":{"oid":"58f56170ee9d4bd5e610d644"},
  "num":"002",
  "name":"Ivysaur",
  “CP”:200,
  "candy_count":100.0,
  “current_candy”:0.0, 
}
```
CP augmentarà en 100. És buscarà a la Coll pokemon el primer valor de la llista de next_evolution, i s'aplica el seu num i name als valors del document. Current_candy tornarà a 0, i candy_count s’actualitzarà amb el valor apropiat del seu Pokémon a la collection pokemon.

Un exemple seria així

Candy Bulbasaur

Caramel donat a Bulbasaur. Caramels 24

Candy Bulbasaur

Bulbasaur evoluciona a Ivysaur!

Assumiu que mai es donara un caramel a un pokèmon sense evolucions (no cal tractar l’error)