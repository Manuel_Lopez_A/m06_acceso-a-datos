/////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////     UPDATE      //////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

// 1. $set:
// La operador $set se utiliza para establecer o actualizar valores de campos en un documento.

db.miColeccion.updateOne(
  { _id: ObjectId('12345') },
  { $set: { campo1: "nuevoValor" } }
);
// 2. $unset:
// El operador $unset se utiliza para eliminar un campo de un documento.
db.miColeccion.updateOne(
  { _id: ObjectId('12345') },
  { $unset: { campo1: "" } }
);
// 3. $inc:
// El operador $inc se utiliza para incrementar el valor de un campo numérico en un documento.
db.miColeccion.updateOne(
  { _id: ObjectId('12345') },
  { $inc: { cantidad: 1 } }
);
// 4. $addToSet:
// El operador $addToSet se utiliza para agregar un elemento a un conjunto (array) solo si no existe ya en el conjunto.
db.miColeccion.updateOne(
  { _id: ObjectId('12345') },
  { $addToSet: { conjunto: "nuevoElemento" } }
);
// 5. $push:
// El operador $push se utiliza para agregar un elemento a un conjunto (array).
db.miColeccion.updateOne(
  { _id: ObjectId('12345') },
  { $push: { conjunto: "nuevoElemento" } }
);
// 6. $pull:
// El operador $pull se utiliza para eliminar un elemento de un conjunto (array)
db.miColeccion.updateOne(
  { _id: ObjectId('12345') },
  { $pull: { conjunto: "elementoAEliminar" } }
);
// 7. $rename:
// El operador $rename se utiliza para cambiar el nombre de un campo en un documento.
db.miColeccion.updateOne(
  { _id: ObjectId('12345') },
  { $rename: { "nombreAntiguo": "nombreNuevo" } }
);