// «INSERT» operations: 


// 05 - Fes una inserció mitjançant la Shell amb del següent document:
// ----------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------
db.moviesDAMVI.insertOne(
{
"title":"The Kid",
"year":2000,
"rated":"PG",
"runtime":104,
"countries":["USA"],
"genres":["Comedy","Family","Fantasy"],
"director":"Jon Turteltaub",
"writers":["Audrey Wells"],
"actors":["Bruce Willis","Spencer Breslin","Emily Mortimer","Lily Tomlin"],
"plot":"An unhappy and disliked image consultant gets a second shot at life when an eight year old version of himself mysteriously appears.",
"poster":"http://ia.media-imdb.com/images/M/MV5BMTIwMzk2NTE5NF5BMl5BanBnXkFtZTcwMTcyOTAwMQ@@._V1_SX300.jpg",
"imdb":{"id":"tt0219854","rating":6.1,"votes":33391},
"tomato":{"meter":49,"image":"rotten","rating":5.2,"reviews":97,"fresh":48,"consensus":"Critics find The Kid to be too sweet and the movie's message to be annoyingly simplistic.","userMeter":49,"userRating":3,"userReviews":60463},
"metacritic":45,
"awards":{"wins":1,"nominations":2,"text":"1 win \u0026 2 nominations."},
"type":"movie"
})



// 06 - Fes una inserció múltiple de diferents documents mitjançant la shell:
// ----------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------
db.moviesDAMVI.insertMany([
{
"title":"MacGyver: Trail to Doomsday",
"year":1994,
"rated":null,
"runtime":93,
"countries":["USA"],
"genres":["Action","Adventure"],
"director":"Charles Correll",
"writers":["Lee David Zlotoff","John Considine"],
"actors":["Richard Dean Anderson","Beatie Edney","Peter Egan","Alun Armstrong"],
"plot":"A close friend of MacGyver is murdered. In searching for a reason for this assassination MacGyver discovers a secret nuclear weapons plant right in the center of Britain.",
"poster":null,
"imdb":{"id":"tt0110420","rating":6.4,"votes":1069},
"awards":{"wins":0,"nominations":0,"text":""},
"type":"movie"
},
{
"title":"Where the Trail Ends",
"year":2012,
"rated":null,
"runtime":81,
"countries":["Argentina","Canada","China","Nepal","USA"],
"genres":["Documentary","Adventure","Drama"],
"director":"Jeremy Grant",
"writers":[],
"actors":["Darren Berrecloth","James Doerfling","Andreu Lacondeguy","Kurt Sorge"],
"plot":"Where the Trail Ends is a film following the worlds top freeride mountain bikers as they search for untraveled terrain around the globe, ultimately shaping the future of big mountain free ...",
"poster":"http://ia.media-imdb.com/images/M/MV5BMTQ5NDY5NzY3N15BMl5BanBnXkFtZTgwMTkzOTUzMjE@._V1_SX300.jpg",
"imdb":{"id":"tt2509922","rating":7.7,"votes":951},
"awards":{"wins":1,"nominations":0,"text":"1 win."},
"type":"movie"
},
{
"title":"All Quiet on the Western Front",
"year":1930,
"rated":"UNRATED",
"runtime":136,
"countries":["USA"],
"genres":["Drama","War"],
"director":"Lewis Milestone",
"writers":["Erich Maria Remarque","Maxwell Anderson","George Abbott","Del Andrews","C. Gardner Sullivan"],
"actors":["Louis Wolheim","Lew Ayres","John Wray","Arnold Lucy"],
"plot":"A young soldier faces profound disillusionment in the soul-destroying horror of World War I.",
"poster":"http://ia.media-imdb.com/images/M/MV5BNTM5OTg2NDY1NF5BMl5BanBnXkFtZTcwNTQ4MTMwNw@@._V1_SX300.jpg",
"imdb":{"id":"tt0020629","rating":8.1,"votes":45511},
"awards":{"wins":5,"nominations":2,"text":"Won 2 Oscars. Another 5 wins \u0026 2 nominations."},
"type":"movie"
}]
)



// 07 - Inserta almenys una pel·licula que a tú t’agradi omplint la majoria dels camps.
// ----------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------

db.moviesDAMVI.insertOne(
{
"title":"Falling Down",
"year":1993,
"rated":"PG",
"runtime":112,
"countries":["USA"],
"genres":["Mystery","Family","Drama"],
"director":"Joel Schumacher",
"writers":["Ebbe Roe Smith"],
"actors":["Michael Douglas", "Robert Duvall", "Barbara Hershey", "Rachel Ticotin"],
"plot":"A middle-aged man dealing with both unemployment and divorce, William Foster (Michael Douglas) is having a bad day. When his car breaks down on a Los Angeles highway, he leaves his vehicle and begins a trek across the city to attend his daughter's birthday party. As he makes his way through the urban landscape, William's frustration and bitterness become more evident, resulting in violent encounters with various people, including a vengeful gang and a dutiful veteran cop (Robert Duvall).",
"poster":"https://resizing.flixster.com/v2qgLS7omQhX1XRskTaOVuzVV_Q=/206x305/v2/https://flxt.tmsimg.com/assets/p14593_p_v8_aa.jpg",
"imdb":{"id":"tt0219854","rating":6.1,"votes":33391},
"tomato":{"meter":49,"image":"rotten","rating":5.2,"reviews":97,"fresh":48,"consensus":"Critics find The Kid to be too sweet and the movie's message to be annoyingly simplistic.","userMeter":49,"userRating":3,"userReviews":60463},
"metacritic":45,
"awards":{"wins":1,"nominations":2,"text":"1 win \u0026 2 nominations."},
"type":"movie"
})


// «UPDATE» operations: 


// 8 - Compta quantes pel·lícules de l'any 1999 hi ha a la BD.
db.moviesDAMVI.find({year:1999}).count();
// resultat --> 4


// 9 - Modifica l'any de només una de les pel·lícules del 1999 canviant-lo pel 2000.
db.moviesDAMVI.updateOne(
	{year:1999},
	{$set:{year:2000}},
	{upsert: true} // si no hay ningun archivo lo inserta, si estuviera false no lo insertaría en el caso de no existir
)


// 10 - Comprova que ara hi ha una menys de pel·lícules de l'any 1999.
db.moviesDAMVI.find({year:1999}).count();
// resultat --> 3



// 11 - Primer mira quantes pel·licules hi ha al 2015. Després, afegeix l'actor “Jack Nicholson” al repartiment de les pel·lícules de l'any 2015. Adona't que el repartiment és un array. Després mira quantes pelicul·les s’han vist afectades.

db.moviesDAMVI.updateOne(
	{year:2015},
	{$addToSet:{actors:"Jack Nicholson"}},
	{upsert: true}
)



// 12 - Primer mira quantes pel·licules hi ha amb el camp “tomato” a null. Després, slimina el camp “tomato” dels documents en què el valor d'aquest camp sigui “null”. Ara comprova com han quedat aquestes pel·licules.
 db.moviesDAMVI.find({$and:[{tomato:null}, {tomato:{$exists:true}}]}).count();
// resultat --> 2

db.moviesDAMVI.updateMany(
	{$and:[{tomato:null}, {tomato:{$exists:true}}]},
	{$unset: {"tomato":""} },
	{upsert: true}
)
db.moviesDAMVI.updateMany(
	{tomato:null, tomato:{$exists:true}},
	{$unset: {"tomato":""} },
	{upsert: true}
)