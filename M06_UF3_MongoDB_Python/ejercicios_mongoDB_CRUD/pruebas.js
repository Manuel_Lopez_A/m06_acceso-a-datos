function obtenerHoraActual() {
    const fechaActual = new Date();
    const hora = fechaActual.getHours();
    const minutos = fechaActual.getMinutes();
    const segundos = fechaActual.getSeconds();
    
    return `La hora actual es ${hora}:${minutos}:${segundos}`;
}
console.log(obtenerHoraActual());
