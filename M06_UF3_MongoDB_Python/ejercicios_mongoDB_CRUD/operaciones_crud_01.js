// 01 - Consulta que mostri l'id, title, year i genre de totes les pel·lícules
//      que sí tenen valor al camp genre ordenades descendentment per year.
db.movies.find(
  { genre: { $exists: true } },
  { title: 1, year: 1, genre: 1 },
  { year: 1 }
);

// 02 - Mostrar totes les dades dels curts (“Short”) que s'hagin fet l'any 2000
//      ordenats ascendement per la durada (runtime).
db.movies
  .find({
    runtime: { $exists: true },
    year: 2000,
    genre: "Short",
  })
  .sort({ runtime: 1 });

// 03 - Mostra el títol, any i director de tots els drames, pel·lícules
//      de terror i musicals  (genre = “Drama” o “Horror” o “Music”) que tinguin
//      una durada superior a 2 hores i estiguin només en francès
//      ordenades de més nova a més antiga . Es vol que NO es mostri l'id.
db.movies
  .find(
    {
      $and: [
        { runtime: { $gt: 120 } },
        {
          $or: [
            { genre: { $regex: "Drama" } },
            { genre: { $regex: "Horror" } },
            { genre: { $regex: "Music" } },
          ],
        },
      ],
    },
    { _id: 0, title: 1, year: 1, director: 1, runtime: 1, genre: 1 }
  )
  .sort({ year: -1 });
// lo mismo pero sin $and
db.movies
  .find(
    {
      runtime: { $gt: 120 },
      $or: [
        { genre: { $regex: "Drama" } },
        { genre: { $regex: "Horror" } },
        { genre: { $regex: "Music" } },
      ],
    },
    { _id: 0, title: 1, year: 1, director: 1, runtime: 1, genre: 1 }
  )
  .sort({ year: -1 });


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

//   01 - Mostra totes les dades de les pel·lícules que no tenen el camp director.
//
db.movies.find({ director: { $exists: false } });

// 02 - Mostra només el títol, any i director de les pel·lícules fetes en
//      els anys setanta i noranta, ordenades en ordre descendent per l'any.
db.movies
  .find(
    {
      $or: [
        { $and: [{ year: { $gt: 1969 } }, { year: { $lt: 1980 } }] },
        { $and: [{ year: { $gt: 1989 } }, { year: { $lt: 2000 } }] },
      ],
    },
    { title: 1, _id: 0, year: 1, director: 1 }
  )
  .sort({ year: -1 });

// lo mismo más simple -->
  db.movies
  .find(
    {
      $or: [
        {  year: { $gt: 1969 , $lt: 1980 } },
        {  year: { $gt: 1989 , $lt: 2000 } },
      ],
    },
    { title: 1, _id: 0, year: 1, director: 1 }
  )
  .sort({ year: -1 });


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

// 01 - Mostra l'id, title, any, director i protagonistes (“cast”) de les pel·lícules que tinguin
//      exactament 1 o 2 actors enregistrats al camp cast.
// ---> db.movies.find({"cast": {$exists:true}});
db.movies.find(
    {$or: [
        { cast: { $size: 1 } },
        { cast: { $size: 2 } }
    ]},
    { title:1, year:1, director:1 , cast: 1}
);

// 02 - Selecciona totes les dades de les pel·lícules que estiguin només en espanyol i 
//      el camp cast tingui exactament dos elements.
db.movies.find(
    {
        language: "Spanish",
        cast: { $size: 2 }
    }
)


////////////////////////////////////////////////////////////////////////////
///////////////////////   100YWeatherSmall  ////////////////////////////////
/////////////////////////      “data”    ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////

// 2.-	Mostra els camps temperatura de l'aire i canvi de la pressió atmosfèrica de tots els 
//      documents 	que tinguin el valor de la temperatura de l'aire superior a 45 i inferior a 50.
db.data.find(
  {"airTemperature.value": {"$gt": 44, "$lt": 51}},
  { airTemperature: 1, pressure: 1, _id: 0 }
)

// 3.-	Mostra les mateixes dades que l'exercici anterior però només dels documents que tinguin 
//      tots dos camps: la temperatura de l'aire i el canvi de la pressió atmosfèrica.
db.data.find(
  {"airTemperature.value": {"$gt": 44, "$lt": 51}, pressure: {$exists: true}},
  { airTemperature: 1, pressure: 1, _id: 0 }
)
////////////////////////////////////////////////////////////////////////////
/////////////////////////     MOVIES      //////////////////////////////////
/////////////////////////     videos     ///////////////////////////////////
////////////////////////////////////////////////////////////////////////////

// 01 - Mostra l'id, títol, director, any i idioma de les 10 primeres pel·lícules en què alguns dels seus idiomes 
//      sigui “Spanish” ordenades per l'idioma.
db.movies.find(
    { language: "Spanish" },
    { title: 1, director: 1, year: 1,  language:1 }
).sort({language:1}).limit(10)

// ---> el "$in" proporciona el "$or" que necesita para buscar un idioma u otro del array
// db.movies.find(
//     { language: {$in: ["Spanish", "English"]} },
//     { title: 1, director: 1, year: 1,  language:1 }
// ).sort({language:1}).limit(100)

// 02 - De les consultes dels anteriors apartats, agafa 3 qualsevol i modifica el que consideris per tal que 
//      es mostri per la Shell.


