<?php

namespace App\View\Components;

use App\Models\Car;
use Illuminate\View\Component;

class Post extends Component
{
    public $post;
    public $button_text;
    public $button_url;

    public function __construct($post, $fav)
    {
        $this->post = $post;
        if ($fav) {
            $this->button_text = "favourite";
            $this->button_url = route("savePost");
        } else {
            $this->button_text = "Unfavourite";
            $this->button_url = route("unsavePost");
        }

        // $this->post->car = Car::find($post->car_id);
    }

    public function render()
    {
        return view('components.post')->with("button_text", $this->button_text)->with("post", $this->post)->with("button_url", $this->button_url);
    }
}
