<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\Post;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    // no funcionan los atributos no concuerdan con las columnas de la tabla
    public function sqlActions($nombre){
        //find by id
        $algo1 = Post::find(1);
        //find by id with condition
        $algo2 = Post::where('title', $nombre)->get();
        $algo3 = Post::where('title', $nombre)->first();
        //find with conditions and order
        $algo4 = Post::where('preu', '>=', 5 )->orderBy('title', 'desc')->first();
        //find all
        $algo3 = Post::all();
        //update name
        $algo1->title = 'new title';
        $algo1->save();
        //delete
        $algo1->delete();
    }
}
