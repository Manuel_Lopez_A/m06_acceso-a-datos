<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    use AuthorizesRequests, ValidatesRequests;

    public function showPost($post_id)
    {
        $post = Post::find($post_id);
        // $post->comments = $post->comments->reverse();
        return view('postDetails')->with("post", $post);
    }

    public function addComment(Request $req)
    {
       $text = $req->text;
       $postId = $req->id;
       $comment = new Comment();
       $comment->text = $text;
       $comment->post_id = $postId;
       $comment->user_id = Auth::id();
       if($text!==null) $comment->save();
       return $this->showPost($postId);
    }
}
