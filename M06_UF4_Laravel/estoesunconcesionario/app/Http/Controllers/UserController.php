<?php

namespace App\Http\Controllers;


use App\Models\Car;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    //use Aut=orizesRequests, ValidatesRequests;
    //Mostra la pàgina principal amb totes les ofertes de cotxes.
    public function index()
    {

    }

    //Mostra les ofertes publicades per l'usuari.
    public function showAdds()
    {
        $posts = Post::all();
        $myPosts = array();
        foreach ($posts as $post) {
           if ($post->user_id === Auth::id()) {
               array_push($myPosts, $post);
           }
        }
        return view('myPosts')->with("posts", array_reverse($myPosts));
    }

    //Mostra les ofertes guardades per l'usuari.
    public function showSaved()
    {
        $user = User::find(Auth::id());
        $posts = $user->favourite_posts()->get();
//        foreach ($posts as $post) {
//            $post->car = Car::find($post->car_id);
//        }
        return view('saved')->with("posts", $posts);
    }

    // Mostra el formulari per crear una nova oferta.
    public function showNewAddForm()
    {

    }

    public function savePost(Request $req)
    {
        $user = User::find(Auth::id());
        $post = Post::find($req->post_id);

        $favourite_posts = $user->favourite_posts()->where('post_id', $post->id)->exists();

        if (!$favourite_posts)
            $user->favourite_posts()->attach($post);

        return redirect::to('profile/saved');
    }

    public function unsavePost(Request $req)
    {
        $user = User::find(Auth::id());
        $post = Post::find($req->post_id);

        $favourite_posts = $user->favourite_posts()->where('post_id', $post->id)->exists();

        if ($favourite_posts)
            $user->favourite_posts()->detach($post);

        return redirect::to('profile/saved');
    }

    // Emmagatzema la nova oferta creada per l'usuari.
    public function storeNewAdd(Request $req)
    {
        $post = new Post();
        $post->title = $req->title;
        $post->description = $req->desc;
        $post->user_id = Auth::id();
        $post->save();

        $car = new Car();
        $car->post_id = $post->id;
        $car->brand = $req->brand;
        $car->model = $req->model;
        $car->year = $req->year;
        $car->price = $req->price;
        $car->km = $req->km;
        $car->image = $req->image;
        $car->save();

        print($req->brand);
        return Redirect::to('/');
    }
}
