<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Post extends Model
{
    use HasFactory;

    protected $table = 'posts';

    protected $fillable = [
        'title',
        'description'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class);
    }

    /*   public function favourited_by(): BelongsToMany
       {
           return $this->belongsToMany(User::class);
       }*/

    public function favourited_by()
    {
        return $this->belongsToMany(User::class,'user_favouriteposts', 'post_id', 'user_id');
    }

    public function car()
    {
       return $this->hasOne(Car::class);
    }

}
