<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Car extends Model
{
    use HasFactory;

    public $timestamps = false;
    public $table = 'cars';
    protected $fillable = [
        'brand',
        'model',
        'year',
        'price',
        'km',
        'image',
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

}
