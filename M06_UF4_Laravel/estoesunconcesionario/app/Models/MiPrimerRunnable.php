<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MiPrimerRunnable extends Model
{
    use HasFactory;

    //fillable, datos propios
    protected $fillable = ['mipaquete', 'estoesunmotor', 'mithread'];

    //para generar una tabla en la BBDD
    //hacer migración
    //php artisan make:migration create_table_nombretable

//    COMANDES RECURRENTS:
//
//    composer create-project laravel/laravel [Nom Projecte]
//    cd [Nom Projecte]
//    php artisan serve -> Aixeca el servidor
//    instal·lacio Breeze (login):
//    composer require laravel/breeze
//    php artisan breeze:install
//    npm install
//    npm run dev
//    php artisan make:model [Nom del Model] -sm -> genera un nou model amb els seeders i les migrations
//    php artisan make:migration [Nom Migracio] -> genera una nova migracio
//    php artisan make:view [Nom Vista] -> genera una nova vista/plantilla de blade
//    php artisan make:seeder [Nom Seeder] -> genera un nou seeder
//    php artisan migrate -> Fa migracio de totes les migrations que encara no hagi fet
//    php artisan migrate:fresh -> Fa migracio de totes les migracions perquè fa un drop de la BBDD
//    php artisan db:seed -> Executa el DatabaseSeeder


//    RUTES RECURRENTS:
//    Controladors -> app/Http/Controllers
//    Models -> app/Models
//    Rutes -> routes/web.php
//    Vistes -> resources/views
//    Migrations -> database/migrations
//    Seeders -> database/seeders
//    Factories -> database/factories
//    .env (arxiu configuració de la connexió amb la BBDD) -> a la mateixa arrel


}
