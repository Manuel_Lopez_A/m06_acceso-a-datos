<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory()->create([
            'name' => 'Manuel',
            'email' => 'manuel2@example.com',
            'password' => bcrypt('123456')
        ]);
        User::factory()->create([
            'name' => 'Juan',
            'email' => 'juan@example.com',
            'password' => bcrypt('123456')
        ]);
        User::factory()->create([
            'name' => 'Pedro',
            'email' => 'pedrin@ejemplo.com',
            'password' => bcrypt('123456')
        ]);
    }
}
