<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Comment;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        Comment::create([
            'text' => 'Este es 1 comentario de prueba',
            'user_id' => 1,
            'post_id' => 1,
        ]);
        Comment::create([
            'text' => 'Este es 2 comentario de prueba',
            'user_id' => 1,
            'post_id' => 3,
        ]);
        Comment::create([
            'text' => 'Este es 3 comentario de prueba',
            'user_id' => 1,
            'post_id' => 2,
        ]);
    }
}
