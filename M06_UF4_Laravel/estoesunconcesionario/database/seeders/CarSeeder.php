<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Car;

class CarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Car::create([
            'brand' => 'Opel',
            'model' => 'Astra',
            'year' => 2021,
            'price' => 100000,
            'km' => 0,
            'image' => 'https://k3j6j2a5.rocketcdn.me/wp-content/uploads/2018/08/knight-rider-car-KITT-759x500.jpg',
            'post_id' => 1
        ]);
        Car::create([
            'brand' => 'Ford',
            'model' => 'Focus',
            'year' => 2020,
            'price' => 90000,
            'km' => 10000,
            'image' => 'https://cdn.topgear.es/sites/navi.axelspringer.es/public/media/image/2016/09/cazado-delorean-dmc-12-intentando-viajar-tiempo.jpg?tf=1200x',
            'post_id' => 2
        ]);
        Car::create([
            'brand' => 'Renault',
            'model' => 'Clio',
            'year' => 2019,
            'price' => 80000,
            'km' => 20000,
            'image' => 'https://www.rodi.es/blog/wp-content/uploads/2016/04/4-super-ferrari-de-autos-locos-rodimotorservices.jpg',
            'post_id' => 3
        ]);
        Car::create([
            'brand' => 'Seat',
            'model' => 'Ibiza',
            'year' => 2018,
            'price' => 70000,
            'km' => 30000,
            'image' => 'https://www.rodi.es/blog/wp-content/uploads/2016/04/coche-mystery-machine-scooby-doo.jpg',
            'post_id' => 4
        ]);
   
    }   
}

