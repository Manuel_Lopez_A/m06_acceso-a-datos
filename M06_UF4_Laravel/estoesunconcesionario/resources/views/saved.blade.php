<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            Favourites
        </h2>
    </x-slot>
    <div class="flex flex-col items-center">
        @foreach($posts as $post)
            <x-post :$post :fav="false" />
        @endforeach
    </div>
</x-app-layout>
