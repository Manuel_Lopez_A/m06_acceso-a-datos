<x-app-layout>
    <x-slot name="header bg-white">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{$post->title}}
        </h2>
    </x-slot>
    <div>
    </div>
    <div class="flex items-center">
    <div class="flex flex-col mx-auto items-center bg-white container">
        <img src="{{$post->car->image}}" class="h-100 object-cover" onerror="this.src=''"/>
        <h1 class="text-2xl">{{$post->title}}</h1>
        <h2 class="text-accent">{{$post->description}}</h2>
        <h2 class="text-accent">{{$post->car->brand}}</h2>
        <div>Comments</div>
        @foreach($post->comments as $comment)
            <div class="chat chat-start">
                <div class="chat-header flex items-center justify-between gap-2">
                    <span>{{$comment->user->name}}</span>
                    <time class="text-xs opacity-50">{{$comment->created_at}}</time>
                </div>
                <div class="chat-bubble">{{$comment->text}}</div>
            </div>
        @endforeach
        <form class="flex flex-col gap-4" method="post" action="{{route('newComment')}}">
            @csrf
            <label class="form-control">
                <div class="label">
                    <span class="label-text">Write a comment</span>
                </div>
                <textarea class="textarea textarea-bordered h-24" name="text" placeholder="Your comment..."></textarea>
            </label>
            <button class="btn btn-success" type="submit" name="id" value="{{$post->id}}">Write</button>
        </form>
    </div>
    </div>
</x-app-layout>
