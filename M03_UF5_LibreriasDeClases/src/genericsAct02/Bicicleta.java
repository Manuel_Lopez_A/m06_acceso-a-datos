package genericsAct02;

public class Bicicleta implements Vehicle{

	private String nomPropietari;
	private Double llarg;
	private String marca;
	private String model;
	
	public Bicicleta(String nomPropietari, String model, String marca, Double llarg) {
		super();
		this.nomPropietari = nomPropietari;
		this.llarg = llarg;
		this.marca = marca;
		this.model = model;
	}

	public String getNomPropietari() {
		return nomPropietari;
	}

	public void setNomPropietari(String nomPropietari) {
		this.nomPropietari = nomPropietari;
	}

	public Double getLlarg() {
		return llarg;
	}

	public void setLlarg(Double llarg) {
		this.llarg = llarg;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "Bicicleta [nomPropietari=" + nomPropietari + ", llarg=" + llarg + ", marca=" + marca + ", model="
				+ model + "]";
	}
	
	
	
	
}
