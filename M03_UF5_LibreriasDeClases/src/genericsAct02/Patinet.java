package genericsAct02;

public class Patinet implements Vehicle{
	
	private String nomPropietari;
	private Double potencia;
	private String marca;
	private String model;
	
	public Patinet(String nomPropietari, Double potencia, String marca, String model) {
		super();
		this.nomPropietari = nomPropietari;
		this.potencia = potencia;
		this.marca = marca;
		this.model = model;
	}

	public String getNomPropietari() {
		return nomPropietari;
	}

	public void setNomPropietari(String nomPropietari) {
		this.nomPropietari = nomPropietari;
	}

	public Double getPotencia() {
		return potencia;
	}

	public void setPotencia(Double potencia) {
		this.potencia = potencia;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "Patinet [nomPropietari=" + nomPropietari + ", potencia=" + potencia + ", marca=" + marca + ", model="
				+ model + "]";
	}
	
}
