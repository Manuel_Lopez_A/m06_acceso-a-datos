package genericsAct02;

public class Moto implements Vehicle{

	
	private  String matricula;
	private String marca;
	private String model;
	private Double llarg;
	
	public Moto(String matricula, String marca, String model, Double llarg) {
		super();
		this.matricula = matricula;
		this.marca = marca;
		this.model = model;
		this.llarg = llarg;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Double getLlarg() {
		return llarg;
	}

	public void setLlarg(Double llarg) {
		this.llarg = llarg;
	}


	@Override
	public String toString() {
		return "Moto [matricula=" + matricula + ", marca=" + marca + ", model=" + model + ", llarg=" + llarg
				+ "]";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
}
