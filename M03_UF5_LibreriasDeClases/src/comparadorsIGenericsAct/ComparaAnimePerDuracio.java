package comparadorsIGenericsAct;

import java.util.Comparator;

public class ComparaAnimePerDuracio implements Comparator<Anime>{

	@Override
	public int compare(Anime o1, Anime o2) {
		// duración descendiente
		if(o1.duration < o2.duration) return 1;
		else if (o1.duration > o2.duration) return -1;
		else return 0;
				
//		return (int)o2.duration - (int)o1.duration;  // <-- MAL
	}

}
