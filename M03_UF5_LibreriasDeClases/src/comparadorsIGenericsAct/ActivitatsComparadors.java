package comparadorsIGenericsAct;

import java.util.ArrayList;
import java.util.Collections;

public class ActivitatsComparadors {

	public static void main(String[] args) {

		ArrayList<Anime> listaAnime = new ArrayList<>();
		listaAnime.add(new Anime("Dr Slump", 1984, 20.15));
		listaAnime.add(new Anime("AppleSeed", 1988, 92.50));
		listaAnime.add(new Anime("Tenchi Muyo", 1992, 45.00));
		listaAnime.add(new Anime("Ousama Ranking", 2021, 20.20));
		listaAnime.add(new Anime("The Heike Story", 2022, 24.30));
		listaAnime.add(new Anime("Odd Taxi", 2022, 21.50));
		
		System.out.println(listaAnime);
		System.out.println(".............");
		
//		01
//		Crea una classe principal que tingui una llista de animes que es pugui ordenar quan s’inserten els elements. 
//		Un cop afegida una llista d’animes, mostra per pantalla la llista per comprovar que estan ordenats alfabèticament.
		Collections.sort(listaAnime);
		
		System.out.println(".............");
		System.out.println("ORDEN POR Comparable<T>, TÍTULO ALFABÉTICAMENTE:");

		for (Anime anime : listaAnime) {
			System.out.println(anime);
		}
		
//		02
//		Crea una nova classe principal semblant a l’anterior. Però crea una classe de comparació a partir de 
//		la interficie Comparator. En aquest cas, l’ordenació és per any d’estrena de manera ascendent.
		listaAnime.sort(new ComparaAnimePerAny());
		System.out.println(".............");
		System.out.println("ORDEN POR Comparator, AÑO ASCENDIENTE:");

		for (Anime anime : listaAnime) {
			System.out.println(anime);
		}
		
//		03
//		Crea una nova classe principal semblant a l’anterior. Però crea una classe de comparació a partir de la 
//		interficie Comparator. En aquest cas, l’ordenació és per la duració de manera descendent.
		listaAnime.sort(new ComparaAnimePerDuracio());
		System.out.println(".............");
		System.out.println("ORDEN POR Comparator, DURACION DESCENDIENTE:");

		for (Anime anime : listaAnime) {
			System.out.println(anime);
		}
		
	}

}
