package comparadorsIGenericsAct;

public class Anime implements Comparable<Anime>{

	private String title;
	public int premiere;
	public double duration;
	
	public Anime(String title, int premiere, double duration) {
		super();
		this.title = title;
		this.premiere = premiere;
		this.duration = duration;
	}

	@Override
	public String toString() {
		return "Anime [title=" + title + ", premiere=" + premiere + ", duration=" + duration + "]";
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getPremiere() {
		return premiere;
	}

	public void setPremiere(int premiere) {
		this.premiere = premiere;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}

//	Comparamos dos Strings (Ascendente), el método ya nos devuelve un int al usarlo
	@Override
	public int compareTo (Anime o) {
		return this.title.compareTo(o.title);
	}
	
//	Comparamos dos ints (Ascendente)
//	@Override
//	public int compareTo (Anime o) {
//		return this.premiere - o.premiere;
//	}
	
//	Si comparamos con un Double (Ascendente)
//	@Override
//	public int compareTo (Anime o) {
//		if (this.duration > o.duration) {
//			return 1;
//		} else if (this.duration < o.duration) {
//			return -1;
//		} else {
//			return 0;
//		}		
//	}




	
	
}
