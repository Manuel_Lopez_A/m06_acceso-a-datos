package comparadorsIGenericsAct;

import java.util.Comparator;

public class ComparaAnimePerAny implements Comparator<Anime> {

	@Override
	public int compare(Anime o1, Anime o2) {
//		año ascendente
		return o1.premiere - o2.premiere;
	}

}
