package decoratorAct01;

public class Atun extends Ingredient{

	Atun(Pizza pizza) {
		super(pizza);
	}
	@Override
	public double getPrice() {
//		Precio del ingrediente se aplica a la pizzaBase
//		PizzaBase es una variable de la clase padre
		return pizzaBase.getPrice() + 2.50;
	}
	
	@Override
	public String getDescription() {
		return super.getDescription()+" + atún";
	}
}
