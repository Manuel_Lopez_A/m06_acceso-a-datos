 package decoratorAct01;

 public abstract class Ingredient implements Pizza{

	protected Pizza pizzaBase;
	double price;
	
//	Constructor
	Ingredient(Pizza pizza) {
		this.pizzaBase = pizza;
	}
	
	@Override
	public String getDescription() {
		return pizzaBase.getDescription();
	}

	@Override
	public double getPrice() {
		return pizzaBase.getPrice();
	}

}
