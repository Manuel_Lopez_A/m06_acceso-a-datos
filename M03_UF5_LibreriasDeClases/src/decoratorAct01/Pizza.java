package decoratorAct01;

public interface Pizza
{
	public String getDescription();
	public double getPrice();
}
