package decoratorAct01;

public class Champis extends Ingredient{

	Champis(Pizza pizza) {
		super(pizza);
	}
	
	@Override
	public double getPrice() {
//		Precio del ingrediente se aplica a la pizzaBase
//		PizzaBase es una variable de la clase padre
		return pizzaBase.getPrice() + 2.00;
	}

	@Override
	public String getDescription() {
		return super.getDescription()+" + champiñones";
	}
}
