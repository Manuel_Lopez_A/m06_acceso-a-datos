package decoratorAct01;

public class Tomate extends Ingredient{

	Tomate(Pizza pizza) {
		super(pizza);
	}
	
	@Override
	public double getPrice() {
//		Precio del ingrediente se aplica a la pizzaBase
//		PizzaBase es una variable de la clase padre
		return pizzaBase.getPrice() + 0.75;
	}

}
