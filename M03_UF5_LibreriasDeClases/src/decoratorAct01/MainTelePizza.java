package decoratorAct01;

public class MainTelePizza {

	public static void main(String[] args) {
		
		Pizza p1 = new MassaFina();
		System.out.println(p1.getPrice());
		
		Pizza p2 = new MassaTradicional();
		System.out.println(p2.getPrice());
		
		p1 = new Mozzarella(new Champis(new Tomate(p2)));
		p2 = new Mozzarella(new Tomate(new Jamon(new Atun(p2))));
		
		Pizza p69 = new MassaTradicional();
		p69 = new Tomate(p69);
		p69 = new Mozzarella(p69);
		
		System.out.println(p69.getDescription());
		
		System.out.println(p1.getDescription());
		System.out.println(p1.getPrice());
		
		System.out.println(p2.getDescription());
		System.out.println(p2.getPrice());
	}
	
	
}
