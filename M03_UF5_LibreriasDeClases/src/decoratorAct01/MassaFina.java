package decoratorAct01;

public class MassaFina implements Pizza{

	@Override
	public String getDescription() {
		return "PIZZA: Masa fina";
	}

	@Override
	public double getPrice() {
		return 10.00;
	}

}
