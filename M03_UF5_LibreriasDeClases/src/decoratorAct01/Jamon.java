package decoratorAct01;

public class Jamon extends Ingredient{

	Jamon(Pizza pizza) {
		super(pizza);
	}
	
	@Override
	public double getPrice() {
//		Precio del ingrediente se aplica a la pizzaBase
//		PizzaBase es una variable de la clase padre
//		SIRVE IGUAL super.getPrice() + 1.90;
		return pizzaBase.getPrice() + 1.90;
	}
	@Override
	public String getDescription() {
		return super.getDescription()+" + jamón";
	}
}
