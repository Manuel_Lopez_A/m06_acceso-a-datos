package adapter00;

public class AdaptadorLampara implements AdaptadorLuz{

	private Lampara lampara;
	
	public AdaptadorLampara(Lampara _lampara) {
		this.lampara = _lampara;
	}
	
	@Override
	public void on() {
		lampara.encenderLampara();
	}

	@Override
	public void off() {
		lampara.apagarLampara();
	}
	
}
