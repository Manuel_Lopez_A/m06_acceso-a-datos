package adapter00;

public class Lampara {
	
    public void encenderLampara() {
        System.out.println("Lámpara encendida.");
    }

    public void apagarLampara() {
        System.out.println("Lámpara apagada.");
    }

}
