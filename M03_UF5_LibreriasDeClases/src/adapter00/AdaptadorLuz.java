package adapter00;

public interface AdaptadorLuz {

	void on();
	void off();
	
}
