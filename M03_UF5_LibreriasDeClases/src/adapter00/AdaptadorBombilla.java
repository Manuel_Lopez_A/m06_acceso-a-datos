package adapter00;

public class AdaptadorBombilla implements AdaptadorLuz{

	private Bombilla bombilla;
	
	public AdaptadorBombilla(Bombilla _bombilla) {
		this.bombilla = _bombilla;
	}
	
	@Override
	public void on() {
		bombilla.encenderBombilla();
	}

	@Override
	public void off() {
		bombilla.apagarBombilla();
	}

}
