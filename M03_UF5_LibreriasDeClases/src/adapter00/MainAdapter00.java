package adapter00;

public class MainAdapter00 
{
	public static void main(String[] args) {

		Bombilla bombilla01 = new Bombilla();
		Lampara lampara01 = new Lampara();

		AdaptadorLuz interfazLuz01 = new AdaptadorBombilla(bombilla01);
		AdaptadorLuz interfazLuz02 = new AdaptadorLampara(lampara01);
		
		interfazLuz01.on();
		interfazLuz02.on();
	}
}
