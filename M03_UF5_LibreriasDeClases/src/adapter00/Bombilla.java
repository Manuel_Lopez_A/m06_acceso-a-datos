package adapter00;

public class Bombilla {

    public void encenderBombilla() {
        System.out.println("Bombilla encendida.");
    }

    public void apagarBombilla() {
        System.out.println("Bombilla apagada.");
    }
    
}
