package adapter01;

public interface Aniversari 
{
	int getAny();
	Mes getMes();
	int getDia();
	
	boolean isLaterThan(Aniversari altre);
	boolean isSame(Aniversari altre);
	
	public enum Mes {
		Gener, Febrer, Març, Abril, Maig, Juny, Juliol, Agost, Setembre, Octubre, Novembre, Desembre
	}
}
