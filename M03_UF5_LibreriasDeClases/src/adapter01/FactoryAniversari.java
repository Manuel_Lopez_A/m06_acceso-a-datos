package adapter01;

public interface FactoryAniversari {
	Aniversari getAniversary(int year, Aniversari.Mes month, int day);
}
