package singletonAct;

public class SingletonAct01 {
	
	public static void main(String[] args) {
		
/*
		ArrayList<Anime> listaAnime = new ArrayList<>();
		listaAnime.add(new Anime("Dr Slump", 1984, 20.15));
		listaAnime.add(new Anime("AppleSeed", 1988, 92.50));
		listaAnime.add(new Anime("Tenchi Muyo", 1992, 45.00));
		listaAnime.add(new Anime("Ousama Ranking", 2021, 20.20));
		listaAnime.add(new Anime("The Heike Story", 2022, 24.30));
		listaAnime.add(new Anime("Odd Taxi", 2022, 21.50));
		*/
		
		SingletonCounter counter = SingletonCounter.getInstance();
        SingletonCounter counter2 = SingletonCounter.getInstance();
        
        Anime anime1 = counter.getNewAnime("Naruto", 2002, 24.5);
        Anime anime2 = counter2.getNewAnime("One Piece", 1999, 23.8);
        Anime anime3 = counter.getNewAnime("Attack on Titan", 2013, 25.0);

        System.out.println("Nombre total d'instàncies d'anime creades: " + counter.getInstanceCount());

        System.out.println("Dades de l'anime 1:");
        System.out.println("Nom: " + anime1.getTitle());
        System.out.println("Any d'estrena: " + anime1.getPremiere());
        
        System.out.println("Mitjana de duració dels capítols: " + anime1.getDuration());
        
	}
}
