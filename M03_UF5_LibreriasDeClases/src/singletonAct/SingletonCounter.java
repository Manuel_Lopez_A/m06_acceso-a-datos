package singletonAct;


public class SingletonCounter {

//	un atributo es el contador de animes:
	private int counterAnimes;
//	le ponemos un mismo Singleton (privado) y lo iniciamos a null:
	private static SingletonCounter miSingletonInstance = null;

//	le añadimos un constructor, iniciamos el contador del singleton a 0:
	private SingletonCounter() {
		this.counterAnimes = 0;
//		System.out.println("Hay "+counterAnimes+" instancias de animes.");
	}

//	Retorna l'única instància de la classe `SingletonCounter`. 
//	Si l'instància ja ha estat creada, imprimeix "La instància ja ha estat creada."
	public static synchronized SingletonCounter getInstance()
	{
		if (miSingletonInstance == null)
			return miSingletonInstance = new SingletonCounter();
		else System.out.println("La instancia ya ha sido creada");
		
		return miSingletonInstance;
	}
	
//	Retorna el nombre total d'instàncies de la classe `Anime` creades fins al moment. 
	public int getInstanceCount() {
		return counterAnimes;		
	}
	
//	Incrementa el comptador d'instàncies de la classe `Anime`.
	public void incrementInstanceCount() {
		this.counterAnimes++;
	}

//	Crea una nova instància de la classe `Anime` amb les dades proporcionades i incrementa el comptador d'instàncies
	public Anime getNewAnime(String nom, int any, double duracio) {
		incrementInstanceCount();
		return new Anime(nom, any, duracio);
	}

}
