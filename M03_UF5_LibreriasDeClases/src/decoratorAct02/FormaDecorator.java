package decoratorAct02;

public abstract class FormaDecorator implements Forma 
{
	protected Forma formaBasica;
	
	FormaDecorator(Forma forma){
		this.formaBasica = forma;
	}
	
	@Override
	public void dibuixar() {
		formaBasica.dibuixar();		
	}

	@Override
	public void redimensionar() {
		formaBasica.redimensionar();
	}

	@Override
	public String description() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isOcult() {
		// TODO Auto-generated method stub
		return false;
	}
}
