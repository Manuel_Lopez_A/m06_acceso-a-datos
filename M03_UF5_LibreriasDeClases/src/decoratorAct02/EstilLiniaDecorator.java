package decoratorAct02;

public class EstilLiniaDecorator extends FormaDecorator {

	TipusLinia style;

	EstilLiniaDecorator(Forma forma, TipusLinia tl) {
		super(forma);
		this.style = tl;
	}

	@Override
	public void dibuixar() {
		super.dibuixar();
		System.out.println(description());
	}

	@Override
	public void redimensionar() {
		// TODO Auto-generated method stub

	}

	@Override
	public String description() {
		return "Estil de la linia: " + style;
	}

	@Override
	public boolean isOcult() {
		// TODO Auto-generated method stub
		return false;
	}
}
