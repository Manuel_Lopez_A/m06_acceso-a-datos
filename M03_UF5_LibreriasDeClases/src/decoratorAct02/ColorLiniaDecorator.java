package decoratorAct02;

public class ColorLiniaDecorator extends FormaDecorator{

	Color color;
	
	ColorLiniaDecorator(Forma forma, Color c) {
		super(forma);
		this.color = c;
	}

	@Override
	public void dibuixar() 
	{
		super.dibuixar();
		System.out.println(description());		
	}

	@Override
	public void redimensionar() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String description() {
		return "Color de la linia: "+ color;
	}

	@Override
	public boolean isOcult() {
		// TODO Auto-generated method stub
		return false;
	}
}
