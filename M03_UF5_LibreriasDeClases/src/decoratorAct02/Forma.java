package decoratorAct02;

public interface Forma {
	
	void dibuixar();
	void redimensionar();
	String description();
	boolean isOcult();
}
