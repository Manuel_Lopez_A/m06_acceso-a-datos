package decoratorAct02;

public class ColorEmplenamentDecorator extends FormaDecorator{

	Color color;
	
	ColorEmplenamentDecorator(Forma forma, Color co) {
		super(forma);
		this.color = co;
	}
	
	@Override
	public void dibuixar() 
	{
		super.dibuixar();
		System.out.println(description());
	}

	@Override
	public void redimensionar() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String description() {
		return "Color d'emplenament: "+ color;
	}

	@Override
	public boolean isOcult() {
		// TODO Auto-generated method stub
		return false;
	}

}
