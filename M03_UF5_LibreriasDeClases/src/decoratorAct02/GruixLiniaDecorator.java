package decoratorAct02;

public class GruixLiniaDecorator extends FormaDecorator{

	double gruix;
	
	GruixLiniaDecorator(Forma forma, double d) 
	{
		super(forma);
		this.gruix = d;
	}
	
	@Override
	public void dibuixar() 
	{
		super.dibuixar();
		System.out.println(description());
	}

	@Override
	public void redimensionar() 
	{
		// TODO Auto-generated method stub
	}

	@Override
	public String description() 
	{
		return "Gruix de la linia: "+ gruix;
	}

	@Override
	public boolean isOcult() 
	{
		return false;
	}

	
}
