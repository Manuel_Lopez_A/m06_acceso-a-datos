package factoriesAct03;

public class CardDebit extends Card
{

	public CardDebit(Marca name, Banco banc, Client client) {
		super(name, banc, client);
	}
	
	@Override
	public String getTipusTarjeta() {
		return "Soy tarjeta de debito";
	}
	
}
