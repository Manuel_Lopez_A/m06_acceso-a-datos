package factoriesAct03;

public class AccountSavings extends Account
{
	public AccountSavings( Client cliente, Banco banc) {
		super(cliente, banc);
	}
	public void getTypeAccount(){
		System.out.println("Soy una cuenta de ahorro");
	}
}
