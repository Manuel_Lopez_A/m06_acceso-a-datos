package factoriesAct03;

public class Card_FactoryProvider {

	static AbstractFactoryCards<Card> getFactory(Tipo tipo) 
	{
		switch (tipo) {
		case debit:
			return new CardDebit_Factory();
		case credit:
			return new CardCredit_Factory();
		default:
			return null;
		}
	}
}
