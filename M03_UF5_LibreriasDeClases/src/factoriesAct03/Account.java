package factoriesAct03;

import java.util.Random;

public abstract class Account {
	
	private String accountId;
	private String client;
	private Card associateCard;
	private Banco bank;
	
	
	public Account(Client cliente, Banco banc) {
		super();
		this.bank = banc;
		this.accountId = createAccountId();
		this.client = cliente.getName();
		this.associateCard = null;
	}



	@Override
	public String toString() {
		return "Account [accountId=" + accountId + ", client=" + client + ", associateCard=" + associateCard + ", bank="
				+ bank + "]";
	}



	public String createAccountId() 
	{
		Random r = new Random();
		String accountId = "";
		for (int i = 0; i < 8; i++) 
		{
			accountId += r.nextInt(10);
		}
		return accountId;
	}


	public String getAccountId() {
		return accountId;
	}


	public String getClient() {
		return client;
	}


	public Card getAssociateCard() {
		return associateCard;
	}


	public void setAssociateCard(Card associateCard) {
		this.associateCard = associateCard;
	}



	public Banco getBank() {
		return bank;
	}

	
	
}
