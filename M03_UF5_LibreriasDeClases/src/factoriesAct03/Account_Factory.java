package factoriesAct03;

public class Account_Factory {

	Account createAccount(Cuentas tipoCuenta, Client cliente, Banco banc) 
	{
		Account cuenta = null;
		switch (tipoCuenta) 
		{
		case AHORRO:
			cuenta = new AccountDeposit( cliente, banc);
		case JOVEN:
			cuenta = new AccountYoung(cliente, banc);
		case CORRIENTE:
			cuenta = new AccountSavings(cliente, banc);
		}
		return cuenta;
	}

}
