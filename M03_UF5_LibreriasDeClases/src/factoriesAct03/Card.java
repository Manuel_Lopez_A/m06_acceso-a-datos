package factoriesAct03;

import java.util.Random;

public abstract class Card {
	
	Marca name;
	String CardId;
	BancoID bancoName;
	private Client client;
	Account associatedAccount;
	
	public Card(Marca name, Banco banc, Client cliente) {
		this.name = name;
		this.bancoName = banc.getBankName();
		this.CardId = createCardId();
		this.client = cliente;
		// asocia esta tarjeta a la cuenta
		associatedAccount = banc.getClientes().get(cliente);
		// asocia la cuenta a la tarjeta
		banc.getClientes().get(cliente).setAssociateCard(this);
	}

	// si lo hacemos abstracto no cal rellenarlo...ya lo rellenan los hijos.
	public abstract String getTipusTarjeta();
	
	
	public String createCardId() 
	{
		Random r = new Random();
		String CardId = "";
		for (int i = 0; i < 8; i++) 
		{
			CardId += r.nextInt(10);
		}
		return CardId;
	}

	@Override
	public String toString() {
		return "Card [name=" + name + ", CardId=" + CardId + ", bancoName=" + bancoName + ", client=" + client.getName()
				+ ", associatedAccount=" + associatedAccount.getAccountId() + "]";
	}








	
}
