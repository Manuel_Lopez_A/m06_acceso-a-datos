package factoriesAct03;

import factoriesAct02.CreditCard;

public class MainFactoriesActivitat03 
{
	public static void main(String[] args) 
	{
//		FABRICA DE TARJETAS SEGÚN MARCA Y TIPO:
		
//		el saldo es indiferente porque las tarjetas las elegiremos nosotros,
//		ya se implementó el método en la actividad 02 de adjudicar tarjeta según saldo...
		Client manu = new Client("Manuel López", 550.00);
		Client laura = new Client("Laura Muñoz", 1850.00);		
		
		Banco ingBank = new Banco(BancoID.ING);
		Banco caixaBank = new Banco(BancoID.CaixaBanc);
//		System.out.println(ingBank);
		
		/* esto hace el banco --> ingBank.crearCuenta(Cuentas.AHORRO, manu); */
		/* quizás no debería acceder al objeto ingBank??, terrorismo o no terrorismo?? */
		laura.pedirCuenta(Cuentas.CORRIENTE, caixaBank);
		laura.pedirCuenta(Cuentas.JOVEN, ingBank);		
		manu.pedirCuenta(Cuentas.AHORRO, ingBank);
		
		manu.pedirTarjeta(Marca.AmericanExpress, Tipo.credit, ingBank);
		System.out.println(".");
		System.out.println(".");
		
		Card hola = new CardCredit(Marca.AmericanExpress, ingBank, laura);
		
//		----------------------------------------------------------------------------------- //
		
		System.out.println("Cuentas de cliente de MANU: ");
		System.out.println(manu.getClientAccounts());
		// para comprobar la clase :
		System.out.println(manu.getClientAccounts().get(0).getClass()); 
		System.out.println(".");
		
		
		System.out.println("Tarjetas de cliente de MANU: ");
		System.out.println(manu.getClientCards());
		// para comprobar la clase :
		System.out.println(manu.getClientCards().get(0).getTipusTarjeta()); 
		System.out.println(".");
		
//		Cuántos clientes tiene ING??
		System.out.println("Clientes de ING:::");
		ingBank.getClientsList();
		System.out.println(".");
		System.out.println(".");
		
//		Resumiendo:
		System.out.println("Información de MANU: ");
		System.out.println(manu);
		System.out.println(".");
		
//		en principio todo funciona, si hubiera algún fallo puede ser que al meter tanto toString()
//		como en el meto clases en otros objetos, Cliente tiene una cuenta, y en el banco hay un cliente, 
//		hago toString y me imprime cliente que tiene cuenta en banco que tiene cliente, que tiene cuenta, 
//		que tiene cliente... 
	}
}
