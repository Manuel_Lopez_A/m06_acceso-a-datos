package factoriesAct03;

public class CardDebit_Factory implements AbstractFactoryCards<Card>{

	@Override
	public Card create(Marca m, Banco b, Client client) {
		switch(m) {
		case AmericanExpress :
			return new CardDebit(Marca.AmericanExpress, b, client);
		case Visa :
			return new CardDebit(Marca.Visa, b, client);
		case MasterCard :
			return new CardDebit(Marca.MasterCard, b, client);
		default:
			return null;
		}
	}
}
