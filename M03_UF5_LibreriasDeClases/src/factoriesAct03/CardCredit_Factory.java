package factoriesAct03;

public class CardCredit_Factory implements AbstractFactoryCards<Card>{

	@Override
	public Card create(Marca m, Banco bancName, Client client) {
		switch(m) {
		case AmericanExpress :
			return new CardCredit(Marca.AmericanExpress, bancName, client);
		case Visa :
			return new CardCredit(Marca.Visa, bancName, client);
		case MasterCard :
			return new CardCredit(Marca.MasterCard, bancName, client);
		default:
			return null;
		}
	}

}
