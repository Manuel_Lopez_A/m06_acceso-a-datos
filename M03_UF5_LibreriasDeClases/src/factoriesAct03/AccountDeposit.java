package factoriesAct03;

public class AccountDeposit extends Account
{
	public AccountDeposit(Client cliente, Banco banc) {
		super(cliente, banc);
	}
	@Override
	public String toString() {
		return "Account_Deposit []";
	}
	public void getTypeAccount(){
		System.out.println("Soy una cuenta nómina");
	}
}
