package factoriesAct03;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Banco {

	private BancoID bankName;
	private String info;
	private HashMap<Client, Account> clientes;

	public Banco(BancoID bankName) {
		super();
		this.bankName = bankName;
		this.clientes = new HashMap<>();
		// le damos la info de forma automática
		if (bankName == BancoID.ING)
			this.info = "ING Direct";
		else if (bankName == BancoID.Santander)
			this.info = "Banco Santander";
		else if (bankName == BancoID.CaixaBanc)
			this.info = "Caixa-Banc";
	}

	public Account crearCuenta(Cuentas tipoCuenta, Client cliente) {
		Account nuevaCuenta = null;
		if (!clientes.containsKey(cliente)) {
			System.out.println(
					this.getBankName() + " no tiene a " + cliente.getName() + " como cliente, le hacemos cuenta");
			Account_Factory accountFactory = new Account_Factory();
			nuevaCuenta = accountFactory.createAccount(tipoCuenta, cliente, this);
			clientes.put(cliente, nuevaCuenta);
//			System.out.println("clientes del banco:" + clientes);
		} else {
			System.out.println(cliente.getName() + " ya tiene una cuenta en este banco");
		}
		return nuevaCuenta;
	}

	public Card crearTarjeta(Marca marca, Tipo tipoTarjeta, Client client) {
		Card newCard = null;

		if (clientes.containsKey(client)) {
			if (tipoTarjeta == Tipo.debit)
				newCard = Card_FactoryProvider.getFactory(Tipo.debit).create(marca, this, client);
			else if (tipoTarjeta == Tipo.credit)
				newCard = Card_FactoryProvider.getFactory(Tipo.credit).create(marca, this, client);
			System.out.println(client.getName() + " es cliente, se le ha hecho tarjeta.");
		} else {
			System.out.println(client.getName() + " no es cliente, primero tiene que hacerse una cuenta.");
		}
		return newCard;
	}

	@Override
	public String toString() {
		return "Banco [" + info + "]";
	}

	public BancoID getBankName() {
		return bankName;
	}

	public void setBankName(BancoID bankName) {
		this.bankName = bankName;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public HashMap<Client, Account> getClientes() {
		return clientes;
	}

//	public void setClientes(HashMap<Client, Account> clientes) {
//		this.clientes = clientes;
//	}

	public void getClientsList() {

//		   for (Client client : clientes.keySet()) {
//		        System.out.println("Cliente: " + client.getName());
//		    }	
		for (Map.Entry<Client, Account> entrada : clientes.entrySet()) {
			System.out.println(
					"Cliente: " + entrada.getKey().getName() + " -> Nº de Cuenta: " + 
							entrada.getValue().createAccountId());
		}
	}

}
