package factoriesAct03;

public class CardCredit extends Card {

	public CardCredit(Marca name, Banco banc, Client client) {
		super(name, banc, client);
	}

	@Override
	public String getTipusTarjeta() {
		return "Soy tarjeta de crédito";
	}
	
	public void hola() {
		System.out.println("hola");
	}
}
