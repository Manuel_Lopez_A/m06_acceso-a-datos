package factoriesAct03;

import java.util.ArrayList;

public class Client 
{
	private String name;
	private Double salary;
	private ArrayList<Card> clientCards;
	private ArrayList<Account> clientAccounts;

	public Client(String name, Double salary) {
		super();
		this.name = name;
		this.salary = salary;
		this.clientCards = new ArrayList<>();
		this.clientAccounts = new ArrayList<>();
	}
	
	public void pedirCuenta(Cuentas tipoCuenta, Banco banco) {
		this.clientAccounts.add(banco.crearCuenta(tipoCuenta, this));
//		System.out.println(banco.crearCuenta(tipoCuenta, this));
	}
	public void pedirTarjeta(Marca marca, Tipo tipo, Banco banco) {
		this.clientCards.add(banco.crearTarjeta(marca, tipo, this));
//		System.out.println(banco.crearCuenta(tipoCuenta, this));
	}
	
	@Override
	public String toString() {
		return "Client [name=" + name + ", salary=" + salary + ", clientCards=" + clientCards + ", clientAccounts="
				+ clientAccounts + "]";
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getSalary() {
		return salary;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}
	public ArrayList<Card> getClientCards() {
		return clientCards;
	}
	public void setClientCards(ArrayList<Card> clientCards) {
		this.clientCards = clientCards;
	}
	public ArrayList<Account> getClientAccounts() {
		return clientAccounts;
	}
	public void setClientAccounts(ArrayList<Account> clientAccounts) {
		this.clientAccounts = clientAccounts;
	}
}
