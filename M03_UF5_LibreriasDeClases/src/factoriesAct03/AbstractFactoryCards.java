package factoriesAct03;

public interface AbstractFactoryCards<T> {
	
	T create(Marca m, Banco bancName, Client client);
	
}
