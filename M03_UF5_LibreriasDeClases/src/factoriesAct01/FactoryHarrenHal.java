package factoriesAct01;

public class FactoryHarrenHal implements FactoryCar<Car>{
	
	@Override
	public Car create(Model m) 
	{
		if(m == Model.BIFUEL)return new CarBifuel(m, Location.HARRENHAL);
		else if ( m== Model.CAVALLS) return new CarCavalls(m, Location.HARRENHAL);
		else if ( m== Model.DRAC) return new CarDrac(m, Location.HARRENHAL);
		else if ( m== Model.ELECTRIC) return new CarElectric(m, Location.HARRENHAL);
		else if ( m== Model.HIBRID) return new CarHybrid(m, Location.HARRENHAL);
		else if ( m== Model.HIDROGEN) return new CarHidrogen(m, Location.HARRENHAL);
		else return null;
	}

	
}
