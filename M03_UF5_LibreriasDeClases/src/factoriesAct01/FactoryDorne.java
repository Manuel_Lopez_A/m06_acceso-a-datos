package factoriesAct01;

public class FactoryDorne implements FactoryCar<Car>{
	@Override
	public Car create(Model m) 
	{
		if(m == Model.BIFUEL)return new CarBifuel(m, Location.PRINCIPATDORNE);
		else if ( m== Model.CAVALLS) return new CarCavalls(m, Location.PRINCIPATDORNE);
		else if ( m== Model.DRAC) return new CarDrac(m, Location.PRINCIPATDORNE);
		else if ( m== Model.ELECTRIC) return new CarElectric(m, Location.PRINCIPATDORNE);
		else if ( m== Model.HIBRID) return new CarHybrid(m, Location.PRINCIPATDORNE);
		else if ( m== Model.HIDROGEN) return new CarHidrogen(m, Location.PRINCIPATDORNE);
		else return null;
	}

}
