package factoriesAct01;

public class ActivitatsFactory01 
{
	public static void main(String[] args) 
	{

		Car cotxe01 = 
				FactoryDeCarsFactory.getCarFactory(Location.INVERNALIA).create(Model.HIDROGEN);
		Car cotxe02 = 
				FactoryDeCarsFactory.getCarFactory(Location.HARRENHAL).create(Model.CAVALLS);
		Car cotxe03 = 
				FactoryDeCarsFactory.getCarFactory(Location.TEMPESTES).create(Model.HIBRID);
		Car cotxe04 = 
				FactoryDeCarsFactory.getCarFactory(Location.PRINCIPATDORNE).create(Model.BIFUEL);
		System.out.println(cotxe01);
		System.out.println(cotxe02);
		System.out.println(cotxe03);
		System.out.println(cotxe04);
		
	}
}
