package factoriesAct01;

public abstract class Car {
	
	Model model;
	Location localitzacio;
	
	public Car(Model m, Location l) {
		this.model = m;
		this.localitzacio = l;		
	}

	@Override
	public String toString() {
		return "Car [model=" + model + ", localitzacio=" + localitzacio + "]";
	};
	
	
}
