package factoriesAct01;

public class FactoryTempestes implements FactoryCar<Car>{

	@Override
	public Car create(Model m) 
	{
		if(m == Model.BIFUEL)return new CarBifuel(m, Location.TEMPESTES);
		else if ( m== Model.CAVALLS) return new CarCavalls(m, Location.TEMPESTES);
		else if ( m== Model.DRAC) return new CarDrac(m, Location.TEMPESTES);
		else if ( m== Model.ELECTRIC) return new CarElectric(m, Location.TEMPESTES);
		else if ( m== Model.HIBRID) return new CarHybrid(m, Location.TEMPESTES);
		else if ( m== Model.HIDROGEN) return new CarHidrogen(m, Location.TEMPESTES);
		else return null;
	}

}
