package factoriesAct01;

public class FactoryDeCarsFactory 
{
	static FactoryCar<Car> getCarFactory(Location location){
		
		if(location == Location.HARRENHAL) return new FactoryHarrenHal();
		else if(location == Location.INVERNALIA) return new FactoryInvernalia();
		else if(location == Location.PRINCIPATDORNE) return new FactoryDorne();
		else if(location == Location.ROCA_CASTERLY) return new FactoryRocaCasterly();
		else if(location == Location.TEMPESTES) return new FactoryTempestes();
		return null;
	}
	
	
}
