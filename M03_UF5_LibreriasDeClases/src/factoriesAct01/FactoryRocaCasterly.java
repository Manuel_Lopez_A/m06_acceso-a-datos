package factoriesAct01;

public class FactoryRocaCasterly implements FactoryCar<Car> {
	
	
	@Override
	public Car create(Model m) {
		if(m == Model.BIFUEL)return new CarBifuel(m, Location.ROCA_CASTERLY);
		else if ( m== Model.CAVALLS) return new CarCavalls(m, Location.ROCA_CASTERLY);
		else if ( m== Model.DRAC) return new CarDrac(m, Location.ROCA_CASTERLY);
		else if ( m== Model.ELECTRIC) return new CarElectric(m, Location.ROCA_CASTERLY);
		else if ( m== Model.HIBRID) return new CarHybrid(m, Location.ROCA_CASTERLY);
		else if ( m== Model.HIDROGEN) return new CarHidrogen(m, Location.ROCA_CASTERLY);
		else return null;
	}


}
