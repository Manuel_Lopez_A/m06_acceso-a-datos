package factoriesAct01;

public interface FactoryCar<T> 
{
	T create(Model m) ;
}
