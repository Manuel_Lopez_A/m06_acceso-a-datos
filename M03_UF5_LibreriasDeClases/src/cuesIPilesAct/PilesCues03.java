package cuesIPilesAct;

import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class PilesCues03 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int lineas = sc.nextInt();
			sc.nextLine(); // debuff
			
			Deque<String> tasquesOrdenadas = new LinkedList<String>();
			HashMap<String, String> tasquesMap = new HashMap<>();
//			HashMap<String, List<String>> tasquesMap = new HashMap<>();
			
			// dividimos por strings....
			for (int j = 0; j < lineas; j++) {
				
				String[] tasca = sc.nextLine().split("-");
				if(tasca.length>1) {
//					tasquesMap.put(tasca[0], Arrays.asList(tasca[1].split(",")));
					tasquesMap.put(tasca[0], tasca[1]);
				}else {
					tasquesOrdenadas.push(tasca[0]);
				}
			}
//			System.out.println(tasquesOrdenadas);
			//comprueba valores del tasquesMap
//			for (Map.Entry<String, String> cobi : tasquesMap.entrySet()) {
//				System.out.println("key foreach: "+cobi.getKey()+ " value foreach: "+cobi.getValue());
//			}
			
			// ahora comparamos el primer valor de la deque con el hashmap, 
			// y en las listas buscamos si tienen el valor...
			while(!tasquesMap.isEmpty()) {
				
				for (Map.Entry<String, String> cobi : tasquesMap.entrySet()) {
//					System.out.println("HOLA");
					if(cobi.getValue().length()==1 && cobi.getValue().equals(tasquesOrdenadas.peekFirst())) {
//						System.out.println("entro en el primer IF");
						tasquesOrdenadas.push(cobi.getKey());
						tasquesMap.remove(cobi.getKey());
					} else if(cobi.getValue().length()>1) {
						
						List<String> tasques = Arrays.asList(cobi.getValue().split(","));
//						System.out.println("entro en el ELSE IF");
						String nuevoValor = "";
						for( int k = 0; k < tasques.size(); k++) {
//							System.out.println("entro en el ELSE IF  VUELTA "+k);
							if(tasques.get(k).equals(tasquesOrdenadas.peekFirst())) {
								nuevoValor+="";
							}else {
								nuevoValor+= tasques.get(k)+",";
							}
						}
						cobi.setValue(nuevoValor.substring(0, nuevoValor.length()-1));
					}
				}
			}
			
//			System.out.println(tasquesOrdenadas);
			while(!tasquesOrdenadas.isEmpty()) {
				String tascaImprime = tasquesOrdenadas.pollLast();
				System.out.print(tasquesOrdenadas.size()>0 ? tascaImprime+ " " : tascaImprime); 
			}
		}
		sc.close();
	}
}
