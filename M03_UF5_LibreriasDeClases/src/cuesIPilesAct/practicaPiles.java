package cuesIPilesAct;

import java.util.ArrayDeque;
import java.util.Deque;


public class practicaPiles {
	public static void main(String[] args) {

		Deque<String> trotaMusicos = new ArrayDeque<>(); // version vectores
														//	linkedlist nodos
		
//		OFFER lo coloca en orden de lista.... FIFO
//		trotaMusicos.offer("Caballo");
//		trotaMusicos.offer("Perro");
//		trotaMusicos.offer("Gato");
//		trotaMusicos.offer("Gallo");
		
//		PUSH los coloca en forma de pila... LIFO
		trotaMusicos.push("Caballo");
		trotaMusicos.push("Perro");
		trotaMusicos.push("Gato");
		trotaMusicos.push("Gallo");
		System.out.println(trotaMusicos);
		
		System.out.println("getLast(): "+ trotaMusicos.getLast()); // saca el Gallo
		System.out.println("getFirst(): "+ trotaMusicos.getFirst()); // saca el Caballo
		
		System.out.println("pop(): "+trotaMusicos.pop());		
		System.out.println(trotaMusicos);
//		pop y poll hacen exactamente lo mismo
//		System.out.println("poll(): "+trotaMusicos.poll());		
//		System.out.println(trotaMusicos);
		
		trotaMusicos.offerFirst("Gallo"); // lo pone de nuevo al principio, un offer normal lo pone al final
		System.out.println(trotaMusicos);

		
		
		
	}

}
