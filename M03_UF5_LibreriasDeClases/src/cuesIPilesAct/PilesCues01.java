package cuesIPilesAct;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Scanner;

public class PilesCues01{

public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		Deque<String> pila = new ArrayDeque<String>();
		

		int lineas = sc.nextInt();
		sc.nextLine(); // desBuff

		boolean etiquetesCorrectes = true;

		for (int i = 0; i < lineas; i++) {
//			Cuántas lineas hay?
			String linia = sc.nextLine();
//			Empieza y acaba por <>? OK
			if (linia.startsWith("<") && linia.endsWith(">")) {
				
				if(tieneIntrusos(linia)) {
					// divide las palabras y retorname una lista
//					System.out.println(linia+ " tiene intrusos");
					etiquetesCorrectes = dividirLinea(linia);
					if( !etiquetesCorrectes) break;
				} 
				else {
					ponerEnPila(linia, pila, etiquetesCorrectes);
				}
//				Está la lista vacía? Ponlo
			}
			else {
				etiquetesCorrectes = false;
				break;
			}
		}
		
		if (etiquetesCorrectes && pila.isEmpty())
			etiquetesCorrectes = true;
		else
			etiquetesCorrectes = false;
		// Imprimim el resultat
		System.out.println(etiquetesCorrectes ? "Etiquetes ben tancades" : "Etiquetes mal tancades");
		sc.close();
	}
	private static boolean dividirLinea(String linia) {
		
		boolean colaLimpia = true;
		String[]lineaDividida = linia.split(">");
//		System.out.println("linea length "+lineaDividida.length);
		for(int j = 0; j < lineaDividida.length/2; j++) {
			// TODO ACABAR ESTO			
			String[] nuevaDivision = lineaDividida[j+1].split("<");
			String p1 = lineaDividida[0].substring(1);
			if (nuevaDivision[1].charAt(0)=='/') {
				if(p1.equals(nuevaDivision[1].substring(1))) {
					colaLimpia = true;
				} else {
					colaLimpia = false;
				}
			}else {
				colaLimpia = false;
			}
		}
		return colaLimpia;
	}
	private static void ponerEnPila(String linia, Deque pila, boolean etiquetesCorrectes) {
		
		if (pila.isEmpty()) {
//			añade la linia sin "<" ni ">"
			pila.push(linia.substring(1, linia.length() - 1));

		} else { 
//			Si no, mira si empieza por "/",
			if (linia.charAt(1) == '/') {
//				si el último tiene es igual sin el "/" es que cierra, así que eliminalo y no agregues este
				if (pila.peek().equals(linia.substring(2, linia.length() - 1))) {
					pila.pop();
				} else {
//					si no es así las etiquetas están mal, cierra esto
					etiquetesCorrectes = false;
				}
//			si no empieza por "/" añadelo y seguimos
			} else {
				pila.push(linia.substring(1, linia.length() - 1));
			}
		}

		}
/**
 * Comprueba si dentro de la linea hay  "<" o ">" para luego seguir haciendo subdivisiones
 * @param linia
 * @return 
 */
	private static boolean tieneIntrusos(String linia) {
		
		boolean intrusos;		
		if(linia.substring(1, linia.length() - 1).contains(">") || linia.substring(1, linia.length() - 1).contains("<")) {
			intrusos = true;
		} else {
			intrusos = false;
		}
		return intrusos;
	}
}