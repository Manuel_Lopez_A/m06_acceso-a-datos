package cuesIPilesAct;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class PilesCues02 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		sc.nextLine(); // debuff

		for (int i = 0; i < casos; i++) {
			
			String plb = sc.nextLine();
			Queue<Character> miCola = new LinkedList<Character>();
			
			for (int j = 0; j < plb.length(); j++) {
				miCola.add(plb.charAt(j));
			}
			
			Character revisaChar = miCola.poll();
//			miCola.poll(); retorna el primero y borra.
//			miCola.peek(); retorna pero no borra el primero.			
			int contadorLetra = 1;			
			while(!miCola.isEmpty()) {
				if(revisaChar == miCola.peek()) {
					contadorLetra ++;
				}else {
					System.out.print(contadorLetra+""+revisaChar);
					contadorLetra = 1;
				}
				revisaChar = miCola.poll();
			}
			System.out.println(contadorLetra+""+revisaChar);
		}
		sc.close();
	}
}
