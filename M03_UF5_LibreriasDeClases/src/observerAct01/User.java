package observerAct01;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class User implements PropertyChangeListener{
	
	private String userName;

	public String getUserName() {
		return userName;
	}

	public User(String userName) {
		super();
		this.userName = userName;
	}

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
		
		if(evt.getPropertyName().equals(this.userName))
		{
//			recibo el mensaje 
			Message nuevoMensaje = (Message) evt.getNewValue();
			leerMensaje(nuevoMensaje);
			
		}
		
	}
	
	public void leerMensaje(Message mensaje) {
		System.out.println(this.userName + " ha recibido un mensaje de "+ mensaje.getSender() +": "+ mensaje.getContent());
	}
	
	

}
