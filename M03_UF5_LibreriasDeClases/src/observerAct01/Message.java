package observerAct01;

public class Message {
	
	private String sender;
	private String recipient;
	private String content;
	
	public Message(String sender, String recipient, String content) 
	{
		super();
		this.sender = sender;
		this.recipient = recipient;
		this.content = content;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	
}
