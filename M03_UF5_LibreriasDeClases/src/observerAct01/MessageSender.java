package observerAct01;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class MessageSender{

	private PropertyChangeSupport support;
	//private Message message;
	
	public MessageSender() {
		super();
		this.support = new PropertyChangeSupport(this);
	}
	
	public void addPropertyChangeListener(PropertyChangeListener pcl)
	{
		support.addPropertyChangeListener(pcl);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener pcl)
	{
		support.removePropertyChangeListener(pcl);
	}


	public void sendMessage(Message mensaje) {
		support.firePropertyChange(mensaje.getRecipient(), "", mensaje);
	}
	
	
	
}
