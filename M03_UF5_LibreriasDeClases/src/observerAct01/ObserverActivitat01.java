package observerAct01;

public class ObserverActivitat01 {
	
	public static void main(String[] args) 
	{

		User manu = new User("Manuel");
		User laura = new User("Laura");
		User dario = new User("Dario");
		User tina = new User("Tina");
		
		MessageSender ms = new MessageSender();
		ms.addPropertyChangeListener(manu);
		ms.addPropertyChangeListener(laura);
		ms.addPropertyChangeListener(dario);
		ms.addPropertyChangeListener(tina);
		
		
		ms.sendMessage(new Message(manu.getUserName(), laura.getUserName(), "Cómete los canelones"));
		ms.sendMessage(new Message(tina.getUserName(), laura.getUserName(), "GUAU GUAU (Tengo hambre)"));
		ms.sendMessage(new Message(laura.getUserName(), tina.getUserName(), "Ahora se lo digo al Manu"));
		ms.sendMessage(new Message(laura.getUserName(), dario.getUserName(), "Dale de comer a la perra"));
		ms.sendMessage(new Message(dario.getUserName(), manu.getUserName(), "GUGUGU"));
		
		
	}
}
