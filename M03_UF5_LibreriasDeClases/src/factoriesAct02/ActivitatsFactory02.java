package factoriesAct02;

public class ActivitatsFactory02 {

	public static void main(String[] args) {
		
		Client Manuel = new Client("Manuel", 650.25);
		Client Pepa = new Client("Francisca", 350.00);
		Client Cristina = new Client("Cristina", -25.00);
		
		CreateCardClient cc = new CreateCardClient();
		cc.associateCardClient(Pepa);
		cc.associateCardClient(Manuel);
		cc.associateCardClient(Cristina);
		
		System.out.println(Pepa);
		System.out.println(Manuel);
		System.out.println(Cristina);
	}
}
