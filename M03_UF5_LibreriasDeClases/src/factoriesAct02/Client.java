package factoriesAct02;

public class Client {

	private String name;
	private Double salary;
	private Card card;
	
	public Client(String name, Double salary) {
		super();
		this.name = name;
		this.salary = salary;
	}


	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getSalary() {
		return salary;
	}
	public void setSalary(Double salary) {
		this.salary = salary;
	}
	public Card getCard() {
		return card;
	}
	public void setCard(Card card) {
		this.card = card;
	}
	
	@Override
	public String toString() {
		if (this.card == null) return "Client [name=" + name + ", salary=" + salary + ", card=" + card + ", SALARI INSUFICIENT]";
		else return "Client [name=" + name + ", salary=" + salary + ", card=" + card + "]";
	}
	
	
}
