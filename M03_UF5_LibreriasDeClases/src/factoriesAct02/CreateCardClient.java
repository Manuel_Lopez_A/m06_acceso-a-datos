package factoriesAct02;

public class CreateCardClient {

	public void associateCardClient(Client client) 
	{
		if (client.getCard() == null) 
		{
			if (client.getSalary() >= 425)
				client.setCard(new CreditCard());
			else if (client.getSalary() >= 0)
				client.setCard(new DebitCard());
			else
				System.out.println("Salari insuficient");
		} else {
			System.out.println("Ja tens associada una targeta.");
		}
	}
}
