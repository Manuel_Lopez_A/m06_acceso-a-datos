package factoriesAct02;

import java.util.Random;

public class DebitCard implements Card{
	
	String cardNumber;

	public DebitCard() {
		super();
		this.cardNumber = createCode();		
	}
	
	public String createCode() {
		Random r = new Random();
		String creditCardName="";
		for(int i = 0; i<8; i++) {
			creditCardName+= r.nextInt(10);
		}
		return creditCardName;
	}

	@Override
	public String toString() {
		return "DebitCard [cardNumber=" + cardNumber + "]";
	}
	
}
