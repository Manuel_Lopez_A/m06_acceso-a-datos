package genericsAct01;

public class Cotxe {

	private String matricula;
	private String marca;
	private String model;
	private Double llarg;
	private Double ample;
	
	public Cotxe(String matricula, String marca, String model, Double llarg, Double ample) {
		super();
		this.matricula = matricula;
		this.marca = marca;
		this.model = model;
		this.llarg = llarg;
		this.ample = ample;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Double getLlarg() {
		return llarg;
	}

	public void setLlarg(Double llarg) {
		this.llarg = llarg;
	}

	public Double getAmple() {
		return ample;
	}

	public void setAmple(Double ample) {
		this.ample = ample;
	}

	@Override
	public String toString() {
		return "Cotxe [matricula=" + matricula + ", marca=" + marca + ", model=" + model + ", llarg=" + llarg
				+ ", ample=" + ample + "]";
	}
	
	
}
