package genericsAct01;

import java.util.ArrayList;
import java.util.List;

public class Garatge<E> {

	public List<E> lista = new ArrayList<E>();

	public void mostrarVehicles() {
		System.out.println("Llista de vehicles al garatge: ");
		for (E e : lista) {
			System.out.println(e);
		}
	}

	public void afegirVehicle(E element) {
		lista.add(element);
	}

}
