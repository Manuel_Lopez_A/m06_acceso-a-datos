package genericsAct03y04;

import java.util.ArrayList;
import java.util.List;

public class ActivitatGenerics04 {

	public static void main(String[] args) {
//		04.-
//		Escriu un mètode genèric que intercanviï l’ordre 
//		dels elements de la llista independentment del tipus d’objecte de la llista.  
		
		ListaPopurri cajonDesastre = new ListaPopurri();
		int n1 = 15; 
		Double n2 = 16.50;
		float n3 = 12;
		Integer n4 = 19;
		String n5 = "patata";
		
		cajonDesastre.afegir(n1);
		cajonDesastre.afegir(n2);
		cajonDesastre.afegir(n3);
		cajonDesastre.afegir(n4);
		cajonDesastre.afegir(n5);
		
//		para ver la clase de valor
//		cajonDesastre.printClass();
		cajonDesastre.print();
		
		cajonDesastre.invierteValores();
		cajonDesastre.print();
		
	


		
		
	}
	
}
