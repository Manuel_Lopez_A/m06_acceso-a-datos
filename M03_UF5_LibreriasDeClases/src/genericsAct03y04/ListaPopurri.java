package genericsAct03y04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListaPopurri <E>  {

	// Declaro un atributo Lista <Genérico> y la convierto en ArrayList
	public List<E> todoValeLista = new ArrayList<E>();

	
	public void afegir(E element)
	{
		todoValeLista.add(element);
	}
	
	public void printClass()
	{
		for(E e : todoValeLista)
		{
			System.out.println(e.getClass());
			
			
		}
	}
	public void print()
	{
		for(E e : todoValeLista)
		{
			System.out.println(e);
		}
	}
	
//	04.-
//	Escriu un mètode genèric que intercanviï l’ordre 
//	dels elements de la llista independentment del tipus d’objecte de la llista.  
	public void invierteValores()
	{
		Collections.reverse(todoValeLista);		
	}

}
