package genericsAct03y04;

import java.util.ArrayList;
import java.util.List;

public class ActivitatsGenerics03 {

	public static void main(String[] args) {
		
		List<Number> numerillos = new ArrayList<>();
		int n1 = 15;
		Double n2 = 16.50;
		float n3 = 12;
		Integer n4 = 19;
//		String n5 = "patata";
		
		numerillos.add(n1);
		numerillos.add(n2);
		numerillos.add(n3);
		numerillos.add(n4);
//		numerillos.add(n5);
		
		
		System.out.println(numerillos);
		
		System.out.println(CalculaMedia(numerillos));

	}

	
	public static Double CalculaMedia(List<Number> nL) {
		
		Double media = 0.0;
		Double cont = 0.0;
		
		for (Number number : nL) {
			Double mNumber = number.doubleValue();
			media += mNumber;
			cont++;
		}
		
		media = media / cont;
		return media;
	}
	
}

