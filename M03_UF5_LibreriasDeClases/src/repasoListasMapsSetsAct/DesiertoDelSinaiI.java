package repasoListasMapsSetsAct;

import java.util.HashMap;
import java.util.Scanner;

public class DesiertoDelSinaiI {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		// casos de prueba
		int casos = sc.nextInt();
		HashMap<String, Integer> mapas = new HashMap<String, Integer>();

		for (int i = 0; i < casos; i++) {
			// linias en cada caso de prueba
			int linias = sc.nextInt();
			sc.nextLine();
			int max = 0;
			String ganador = "";
			
			for (int j = 0; j < linias; j++) {
				String nombre = sc.nextLine();
				//GET OR DEFAULT
				//pon esto en el diccionario, y el value por defecto será 0, y sumarle 1 si no existe
				mapas.put(nombre, mapas.getOrDefault(nombre, 0)+1);
				if (mapas.get(nombre)> max) {
					max = mapas.get(nombre);
					ganador = nombre;					
				
				// si existe cambia el valor al valor anterior + voto
//				if (mapas.containsKey(nombre)) {
//					mapas.put(nombre, mapas.get(nombre) + 1);					
//				}
				// si no, añadelo (y modifica los máximos y el ranking de ganadores)
//				else {
//					mapas.put(nombre, 1);
//					if (mapas.get(nombre)> max) {
//						max = mapas.get(nombre);
//						ganador = nombre;
//					}
//				}
			
			}

		}

		System.out.println(ganador);
		mapas.clear();
	}
	sc.close();

}
}
