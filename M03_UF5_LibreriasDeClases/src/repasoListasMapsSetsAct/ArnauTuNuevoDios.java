package repasoListasMapsSetsAct;

import java.util.TreeMap;
import java.util.Map;
import java.util.Scanner;

public class ArnauTuNuevoDios {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();
		for (int i = 0; i < casos; i++) {

			int lineas = sc.nextInt();
			sc.nextLine();
			TreeMap<String, String> biblia = new TreeMap<String, String>();

			for (int j = 0; j < lineas; j++) {

				String frase[] = sc.nextLine().split(" ");
				String key = "";
				String value = "";

				for (int k = 0; k < frase.length; k++) {
					if (k < 2) {
						key += frase[k];
					} else if (k == 2) {
						if (comprobarNumero(frase[k])) {
							key += ":" + frase[k];
						} else {
							value += frase[k] + " ";
						}
					} else {
						value += frase[k];
						if (k < frase.length - 1) {
							value += " ";
						}
					}

				}

				biblia.put(key, value);

			}
			String busqueda = sc.nextLine();
			System.out.println(biblia);

			for (Map.Entry<String, String> entrada : biblia.entrySet()) {
				if (entrada.getKey().toLowerCase().contains(busqueda.toLowerCase())) {
					System.out.println("\"" + entrada.getValue() + "\"");
				}
			}

		}
		sc.close();

	}

	private static boolean comprobarNumero(String aComprobar) {
		try {
//			int numero = Integer.parseInt(aComprobar) + 1;
			return true;

		} catch (Exception e) {
			return false;
		}

	}

}
