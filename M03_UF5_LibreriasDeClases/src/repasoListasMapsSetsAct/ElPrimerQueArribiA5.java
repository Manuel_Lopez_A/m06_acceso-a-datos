package repasoListasMapsSetsAct;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class ElPrimerQueArribiA5 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		HashMap<String, Integer> jugadors = new HashMap<String, Integer>();
		String nomJugador = sc.nextLine();
		
		while (!nomJugador.equals("xxx")){
			
			if(jugadors.containsKey(nomJugador)) {
				jugadors.put(nomJugador, jugadors.get(nomJugador)+1);
			} else {
				jugadors.put(nomJugador, 1);
			}
			nomJugador = sc.nextLine();
		}
		boolean trobat = false;
		for (Entry<String, Integer> jug : jugadors.entrySet()) {
			if (jug.getValue() == 5) {
				System.out.println(jug.getKey());
				trobat = true;
			}
			
		}
		if (trobat == false) {
			System.out.println("NO");
		}
		sc.close();
	}

}
