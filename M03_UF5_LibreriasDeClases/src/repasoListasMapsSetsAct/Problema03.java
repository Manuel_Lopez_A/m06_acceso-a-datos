package repasoListasMapsSetsAct;

import java.util.TreeMap;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class Problema03 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();

		for (int i = 0; i < casos; i++) {

			int lineas = sc.nextInt();
			sc.nextLine();// limpiaBuffer
			LinkedHashMap<String, Double> animeList = new LinkedHashMap<String, Double>();
			Double puntuacionMedia = 0.0;

			for (int j = 0; j < lineas; j++) {
				String entrada[] = sc.nextLine().split("-");
				animeList.put(entrada[0], Double.parseDouble(entrada[1]));
				puntuacionMedia += animeList.get(entrada[0]);
			}
			puntuacionMedia/=animeList.size();
			
			// RUSTIC MODE => quito dos decimales, redondeo y divido para poner dos decimales
//            puntuacionMedia = Math.round(puntuacionMedia * 100.0) / 100.0;
			// lo da por inválido JO-EL
			
			// Formatea puntuacionMedia a dos decimales
            DecimalFormat df = new DecimalFormat("0.00");
            String puntuacionMediaFormateada = df.format(puntuacionMedia);
			
			TreeMap<Double, String> animeListOrdenada = new TreeMap<Double, String>();
			for (Entry<String, Double> u : animeList.entrySet()) {
				animeListOrdenada.put(u.getValue(), u.getKey());
			}
			
//			System.out.println(animeListOrdenada);
//			System.out.println(animeList.firstKey());
			System.out.print(puntuacionMediaFormateada +" ");
			System.out.print(animeListOrdenada.get(animeListOrdenada.firstKey())+" ");
			System.out.println(animeListOrdenada.descendingMap().get(animeListOrdenada.descendingMap().firstKey()));
		}
		sc.close();
	}

}
