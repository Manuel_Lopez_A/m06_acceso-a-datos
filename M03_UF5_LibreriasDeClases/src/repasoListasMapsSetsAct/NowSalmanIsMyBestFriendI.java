package repasoListasMapsSetsAct;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class NowSalmanIsMyBestFriendI {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();

		for (int i = 0; i < casos; i++) {

			int lineas = sc.nextInt();
			sc.nextLine(); // limpiar buffer famoso BUG

			HashMap<String, String> listaAmigos = new HashMap<String, String>(lineas);
			
//			la linea -1 es 
			for (int j = 0; j < lineas-1; j++) {
				String key = sc.next();
				String value = sc.next();

				listaAmigos.put(key, value);

			}

			String friendLooked = sc.next();
			
//			Podemos buscar también con Map.Entry<String, String> mejorAmigo : listaAmigos.entrySet()
			for (Entry<String, String> mejorAmigo : listaAmigos.entrySet()) {
				if (mejorAmigo.getKey().equals(friendLooked)) {
					System.out.println(mejorAmigo.getValue());
				}
			}
		}
		sc.close();
	}
}
