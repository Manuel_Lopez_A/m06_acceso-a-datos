package repasoListasMapsSetsAct;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

public class Problema02 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int lineas = sc.nextInt();
			sc.nextLine();//limpiaBuffer
			
			HashMap<String, Integer> ranking = new HashMap<String, Integer>(); 
			
			for (int j = 0; j < lineas; j++) {
				String entrada[] = sc.nextLine().split("-");
				ranking.put(entrada[1], ranking.getOrDefault(entrada[1], 0)+1);
			}
			
			TreeMap<Integer, String> rankingOrdenado = new TreeMap<Integer, String>();
			for (Entry<String, Integer> listaPrimera : ranking.entrySet()) {
				rankingOrdenado.put(listaPrimera.getValue(), listaPrimera.getKey());
			}
//			=> este imprime el map entero en orden inverso, puede servir para ir pasándolo 
//			=> a otra lista, mientras lo leo del revés			
//			System.out.println(rankingOrdenado.descendingMap());
			for (Integer u : rankingOrdenado.descendingKeySet()) {
//				System.out.println(rankingOrdenado.get(u)+"-"+u);
				for(Entry<String, Integer> listaPrimera : ranking.entrySet()) {
					if(listaPrimera.getValue() == u) {
						System.out.println(listaPrimera.getKey() + "-"+listaPrimera.getValue());
					}
				}
			}
			
			ranking.clear();
		}
		sc.close();
	}
}