package repasoListasMapsSetsAct;

import java.util.HashMap;
import java.util.Scanner;

public class NowSalmanIsMyBestFriendII {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int lineas = sc.nextInt();
			sc.nextLine(); // limpiar buffer				
			HashMap<String, String> bestFriends = new HashMap<String, String>(lineas);
				
			for (int j = 0; j < lineas-1; j++) {
				
				String friendOne = sc.next();
				String friendTwo = sc.next();				
				bestFriends.put(friendOne, friendTwo);				
				if(bestFriends.containsKey(friendTwo)) {
					bestFriends.put(friendTwo, friendOne);
				}
//				imprime el valor del diccionario de lo que te escanee
//				System.out.println(bestFriends.get(sc.next()));		
				
			}
			
//			String bestFriend = sc.next();
			System.out.println(bestFriends.get(sc.next()));
			
		}

		sc.close();
		
	}

}
