package repasoListasMapsSetsAct;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class DesiertoDelSinaiII {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		// casos de prueba
		int casos = sc.nextInt();

		for (int i = 0; i < casos; i++) {
			
			HashMap<String, Integer> mapas = new HashMap<String, Integer>();
			int linias = sc.nextInt();
			sc.nextLine();
			
			for (int j = 0; j < linias; j++) {
				// dividimos la linea en voto que convertimos a integer
				String[] dividido = sc.nextLine().split(" ");
				Integer voto = Integer.parseInt(dividido[dividido.length-1]);
				// juntar palabros del array dividido
				String mapa = dividido[0];
				for (int k = 1; k < dividido.length-2; k++) {
					mapa += " "+dividido[k];
				}
				
				if (mapas.containsKey(mapa)) {
					mapas.put(mapa, mapas.get(mapa) + voto);
				} else {
					mapas.put(mapa, voto);
				}
			}
			// manera de recoger el valor máximo
			Integer maximo = Collections.max(mapas.values());
			
			for (Map.Entry<String, Integer> mapilla : mapas.entrySet()) {
				if(mapilla.getValue()== maximo) {
					System.out.println(mapilla.getKey());
				}
			}

		}

		sc.close();

	}

}
