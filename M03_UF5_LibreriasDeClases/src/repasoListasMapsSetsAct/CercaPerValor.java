package repasoListasMapsSetsAct;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CercaPerValor {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int lineas = sc.nextInt();
			sc.nextLine(); //limpiar buffer
			HashMap<String, String> paisCapital = new HashMap<String, String>(lineas);
			
			for (int j = 0; j < lineas-1; j++) {
				String entrada = sc.nextLine();
				String troceado[] = entrada.split("-");
				paisCapital.put(troceado[0], troceado[1]);
			}
			String busqueda = sc.nextLine();
					
			System.out.println(paisCapital);
			for (Map.Entry<String, String> pais : paisCapital.entrySet()) {
				if(pais.getValue().equals(busqueda)) System.out.println(pais.getKey());
			}
		}
		sc.close();
	}

}
