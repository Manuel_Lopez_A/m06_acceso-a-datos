package repasoListasMapsSetsAct;

import java.util.HashMap;
import java.util.Scanner;

public class Problema01 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			
			int lineas = sc.nextInt();
			sc.nextLine();//limpiaBuffer
			
			HashMap<String, Integer> ranking = new HashMap<String, Integer>();
			
			for (int j = 0; j < lineas-1; j++) {
				String entrada[] = sc.nextLine().split("-");
				ranking.put(entrada[1], ranking.getOrDefault(entrada[1], 0)+1);
			}
			
			String aBuscar = sc.nextLine();
			
			if(ranking.containsKey(aBuscar)) { 
				System.out.println(ranking.get(aBuscar));
			} else {
				System.out.println("0");
			}
			
			ranking.clear();
		}
		sc.close();
	}
}
