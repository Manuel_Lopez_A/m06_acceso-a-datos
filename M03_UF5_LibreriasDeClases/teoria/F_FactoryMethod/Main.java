package F_FactoryMethod;

public class Main {

	
	public static void main(String[] args) {
		GossosFactory factory = new GossosFactory();
		
		Gos corgi = factory.getGos("corgi");
		Gos golden = factory.getGos("golden");
		Gos gnomo = factory.getGos("gnomo");
		
		corgi.getRasa();
		golden.getRasa();
		gnomo.getRasa();
		
		
		

	}

}
