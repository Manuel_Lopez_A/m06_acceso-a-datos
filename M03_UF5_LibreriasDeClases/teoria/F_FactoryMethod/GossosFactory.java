package F_FactoryMethod;

public class GossosFactory {		
	
	Gos getGos(String rasa)
	{
		Gos g;
		if(rasa.equalsIgnoreCase("corgi"))
			g = new Corgi();
		else if(rasa.equalsIgnoreCase("golden"))
			g = new Golden();
		else if(rasa.equalsIgnoreCase("gnomo"))
			g = new Gnomo();
		else g = null;
		return g;
	}
}
