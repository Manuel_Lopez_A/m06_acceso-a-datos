package F_AbstractFactoryu;

public class FerrallaFactory implements AbstractFactory<Moble>{
	@Override
	public Moble create(Mobles m, int potes) {
		
			if(m==Mobles.CADIRA)
				return new Cadira(Estil.FERRALLA, potes);
			else if(m==Mobles.SOFA)
				return new Sofa(Estil.FERRALLA, potes);
			else if(m==Mobles.TAULA)
				return new Taula(Estil.FERRALLA, potes);
			return null;
		
	}
}
