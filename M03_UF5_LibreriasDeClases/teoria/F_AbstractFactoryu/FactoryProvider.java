package F_AbstractFactoryu;

public class FactoryProvider {
	
	static AbstractFactory getFactory(Estil e)
	{
		if(e==Estil.ARTNOVEAU)
			return new ArtNovaeauFactory();
		if(e==Estil.VICTORIA)
			return new VictorianFactory();
		if(e==Estil.FERRALLA)
			return new FerrallaFactory();
		return null;
		
	}
}
