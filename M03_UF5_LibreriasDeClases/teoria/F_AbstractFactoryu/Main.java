package F_AbstractFactoryu;

public class Main {
	public static void main(String[] args) 
	{
		Cadira m = (Cadira) FactoryProvider.getFactory(Estil.ARTNOVEAU).create(Mobles.CADIRA, 10);
		Moble m1 = (Moble) FactoryProvider.getFactory(Estil.VICTORIA).create(Mobles.SOFA, 100);
		Moble m2 = (Moble) FactoryProvider.getFactory(Estil.FERRALLA).create(Mobles.CADIRA, 2);
	
		System.out.println(m1 + m1.getClass().toString());
		System.out.println(m2 + m2.getClass().toString());
		System.out.println(m + m.getClass().toString());	
	}
}
