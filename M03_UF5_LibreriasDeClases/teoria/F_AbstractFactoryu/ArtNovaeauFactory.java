package F_AbstractFactoryu;

public class ArtNovaeauFactory implements AbstractFactory<Moble>{

	@Override
	public Moble create(Mobles m, int potes) {
		if(m==Mobles.CADIRA)
			return new Cadira(Estil.ARTNOVEAU, potes);
		else if(m==Mobles.SOFA)
			return new Sofa(Estil.ARTNOVEAU, potes);
		else if(m==Mobles.TAULA)
			return new Taula(Estil.ARTNOVEAU, potes);
		return null;
	}

}
