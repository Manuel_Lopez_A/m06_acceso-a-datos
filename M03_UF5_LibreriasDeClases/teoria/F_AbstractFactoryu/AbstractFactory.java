package F_AbstractFactoryu;

public interface AbstractFactory<J>{
	
	J create(Mobles m, int potes);
	
}
