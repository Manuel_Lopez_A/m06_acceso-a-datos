package F_AbstractFactoryu;

public abstract class Moble 
{
	public Estil tipus;
	public int potes;
	public Moble(Estil e, int p) {
		this.tipus = e;
		this.potes = p;
	}
	public Moble() {
		// TODO Auto-generated constructor stub
	}
	public void diguesTipus()
	{
		System.out.println(this.tipus);
	}
	@Override
	public String toString() {
		return "Moble [tipus=" + tipus + ", potes=" + potes + "]";
	}
	
	
}
