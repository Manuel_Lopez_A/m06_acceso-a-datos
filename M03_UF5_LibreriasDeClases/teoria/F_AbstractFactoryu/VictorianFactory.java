package F_AbstractFactoryu;

public class VictorianFactory implements AbstractFactory<Moble>{

	@Override
	public Moble create(Mobles m, int potes) {
		
			if(m==Mobles.CADIRA)
				return new Cadira(Estil.VICTORIA, potes);
			else if(m==Mobles.SOFA)
				return new Sofa(Estil.VICTORIA, potes);
			else if(m==Mobles.TAULA)
				return new Taula(Estil.VICTORIA, potes);
			return null;
		
	}

}
