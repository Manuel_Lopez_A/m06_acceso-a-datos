package SingletonTeoria;

public class MainSingleton 
{
	public static void main(String[] args) 
	{
		MiSingleton a = MiSingleton.getInstance();
		
		MiSingleton b = MiSingleton.getInstance();
		
		MiSingleton c = MiSingleton.getInstance();
		
		
		a.setSalutacio(c.getSalutacio().toUpperCase());
		
		
		System.out.println(a.getSalutacio());
		System.out.println(b.getSalutacio());
		System.out.println(c.getSalutacio());
		
		
	}
}
