package SingletonTeoria;

public class MiSingleton 
{
	private String salutacio;
	private static MiSingleton instance = null;	
	
//	private MiSingleton()
//	{
//		this.salutacio = "soc un pobre single... ton";
//	}
	
	public static synchronized MiSingleton getInstance()
	{
				
		if(instance==null)
			return instance = new MiSingleton();
		return instance;
		
	}
	
	public String getSalutacio() {
		return salutacio;
	}

	public void setSalutacio(String salutacio) {
		this.salutacio = salutacio;
	}

	
	
	
}
