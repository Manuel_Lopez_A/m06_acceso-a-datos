package Observer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class OcaPropertyChange implements PropertyChangeListener{
	@Override
	public void propertyChange(PropertyChangeEvent evt) 
	{
		if(evt.getNewValue().equals("aigua d'oca"))
		{
			System.out.println("evt.getNewValue(): "+ evt.getNewValue());
			System.out.println("evt.getOldValue(): "+ evt.getOldValue());
			System.out.println("evt.getPropertyName(): " + evt.getPropertyName());
			this.bebo();

		}
		
	}
	
	public void bebo()
	{
		System.out.println("Soc oca muerte a todos");
	}
}
