package Observer;

public class MainPropertyChange 
{
	public static void main(String[] args) 
	{
		AbeuradorPropertyChanges a = new AbeuradorPropertyChanges();
		
		GallinaPropertyChange turuletaPija = new GallinaPropertyChange();
		OcaPropertyChange descalsaPija = new OcaPropertyChange();
		AnecPropertyChange donaldPijo =new AnecPropertyChange();
		
		a.addPropertyChangeListener(donaldPijo);
		a.addPropertyChangeListener(descalsaPija);
		a.addPropertyChangeListener(turuletaPija);
		
		
		a.setTipusAigua("aigua d'oca");
		
	}
}
