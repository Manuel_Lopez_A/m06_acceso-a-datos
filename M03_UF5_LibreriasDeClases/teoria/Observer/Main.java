package Observer;

public class Main 
{
	public static void main(String[] args) 
	{
		Abeurador a = new Abeurador();
		
		Gallina turuleta = new Gallina();
		Anec Donald = new Anec();
		Oca Descalsa = new Oca();
		
		a.addSubs(Donald);
		a.addSubs(turuleta);
		a.addSubs(Descalsa);
		
		a.hayAgua();
		
		
	}
}
