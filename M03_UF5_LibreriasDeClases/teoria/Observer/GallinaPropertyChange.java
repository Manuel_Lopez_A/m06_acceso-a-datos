package Observer;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class GallinaPropertyChange implements PropertyChangeListener {
	@Override
	public void propertyChange(PropertyChangeEvent evt) 
	{
		
		if(evt.getNewValue().equals("aigua de gallina"))
		{
			System.out.println("evt.getNewValue(): "+ evt.getNewValue());
			System.out.println("evt.getOldValue(): "+ evt.getOldValue());
			System.out.println("evt.getPropertyName(): " + evt.getPropertyName());
			this.bebo();

		}
		
	}
	
	public void bebo()
	{
		System.out.println("Soc gallina cocococococo");
	}
}
