package Observer;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class AbeuradorPropertyChanges {
	
	private PropertyChangeSupport support;
	private String tipusAigua = "";

	public AbeuradorPropertyChanges() {
		super();
		this.support = new PropertyChangeSupport(this);
	}
	
	public void addPropertyChangeListener(PropertyChangeListener pcl)
	{
		support.addPropertyChangeListener(pcl);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener pcl)
	{
		support.removePropertyChangeListener(pcl);
	}
	
	public String getTipusAigua()
	{
		return this.tipusAigua;
	}
	
	public void setTipusAigua(String tipus)
	{
		support.firePropertyChange("caca", this.getTipusAigua(), tipus);
		this.tipusAigua = tipus;
	}
	
	
	
	

	
}
