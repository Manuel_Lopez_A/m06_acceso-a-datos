package Observer;

public interface Observable 
{
	public void addSubs(Observer o);
	public void removeSubs(Observer o);
	public void hayAgua();
}
