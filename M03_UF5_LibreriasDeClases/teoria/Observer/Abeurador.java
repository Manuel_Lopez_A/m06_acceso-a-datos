package Observer;

import java.util.ArrayList;

public class Abeurador implements Observable
{
	public ArrayList<Observer> animals = new ArrayList<Observer>();
	@Override
	public void addSubs(Observer o) {
		animals.add(o);
		
	}

	@Override
	public void removeSubs(Observer o) {
		animals.remove(o);
		
	}

	@Override
	public void hayAgua() {
		for(Observer o : animals)
		{
			o.voyABeber();
		}
	}

}
