package Decorator;

public class Fideos extends DecoratorRamen
{

	Fideos(DecorateInterface object) {
		super(object);
	}
	
	public String decorate()
	{
		return super.decorate() + " amb fideus ";
	}
	
	
}
