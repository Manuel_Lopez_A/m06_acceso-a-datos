package Decorator;

public interface DecorateInterface 
{
	String decorate();
}
