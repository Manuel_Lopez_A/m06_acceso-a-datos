package Decorator;

public class Main 
{
	public static void main(String[] args) 
	{
		DecorateInterface ramen = new Ou(new Naruto(new Fideos(new Sopa())));
		DecorateInterface ramen2 = new Sopa();
		DecorateInterface ramenAmbOu = new Ou(ramen2);
		System.out.println(ramen.decorate());
		System.out.println(ramenAmbOu.decorate());
				
	}
}
