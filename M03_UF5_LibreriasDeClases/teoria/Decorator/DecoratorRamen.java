package Decorator;

public abstract class DecoratorRamen implements DecorateInterface
{
	private DecorateInterface ramen;
	
	public DecoratorRamen(DecorateInterface object)
	{
		this.ramen = object;
	}
	
	@Override
	public String decorate() {
		return ramen.decorate();
	}
	
}
