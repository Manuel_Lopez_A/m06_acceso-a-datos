package Generics;

public class GenericMain 
{
	public static void main(String[] args) 
	{
		StringsAndInts<String> elementCadena = new StringsAndInts<String>("Ari, buenas tardes!!!");
		StringsAndInts<Integer> elementInteger = new StringsAndInts<Integer>(33);
		StringsAndInts<Double> elementDouble = new StringsAndInts<Double>(69.420);
		elementCadena.Syso();
		elementInteger.Syso();
		elementDouble.Syso();
		
		readNumbers(elementDouble);
		readNumbers(elementInteger);
		
		//readNumbers(elementCadena);
		
		
		ListaIndefinid lista = new ListaIndefinid<>();
		
		lista.afegir(33.3);
		lista.afegir(3);
		lista.afegir("hola");
		
		lista.print();
		
		
		
		
		
		
	}
	
	public static void readNumbers(StringsAndInts<? extends Number> numero)
	{
		if(numero.dada instanceof Double)
		{
			System.out.println("PERO BUENO, SI AIXO ES UN DABEL");
		}
		else if(numero.dada instanceof Integer)
		{
			System.out.println("AH BUENO... NOMES ES UN INTEGER");
		}
	}
}
