package Generics;

import java.util.ArrayList;
import java.util.List;

public class ListaIndefinid <E>
{
	public List<E> lista = new ArrayList<E>();
	
	public void afegir(E element)
	{
		lista.add(element);
	}
	
	public void print()
	{
		for(E e : lista)
		{
			System.out.println(e.getClass());
			
			
		}
	}
}
