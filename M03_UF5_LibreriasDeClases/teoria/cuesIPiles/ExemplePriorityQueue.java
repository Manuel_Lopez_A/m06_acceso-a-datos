package cuesIPiles;

import java.util.PriorityQueue;
import java.util.Queue;

public class ExemplePriorityQueue {
	public static void main(String[] args) {
		// Creem una cua de prioritat
		Queue<Tasca> cuaPrioritat = new PriorityQueue<>();

		// Afegim tasques amb diferents prioritats
		// El mètode offer serveix per afegir nous elements a la part de la cua
		boolean valorMetode = false;
		
		valorMetode = cuaPrioritat.offer(new Tasca("Tasca posar més contingut", 0));
		System.out.println("El resultat d'afegir: " + valorMetode);
		cuaPrioritat.offer(new Tasca("Tasca posar més exercicis", 50));
		cuaPrioritat.offer(new Tasca("Tasca corregir", 100));
		
		
		// Examinem la part davantera de la cua el valor que hi ha
		Tasca tascaDavant = cuaPrioritat.peek();
		System.out.println("Tasca de la part davantera: " + tascaDavant);
		// Eliminem les tasques de la part davantera i a més mostrem el valor
		// No hi ha una altra manera de veure el valor que eliminant el contingut de la cua
		System.out.println("Tasques en ordre de prioritat:");
		while (!cuaPrioritat.isEmpty()) {
			// Mètode Poll, elimina valors del davant de la cua
			Tasca tasca = cuaPrioritat.poll();
			System.out.println(tasca);
		}
	}

}

// Creem la classe tasca amb els seus atributs
class Tasca implements Comparable<Tasca> {
	private String descripcio;
	private int prioritat;

	// Constructor de la classe tasca
	public Tasca(String descripcio, int prioritat) {
		this.descripcio = descripcio;
		this.prioritat = prioritat;
	}

	// Comparació basada en la prioritat.
	// Els valors més petits són més importants
	@Override
	public int compareTo(Tasca altra) {
		return Integer.compare(this.prioritat, altra.prioritat);
	}

	@Override
	public String toString() {
		return "Tasca: " + descripcio + " (Prioritat: " + prioritat + ")";
	}
}
