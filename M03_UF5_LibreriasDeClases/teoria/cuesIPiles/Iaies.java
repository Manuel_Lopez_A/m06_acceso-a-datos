package cuesIPiles;

public class Iaies implements Comparable<Iaies>
{
	String nom;
	int edat;
	int caramels;	
	
	public Iaies(String nom, int edat, int caramels) {
		super();
		this.nom = nom;
		this.edat = edat;
		this.caramels = caramels;
	}

	public int compareTo(Iaies altra) {
		return Integer.compare(altra.caramels, this.caramels);
	}
	
	@Override
	public String toString() {
		return "Iaies [nom=" + nom + ", edat=" + edat + ", caramels=" + caramels + "]";
	}
	
	
	
}
